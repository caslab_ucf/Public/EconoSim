;; Input values need to correspond to distribution means, not exact values. 

;__includes ["banks.nls" firms.nls" "sources.nls" "trans.nls"]

; to generate random seeds for experiments:   n-values 20 [-2147483648 + random( 2147483647 - -2147483648 + 1)]

globals [
  ;; Utility constants
  max-radius
  num-firms
  
  rule-table   ; for plotting
  
  viable-species
  viable-resources
  
  ; fitness measures
  max-resources-bought
  max-resources-sold
  max-energy
  
  total-firms-injected
  total-firms-dissipated
  
  ;;;; per step measures
  
  firm-count
  active-firm-count      ; number of firms that bought or sold in the current time step
  firms-born        ; cannot calculate just based on population at time t and t-1 because of confounding
  firms-killed
  
  incubator-population
  incubation-time-remaining
  incubated-firms
  
  ; Distributions
  firm-ids
  household-ids
  bank-ids
    
  ; Firm Distributions
  fvr ; firm vision radius
  ftr ; firm transfer radius
  
  fparent ; firm id of the parent agent
  fage    ; firm number of ticks the agent has been alive
  
  finput  ; input resource
  foutput  ; output resource
  
  fop ; output resource price
  fpst ; price stability threshold
  fpcr ; price change rate
  
  firq ; input resource quantity
  forq ; output resource quantity
  fmq  ; money quantity
  
  firf ; input resources bought from firms
  fmsf ; money spent to buy resources from firms
  forf ; output resources sold to firms
  fmef ; money earned from selling resources to firms
  
  firh ; input resources bought from households
  fmsh ; money spent to buy resources from households
  forh ; output resources sold to households
  fmeh ; money earned from selling to households

  firt ; input resources transformed
  fort ; output resources produced
  
  fird ; input resources dissipation
  firi ; input resources injection
  ford ; output resources dissipation
  fori ; output resources injection
  fmd  ; money dissipation
  fmi  ; money injection
  
  firc ; input resouces given to child
  forc ; output resources given to child
  fmc ; money given to child
  
  firp ; input resources from parent
  forp ; output resources from parent
  fmp  ; money from parent
  
  ; Derived distributions
  rule-pairs
  
  factor-prices
  product-prices
  
  fprofits  ; profit made by firms
  hprofits  ; profit made by households
  
  fdemand
  
  fincubated
  
  ; Summary Measures
  
  ; flow of mq variables
  mff ; mq from firm to firm
  mfg ; mq from firm to government
  mgf
  
  ; flow of resource variables
  rff ; resources from firm to firm
  rfg ; resources from firm to government
  rgf
  
  
  ; population measures
 ; rule-dist
  mortality-rate
  
  ; macroeconomic measures
  gdp                          ; Consumer spending + Investment + Government spending + (eXports - iMports)
  prev-gdp                     ; used for calculation of the change in gdp
  delta-gdp
  
  rgdp                         ; real GDP
  prev-rgdp
  delta-rgdp
  wages
  
  trade-volume
  
  base-cpi    ; baseline consumer price index
  cpi    ; consumer price index - accounts for the price of all inputs used by firms with a base year of 0
  prev-cpi
  delta-cpi   ; consumer price index inflation
  
  base-ppi    ; baseline producer price index
  ppi
  prev-ppi
  delta-ppi
]


breed [firms firm]

firms-own [
  ;; fields
  vr ; vision radius
  tr ; transfer radius
  rr ; reproduction radius
  
  parent ; id of the parent agent
  age    ; number of ticks the agent has been alive
  
  input  ; input resource
  output  ; output resource
  
  op ; output resource price
  pst ; price stability threshold
  pcr ; price change rate
  
  irq ; input resource quantity
  orq ; output resource quantity
  mq  ; money quantity
  
  irf ; input resources bought from firms
  msf ; money spent to buy resources from firms
  orf ; output resources sold to firms
  mef ; money earned from selling resources to firms

  irt ; input resources transformed
  ort ; output resources produced
  
  ird ; input resources dissipation
  iri ; input resources injection
  ord ; output resources dissipation
  ori ; output resources injection
  md  ; money dissipation
  mi  ; money injection
  
  irc ; input resouces given to child
  orc ; output resources given to child
  mc ; money given to child
  
  irp ; input resources from parent
  orp ; output resources from parent
  mp  ; money from parent
  
  dissipation-probability
  dissipation-rate
  
  ; summary measures
  ticks-successful   ; how many ticks in a row has the agent been able to sell something
  
  energy
  
  trade-target
  
  demand
  prev-demand
  
  incubated?
  
  ; dissipation needs to be a firm variable, not global. 
  
  spawn-probability
  parent-contribution
  mutation-probability
  
  ;; methods
  ; move        has the side effect of selecting the trade target and repositioning the agent
  ; trade
  ; produce
  ; dissipate
  ; reproduce
  ; die 
]


undirected-link-breed [trade-ties trade-tie]      ; the trade ties are the realized trade network at step t

breed [trans-nodes trans-node]
trans-nodes-own [
  id
]

directed-link-breed [trans-ties trans-tie]
trans-ties-own [
  exist-weight
  active-weight
]



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Simulation Setup
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup
  clear-all
;  set num-firms ceiling (creation-radius * world-width * world-height)
  set seed ifelse-value use-seed? [seed] [new-seed]
  random-seed seed
  
  ; set constant values
  set max-radius ceiling (0.5 * world-width)
  
  ;; create and initialize the table of all possible rule combinations
  set rule-table []
  foreach n-values (NUM-FACTORS + NUM-PRODUCTS) [?] [
    let i ?
    foreach n-values (NUM-FACTORS + NUM-PRODUCTS) [?] [
      let o ?
      set rule-table lput (list i o) rule-table
    ]
  ]
  
  ;; set the background color
  ask patches [set pcolor white]
  
  set num-firms count ((patches with [distance patch 0 0 < floor (creation-radius * world-width / 2)]) with [count firms-here = 0]) - 1  ; we're dealing with radius, not diameter
  create-firms num-firms [
    initialize-firm-agent
  ]
;  while [count ((patches with [distance patch 0 0 < floor (creation-radius * world-width)]) with [count firms-here = 0]) > 0  ] [
;    create-firms 1 [
;      initialize-firm-agent
;    ]
;  ]
  
  ask links [ hide-link ]

  set incubated-firms nobody
  
  ; set the base measures for price indices
  
;    let cpi-agents [op] of firms with [not empty? filter [output = ?] [input] of households]
;    let ppi-agents (sentence ([op] of firms with [not empty? filter [output = ?] [input] of firms]) 
;                             ([op] of households with [not empty? filter [output = ?] [input] of firms]))
;    set base-cpi ifelse-value (empty? cpi-agents) [0] [mean cpi-agents]
;    set base-ppi ifelse-value (empty? ppi-agents) [0] [mean ppi-agents]
  
  reset-tick-variables
  update-tick-variables
  update-population-variables
  
  create-transformation-network
  update-transformation-network
     
  if record-video? [
    ;    ask (turtle-set firms sources banks) [set color white]
    movie-cancel
    movie-start (word movie-file-name ".mov")
    movie-set-frame-rate 5
    movie-grab-interface ;; show the initial state
  ]
  
  reset-ticks
end



to setup-2-cycle
  setup
  set viable-resources [0]
  set viable-species [[0 1]]
  ask firms [set input 0 set output 1]
  ask n-of (num-firms / 2) firms [set input 1 set output 0]
end

to setup-4-cycle
  setup
  set viable-resources [0]
  set viable-species [[0 1]]
  ask firms [set input 0 set output 1]
  ask n-of (num-firms / 4) firms [set input 1 set output 2]
  ask n-of (num-firms / 4) firms [set input 2 set output 3]
  ask n-of (num-firms / 4) firms [set input 3 set output 0]
end




to initialize-firm-agent
  set vr calculate-vision-radius firm-mean-vision-radius firm-sd-vision-radius
  set tr calculate-transfer-radius firm-mean-transfer-radius firm-sd-vision-radius
  set rr firm-repro-radius
  
  if equal-radius? [
    set tr vr
    set rr vr
  ]
   
  set parent -1
  set age 0
  
  let initial-rules create-initial-agent-transformation-rule
  set input item 0 initial-rules
  set output item 1 initial-rules
  
  set op min-firm-price
  set pst max (list 1 (ceiling random-normal firm-mean-price-stability firm-sd-price-stability))
  set pcr firm-price-change-rate
  
  set dissipation-probability firm-dissipation-probability
  set dissipation-rate        firm-dissipation-rate
  set demand min-firm-demand
  
  set irq 0
  set orq initial-firm-resources
  set mq initial-firm-money
  
  
  ; summary variables
  set energy irq + orq + mq
  
  set incubated? 0
  
  set parent-contribution firm-parent-contrib
  set mutation-probability firm-mutation-probability
  
  set color 10 * output + 5
  set shape "square"
  set size 1
  
  let sites patches with [distance patch 0 0 < floor (creation-radius * world-width / 2)]
  let empty-site one-of sites with [count firms-here = 0]
  let sx [pxcor] of empty-site
  let sy [pycor] of empty-site
  setxy sx sy
end



to-report calculate-vision-radius [mu sigma]
  report min (list max-radius max (list 1 (ceiling random-normal mu sigma)))
end

to-report calculate-transfer-radius [mu sigma]
  report min (list max-radius max (list 1 min (list vr (ceiling random-normal mu sigma))))
end


to-report create-initial-agent-transformation-rule
  let factors  n-values (num-factors)  [?]
  let products n-values (num-products) [num-factors + ?]
  let resources (sentence factors products)
  
  let i 0
  let o 0
  if firm-rule-format = "factor -> factor" [ 
    set i one-of factors
    set o one-of remove i factors
  ]
  if firm-rule-format = "factor -> product" [ 
    set i one-of factors
    set o one-of products
  ]
  if firm-rule-format = "factor -> factor + product" [ 
    set i one-of factors
    set o one-of remove i resources
  ]
  if firm-rule-format = "product -> factor" [ 
    set i one-of products
    set o one-of factors
  ]
  if firm-rule-format = "product -> product" [ 
    set i one-of products
    set o one-of remove i products
  ]
  if firm-rule-format = "product -> factor + product" [ 
    set i one-of products
    set o one-of remove i resources
  ]
  if firm-rule-format = "factor + product -> factor" [ 
    set i one-of resources
    set o one-of remove i factors
  ]
  if firm-rule-format = "factor + product -> product" [ 
    set i one-of resources
    set o one-of remove i products
  ]
  if firm-rule-format = "factor + product -> factor + product" [ 
    set i one-of resources
    set o one-of remove i resources
  ]
  
  report (list i o)
end



to reset-tick-variables
  reset-global-tick-variables
  
  ask firms [
    reset-flow-variables
  ]
end



to reset-global-tick-variables
  set firms-born 0
  set firms-killed 0
end



to reset-flow-variables
  set irf 0; input resources bought from firms
  set msf 0; money spent to buy resources from firms
  set orf 0; output resources sold to firms
  set mef 0; money earned from selling resources to firms
  
  set irt 0; input resources transformed
  set ort 0; output resources produced
  
  set ird 0; input resources dissipated
  set iri 0; input resources injected
  set ord 0; output resources dissipated
  set ori 0; output resources injected
  set md  0; money dissipated
  set mi  0; money injected
  
  set irc 0; input resouces given to child
  set orc 0; output resources given to child
  set mc  0; money given to child
end



to update-tick-variables  
  ; distibutions - we have to make sure the list is sorted by [who] so that we can associate data properly. 
  let sorted-firm-list (sort-on [who] firms)
      
  
  ; Firm Distributions
  set fparent map [[parent] of ?] sorted-firm-list ; id of the parent agent
  set fage    map [[age] of ?] sorted-firm-list    ; number of ticks the agent has been alive
  
  set finput  map [[input] of ?] sorted-firm-list; input resource
  set foutput map [[output] of ?] sorted-firm-list; output resource
  
  set fincubated map [[incubated?] of ?] sorted-firm-list
  
  ;; "variables" handle attribute variables, "measures" update flow variables
  update-price-measures          sorted-firm-list 
  
  update-id-variables            sorted-firm-list 
  
  update-quantity-variables      sorted-firm-list 
  
  update-trade-measures          sorted-firm-list 
  update-transformation-measures sorted-firm-list 
  update-dissipation-measures    sorted-firm-list 
  update-child-measures          sorted-firm-list 
  update-parent-measures         sorted-firm-list 
  update-injection-measures      sorted-firm-list 
  
  update-flow-variables
  
  update-price-variables
    
  update-business-measures       sorted-firm-list 
  update-macroeconomic-measures
  
  ; Derived Distributions
  ;  set rule-pairs (map [ (word "(" ?1 "," ?2 ")") ] (map [[input] of ?] sorted-firm-list) (map [[output] of ?] sorted-firm-list))
end



to update-price-measures  [sorted-firm-list]
  set fop  map [[op] of ?]  sorted-firm-list; output resource price
  set fpst map [[pst] of ?] sorted-firm-list; price stability threshold
  set fpcr map [[pcr] of ?] sorted-firm-list; price change rate
end



to update-id-variables [sorted-firm-list]
  set firm-ids      map [[who] of ?] sorted-firm-list
end



to update-interaction-variables [sorted-firm-list]
  set fvr map [[vr] of ?] sorted-firm-list ; firm vision radius
  set ftr map [[tr] of ?] sorted-firm-list ; firm transfer radius
end



to update-quantity-variables [sorted-firm-list]
  set firq map [[irq] of ?] sorted-firm-list; input resource quantity
  set forq map [[orq] of ?] sorted-firm-list; output resource quantity
  set fmq  map [[mq] of ?]  sorted-firm-list; money quantity
end



to update-trade-measures [sorted-firm-list]
  set firf map [[irf] of ?] sorted-firm-list; input resources bought from firms
  set fmsf map [[msf] of ?] sorted-firm-list; money spent to buy resources from firms
  
  set forf map [[orf] of ?] sorted-firm-list; output resources sold to firms
  set fmef map [[mef] of ?] sorted-firm-list; money earned from selling resources to firms
end



to update-transformation-measures [sorted-firm-list]
  set firt map [[irt] of ?] sorted-firm-list; input resources transformed
  set fort map [[ort] of ?] sorted-firm-list; output resources produced
end



to update-dissipation-measures [sorted-firm-list]
  set fird map [[ird] of ?] sorted-firm-list; input resources dissipation
  set ford map [[ord] of ?] sorted-firm-list; output resources dissipation
  set fmd  map [[md] of ?]  sorted-firm-list; money dissipation
end



to update-injection-measures [sorted-firm-list]
  set firi map [[iri] of ?] sorted-firm-list; input resources injection
  set fori map [[ori] of ?] sorted-firm-list; output resources injection
  set fmi  map [[mi] of ?]  sorted-firm-list; money injection
end



to update-child-measures [sorted-firm-list]
  set firc map [[irc] of ?] sorted-firm-list; input resouces given to child
  set forc map [[orc] of ?] sorted-firm-list; output resources given to child
  set fmc  map [[mc] of ?]  sorted-firm-list; money given to child
end



to update-parent-measures [sorted-firm-list]
  set firp map [[irp] of ?] sorted-firm-list; input resources from parent
  set forp map [[orp] of ?] sorted-firm-list; output resources from parent
  set fmp  map [[mp] of ?]  sorted-firm-list; money from parent
end



to update-price-variables
  set factor-prices  (list map [[op] of ?] (sort-on [who] firms with [output < num-factors]))
  set product-prices (list map [[op] of ?] (sort-on [who] firms with [output >= num-factors]))
end



to update-flow-variables
  ; system flow variables
  set mff sum fmsf              ; sum [mef] of firms
  set mfg sum fmd               ; sum [mg] of banks
  set mgf sum fmi
  
  ; flow of resource variables
  set rff sum forf               ; sum [irf] of firms
  set rfg sum fird + sum ford 
  set rgf sum firi + sum fori
end



to update-business-measures [sorted-firm-list]
  ; Business Measures
  
  ; If households are not accounted for, profits are zero since flows are conserved. 
  set fprofits map [[ (mef) - (msf) ] of ?] sorted-firm-list
  set fdemand  map [[demand] of ?] sorted-firm-list
end



to update-macroeconomic-measures
  ; macroeconomic outputs
;  set prev-gdp  gdp
;  set gdp       sum [msf] of firms ;money from firms to firms where household inputs are being traded.
;  set delta-gdp ifelse-value (gdp > 0) [prev-gdp / gdp - 1] [0]
  
;  set prev-rgdp  rgdp
;  set rgdp       rfh + sum [orf] of firms with [ not empty? filter [output = ?] [input] of households ] ; how many final goods were sold to firms and households
;  set delta-rgdp ifelse-value (gdp > 0) [prev-gdp / gdp - 1] [0]
  
;  set wages mfh
  
  set trade-volume sum ([orf] of firms)  ; calculated at the base price of 1
  
;  set prev-cpi cpi
;  let cpi-agents [op] of firms with [not empty? filter [output = ?] [input] of households]
;  set cpi ifelse-value (empty? cpi-agents or base-cpi = 0) [0] [(mean cpi-agents) / base-cpi]
;  set delta-cpi ifelse-value (cpi > 0) [ prev-cpi / cpi - 1 ] [0]
  
;  set prev-ppi ppi
;  let ppi-agents (sentence ([op] of firms with [not empty? filter [output = ?] [input] of firms])
;                           ([op] of households with [not empty? filter [output = ?] [input] of firms]))
;  set ppi ifelse-value (empty? ppi-agents or base-ppi = 0) [0] [(mean ppi-agents) / base-ppi]
;  set delta-ppi ifelse-value (ppi > 0) [prev-ppi / ppi - 1 ] [0]
end



to update-population-variables
  ; the firm count at this point is the number of firms alive at the start of the current step
  ; the firms killed is the number of firms that died during this current step
  set mortality-rate ifelse-value (firm-count = 0) [0] [firms-killed / firm-count]
  
  set firm-count count firms
  set active-firm-count count firms with [mef + msf > 0]
end



to create-transformation-network
  ask trans-nodes [die]
  ask trans-ties [die]
  
  foreach n-values (num-factors + num-products) [?] [
    create-trans-nodes 1 [
      set id ?
      set color lput 86 extract-rgb green
      set label ?
      set label-color red
      set shape "circle"
      set size 1.0
    ]
  ]
  
  layout-circle trans-nodes min (list world-width world-height) / 4
end



to update-transformation-network
  ask trans-ties [die]
;  ask trans-nodes [set color lput 86 extract-rgb green]
    
  let rule-dist [(list input output) ] of firms
  let rules     remove-duplicates rule-dist
  
  ;; create an edge for each rule
  foreach rules [
    let r ?
    let in-rule item 0 r
    let out-rule item 1 r
        
    ask trans-nodes with [id = in-rule] [
      create-trans-ties-to (other trans-nodes with [id = out-rule]) [
        set thickness 0.25
;        set label length filter [? = r] rule-dist
        let matching-firms firms with [input = in-rule and output = out-rule]
        set exist-weight count matching-firms
        set active-weight count matching-firms with [irf > 0 or orf > 0]  ;an agent is active if it has bought or sold resources
        set label (word exist-weight " (" active-weight ")")
        set label-color black
        set color ifelse-value (active-weight > 0) [lput 70 extract-rgb red] [lput 40 extract-rgb green]
      ]
      if in-rule = out-rule [set color gray]
    ]
  ]
end





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Simulation Runtime
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to go
  if ticks > max-run-time or (ticks > 0 and count firms = 0) [
    if record-video? [movie-close]
    stop
  ]

  ; reset the trade network 
  ask trade-ties [die]
  
  ; reset the transformation network    
  ifelse SHOW-TRANS-NETWORK? [
    update-transformation-network
    ask trans-ties [show-link]
    ask trans-nodes [show-turtle]
  ] [
    ask trans-ties [hide-link]
    ask trans-nodes [hide-turtle]
  ]
  
  reset-tick-variables

  ask firms [
    update-energy
    
    set trade-target find-target
    
    move
    trade
    produce
    
    dissipate
  ]
  
  set reproduction-energy-threshold ceiling (mean [energy] of firms)
  
;  ask firms [ dissipate ]
  ask firms [ reproduce ]
  
  ; some agents may also be selected to be incubators.
  incubate-firms 
  
  ; calculate max values
  set max-resources-bought max [irf] of firms
  set max-resources-sold   max [orf] of firms
  set max-energy           max [energy] of firms
       
  ; inject money into the system  
  inject-energy
  
    
  ; we have to update the tick variables before we kill agents so that we can account for any flows that occured prior to the agent's deaths.
  update-tick-variables  
  
;  check_flow  ;; debug check
  
  ;; The last thing agents do is check to see if they need to die. 
  ask firms [
    set color (list ceiling ((input / (num-factors + num-products)) * 255) (output / ceiling ((num-factors + num-products)) * 255) 0 255 ) ;  lput 200 extract-rgb (10 * output + (min list (orq / 100 + 2) 9.9))
    commit-suicide
  ]

  ask firms [ 
    set age age + 1
    
    update-price min-firm-price
    
    ; update demand
    set prev-demand demand
    set demand min-firm-demand
  ]
    
  update-population-variables
  
;  print (list (mfg + rfg) (mgf + rgf))

    
  if record-video?  and ticks mod grab-rate = 0[
;    ask (turtle-set firms sources banks) [set color white]
;    ask trade-ties [set color white]
    ask trans-ties [set label ""]
    movie-grab-interface ;; show the initial state
  ]
  tick
end


to incubate-firms
  ;;
  ;; Called from Observer context
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  if ticks = INCUBATION-START-TIME [
    let num-incubates incubate-fraction * firm-count
    set num-incubates ifelse-value (firm-count < num-incubates) [firm-count] [num-incubates]
    if incubation-selection = "high energy" [ set incubated-firms turtle-set sublist (sort-by [[energy] of ?1 > [energy] of ?2] firms) 0 num-incubates ] ; highest energy first
    if incubation-selection = "low energy"  [ set incubated-firms turtle-set sublist (sort-by [[energy] of ?1 < [energy] of ?2] firms) 0 num-incubates ] ; lowest energy first
    if incubation-selection = "high age"    [ set incubated-firms turtle-set sublist (sort-by [[age] of ?1 > [age] of ?2] firms) 0 num-incubates ] ; highest energy first
    if incubation-selection = "low age"     [ set incubated-firms turtle-set sublist (sort-by [[age] of ?1 < [age] of ?2] firms) 0 num-incubates ] ; lowest energy first
    if incubation-selection = "random"      [ set incubated-firms n-of num-incubates firms ]
    if incubation-selection = "producers"                [ set incubated-firms n-of num-incubates firms with [length (filter [output = ?] remove-duplicates [input] of other firms) > 0] ]
    if incubation-selection = "consumers"                [ set incubated-firms n-of num-incubates firms with [length (filter [input = ?] remove-duplicates [output] of other firms) > 0] ]
    if incubation-selection = "producers or consumers"   [ set incubated-firms n-of num-incubates (turtle-set firms with [length (filter [output = ?] remove-duplicates [input] of other firms) > 0]
                                                                                                              firms with [length (filter [input = ?] remove-duplicates [output] of other firms) > 0] )]
    if incubated-firms != nobody [
      ask incubated-firms [ set shape "square" ]
      ;      show "incubation starting"
    ]
  ]
  
  if incubate? and incubated-firms != nobody and ticks >= INCUBATION-START-TIME and ticks <= incubation-stop-time [ 
    ; exogeneous incubators
    if incubation-type = "money" [
      ask incubated-firms [
        set mq mq + external-money-quantity
        set mi mi + external-money-quantity        ; incubation of money is a money injection
        update-energy
        
        set incubated? 1
      ]
    ]
    if incubation-type = "resources" [
      ask incubated-firms [
        set irq irq + external-resource-quantity
        set iri iri + external-resource-quantity   ; incubation of resources is an input resource injection
        update-energy
        
        set incubated? 1
      ]
    ]
  ]
  
  set incubator-population ifelse-value (incubated-firms = nobody) [0] [count incubated-firms]
end



to inject-energy
  let agents firms with [agent-is-fit? firm-economic-viability = true]
  
  ask agents [ invest ]
end




to-report agent-is-fit? [fitness-criterion]  
  ; an agent should reproduce with probability var+1 / max[var]+1
  ; the + 1 is to ensure that everybody has a probability to reproduce, even if var=0. 
  let p 0  
  if fitness-criterion = "buy"          [ set p (irf + 1) / (max-resources-bought + 1) ]
  if fitness-criterion = "sell"         [ set p (orf + 1) / (max-resources-sold + 1) ]
  if fitness-criterion = "buy or sell"  [ set p (irf + 1) / (max-resources-bought + max-resources-sold + 1) + 
                                                (orf + 1) / (max-resources-bought + max-resources-sold + 1) ]
  if fitness-criterion = "buy and sell" [ set p (irf + 1) / (max-resources-bought + max-resources-sold + 1) *
                                                (orf + 1) / (max-resources-bought + max-resources-sold + 1) ]
  if fitness-criterion = "energy"       [ set p (energy + 1) / (max-energy + 1)]
  if fitness-criterion = "everyone"     [ set p 1 ]
  
  let ans random-float 1 <= p
  
  report ans
end




;;;;; movement methods
to move
  let visible-patches patches in-radius vr
  let empty-sites visible-patches with [(count firms-here) = 0 or member? myself firms-here]  ; also need to consider the site already occupied by the agent

  ifelse trade-target != nobody [      
    ; move to the empty patch within an agents vision range that is closest to the trade target
    if count empty-sites > 0 [
      let target-patch [patch-here] of trade-target
      let closest-empty-sites empty-sites with-min [distance target-patch]   ;; this bugs out if tr < vr
      let target-site one-of closest-empty-sites
      move-to target-site
;      set shape "default"
      facexy ([xcor] of trade-target) ([ycor] of trade-target)
    ]
    
    ; if, after movement, the trade target is not within the agent's transfer range, then the agents will be unable to trade.
    let nearby-agents ifelse-value (is-firm? self) [other firms in-radius tr] [firms in-radius tr]
    
    if not member? trade-target nearby-agents [
      set trade-target nobody
;      set shape "circle"
    ]
  ] 
  [
    ; if the agent has no trade target, then move to a random empty patch within the agent's vision range. 
    if count empty-sites > 0 [
      move-to one-of empty-sites
;      set shape "circle"
    ]
  ]
end

to-report find-target
  ; find the closest trade target
  let my-input input
  
  let visible-targets ifelse-value (is-firm? self) [other firms in-radius vr] [firms in-radius vr]
  
  ; select the visible trade target with the most resources I can afford limited by what I can see
;  let visible-targets (potential-targets in-radius vr)
  let desired-targets visible-targets with [output = my-input and orq > 0]
  let trade-targets   desired-targets with-max [ ifelse-value (op > 0) [ [mq] of myself / op] [orq] ]   ; we may want to alter this equation... not sure if we need the min. 
  report one-of trade-targets
end



;;;;; Trade
to trade    
  ;;;
  ;;; There needs to be a cost associated with the distance between targets. Agents dissipate 1 dollar for every 1 unit of distance the trade target is from them. Or the seller gets the money as "transportation cost"
  ;;;
  ; if you're close enough to your trade
  if (trade-target != nobody)
  [ ; we have somebody to trade with, they are close enough to trade with, and they have resources to trade
    let trade-cost [op] of trade-target
    if (mq >= trade-cost) 
    [      
      ; need to add a way for an agent to buy more than it has previous sold... like, if it has been selling constantly, then try to acquire more under the assumption that more will be bought
      let quantity-to-buy max (list min-firm-demand (prev-demand - orq - irq))     ; an agent expects to sell prev-demand resources, and already has some resources, so it only needs a few more
    
      ; an agent will buy as much as it had demand for in the last step
      let resources-traded 0
      while [([orq] of trade-target > 0) and (mq >= trade-cost) and resources-traded < quantity-to-buy] 
      [
        ; Buy a resource from the trade target
        ask trade-target [ 
          set orq orq - 1                 ; the target loses an output resource
          set mq  mq + trade-cost         ; the target gains money
          
          ; Update seller flow variables
          set orf orf + 1               ; the target sold output resources to a firm
          set mef mef + trade-cost      ; the target earned money from trade with a firm
        ]
        
        set irq (irq + 1)                 ; the buyer gains input resource
        set mq mq - trade-cost            ; the buyer loses money
        
        ;Update buyer flow variables
        set irf irf + 1                 ; the buyer bought input resource from a firm
        set msf msf + trade-cost        ; the buyer spent money on a firm
        
        set resources-traded resources-traded + 1
      ]
      
      ask trade-target [
        set demand demand + quantity-to-buy                 ; the demand of an agent is based on how much it actually sold
      ]
      
      ; create a link to the trade partner if trade actually occurred
      if resources-traded > 0 [
        create-trade-tie-with trade-target [
          set color (10 * [input] of myself) + 5
          set thickness 0.1
        ]
      ]
    ]
  ]
end



;;;;; Production
to produce
  ; There is no real reason that a firm can only produce as much as it has input resources. 
  ; We COULD allow for technology that produces more than it consumes, but we'd lose conservation.
  if irq > 0 [                      
    ;let A 1   ; A represents the technological maturity of the firm. It should change in the same way prices do. After X successful productions, it increases by a small amount.
    ;let transform-value min (list (ceiling ((A * 1 + mq) ^ 0.5 * irq ^ 0.5)) irq)
    let transform-value irq
                                        
    set irq (irq - transform-value)       ; the agent consumes X input resources
    set orq (orq + transform-value)       ; the agent produces X output resources
    
    set irt transform-value               ; the agent transformed X input resources
    set ort transform-value               ; the agent produced X output resources
  ]
end



;;;;; Dissipation
to dissipate
  ;; Dissipation removes money and resources from an agent and transfers them
  ;; to the government in order to simulate a closed system.    
  update-energy 
  
  if (energy > 0) and (random-float 1 <= dissipation-probability) [
    let total-dissipated 0
    while [ (total-dissipated < dissipation-rate) and (energy > 0) ] [   ; while dissipation is required and there is still energy to dissipate
      ;dissipate energy from a random component
      let index random 3                                                 ; select either money, input resource, or output resource to dissipate
      if index = 0 and mq > 0 [                                          ; dissipate money if the agent has any
        set mq mq - 1                                                    ; the agent dissipates a quantity of money
        set md md + 1                                                    ; the agent has dissipated money
        
        set total-dissipated total-dissipated + 1
      ]
      if index = 1 and orq > 0 [ 
        set orq orq - 1
        set ord ord + 1
        
        set total-dissipated total-dissipated + 1
      ]
      if index = 2 and irq > 0 [ 
        set irq irq - 1
        set ird ird + 1
        
        set total-dissipated total-dissipated + 1
      ]
      update-energy
    ]
  ]
end

;;;;; Invest
to invest
  if (random-float 1 <= injection-probability) [
    let total-injected 0
    while [ (total-injected < external-energy-flow) ] [ 
      let index random 2                                    
      if index = 0 [                                       
        set mq mq + 1                                                
        set mi mi + 1                                                  
        
        set total-injected total-injected + 1
      ]
      if index = 1 [ 
        set irq irq + 1
        set iri iri + 1
        
        set total-injected total-injected + 1
      ]
    ]
    update-energy
  ]
end

to reproduce
  ;; updates: firms-born
  
  update-energy

  ; agents consider a space to be empty if it either 1) has no agents or 2) any agents there will be killed at the end of the turn
;  let empty-sites patches in-radius firm-repro-radius with [count firms-here + count firms-here with [energy > 0] = 0]
  let empty-sites patches in-radius rr with [count firms-here = 0]
  let spatial-cond count empty-sites > 0
  
  if birth? and firm-count < max-firms 
            and spatial-cond
            and energy > reproduction-energy-threshold
            and (agent-is-fit? firm-reproduction-criterion) 
  [ 
    let child-rule generate-child-input-and-output
    
    let child-input item 0 child-rule
    let child-output item 1 child-rule
    let child-energy floor (energy * parent-contribution)
        
    let res-contrib floor (orq * parent-contribution)
    let m-contrib floor (mq * parent-contribution)
    
    let pwho who
    let pvr vr
    let ptr tr
    let ppst pst
    let ppcr pcr
    
    hatch-firms 1 [    
      set vr calculate-vision-radius firm-mean-vision-radius firm-sd-vision-radius
      set tr calculate-transfer-radius firm-mean-transfer-radius firm-sd-transfer-radius
      set rr firm-repro-radius
      
      if equal-radius? [
        set tr vr
        set rr vr
      ]
      
      set parent pwho
      set age 0
      
      set input  child-input
      set output child-output

      ; set op min-firm-price   ; children should base their price off the parent's price      
      set pst max (list 1 (ceiling random-normal firm-mean-price-stability firm-sd-price-stability))
      ; set pcr ppcr              ; inherit from the parent
      
      reset-flow-variables
      
      set irq res-contrib
      set orq 0
      set mq  m-contrib
      
      set irp res-contrib    ; input resources from parent
      set orp 0   ; output resources from parent
      set mp  m-contrib    ; money from parent
      
;      set spawn-probability
;      set parent-contribution
;      set mutation-probability
      set demand min-firm-demand
      
      set incubated? 0
      
      set ticks-successful 0
      set energy 0
      
      set trade-target nobody
      
      set color 10 * output + 5
;      set shape "default"   ;if we do not set this, then children of incubators are also squares. 
      set shape "square"
      
;      set heading random-float 360
;      forward 1
      move-to min-one-of empty-sites [distance myself]
      
      update-energy
    ]
    
    set orq orq - res-contrib
    set mq  mq - m-contrib
    
    set irc 0
    set orc res-contrib
    set mc  m-contrib
    
    update-energy
    
    set firms-born firms-born + 1
  ]
 
end



to-report generate-child-input-and-output
  let factors  n-values (num-factors)  [?]
  let products n-values (num-products) [num-factors + ?]
  let resources (sentence factors products)
  
  let existing-factors remove-duplicates filter [? < num-factors] [input] of firms
  let existing-products remove-duplicates filter [? >= num-factors] [input] of firms
  let existing-resources (sentence existing-factors existing-products)
  
  let child-rule (list input output)
  
  if firm-rule-format = "factor -> factor" [ 
    set child-rule generate-child-rule input factors output factors
  ]
  if firm-rule-format = "factor -> product" [ 
    set child-rule generate-child-rule input factors output products
  ]
  if firm-rule-format = "factor -> factor + product" [ 
    set child-rule generate-child-rule input factors output resources
  ]
  if firm-rule-format = "product -> factor" [ 
    set child-rule generate-child-rule input products output factors
  ]
  if firm-rule-format = "product -> product" [ 
    set child-rule generate-child-rule input products output products
  ]
  if firm-rule-format = "product -> factor + product" [ 
    set child-rule generate-child-rule input products output resources
  ]
  if firm-rule-format = "factor + product -> factor" [ 
    set child-rule generate-child-rule input resources output factors
  ]
  if firm-rule-format = "factor + product -> product" [ 
    set child-rule generate-child-rule input resources output products
  ]
  if firm-rule-format = "factor + product -> factor + product" [ 
    set child-rule generate-child-rule input resources output resources
  ]
  
  report child-rule
end


to-report generate-child-rule [_input _input-resources _output _output-resources]    
  let i _input
  let o _output
  
  if (random-float 1 < MUTATION-PROBABILITY) [
    let _mutation-option one-of [1 2 3 4 5]
    if _mutation-option = 1 [   ;i -> X
      set i _input
      set o one-of remove i _output-resources
    ]
    if _mutation-option = 2 [   ;X -> i
      set o _input
      set i one-of remove o _input-resources
    ]
    if _mutation-option = 3 [   ;o -> X
      set i _output
      set o one-of remove i _output-resources
    ]
    if _mutation-option = 4 [   ;X -> o
      set o _output
      set i one-of remove o _input-resources
    ]
    if _mutation-option = 5 [   ;X -> X
      set i one-of _input-resources
      set o one-of remove i _output-resources
    ]
  ]
  
  report (list i o)
end


to commit-suicide
  update-energy
  if death? 
     and ticks > min-lifespan 
     and energy <= 0 
  [
      set firms-killed firms-killed + 1

    die 
  ]
end



to update-energy
  set energy irq + orq + mq
end



to update-price [min-price]
  ; prices should not adjust if an agent failed to sell something because noone was in the area... but how to stop that
  ifelse orf > 0 [   ;if the firm sold something to a firm or household
    set ticks-successful ticks-successful + 1
  ] [
    set ticks-successful ticks-successful - 1
  ]
  
  if ticks-successful > pst [
    set op op + firm-price-change-rate
    set ticks-successful 0
  ]
  
  if ticks-successful < 0 - pst [
    set op max (list (op - firm-price-change-rate) min-price)
    set ticks-successful 0
  ]
end




















;; Create and update the Species Network (food web)
breed [species-nodes species-node]
species-nodes-own [
  id
]

directed-link-breed [species-ties species-tie]
species-ties-own [
  exist-weight
  active-weight
]

to create-species-network
  ask species-nodes [die]
  ask species-ties [die]
  
  foreach n-values rule-table [?] [
    create-species-nodes 1 [
      set id ?
      set color lput 86 extract-rgb green
      set label ?
      set label-color red
      set shape "circle"
      set size 1.0
    ]
  ]
  
  update-species-network
  
  layout-circle species-nodes 10
end

to update-species-network
  ask species-ties [die]
;  ask trans-nodes [set color lput 86 extract-rgb green]
    
  let rule-dist [(list input output) ] of firms
  let rules     remove-duplicates rule-dist
  
  ;; create an edge for each rule
  foreach rules [
    let r ?
    let in-rule item 0 r
    let out-rule item 1 r
        
    ask species-nodes with [id = in-rule] [
      create-species-ties-to (other species-nodes with [id = out-rule]) [
        set thickness 0.25
;        set label length filter [? = r] rule-dist
        let matching-firms firms with [input = in-rule and output = out-rule]
        set exist-weight count matching-firms
        set active-weight count matching-firms with [irf > 0 or orf > 0]
        set label (word exist-weight " (" active-weight ")")
        set label-color black
        set color ifelse-value (active-weight > 0) [lput 70 extract-rgb red] [lput 40 extract-rgb green]
      ]
      if in-rule = out-rule [set color gray]
    ]
  ]
end






to layout-trades
  repeat 100 [ layout-spring firms trade-ties 0.01 5 1 ]
 ;layout-circle trans-nodes 10
end










;;;;; run-time helper functions



to check_flow
  ;; This procedure checks to ensure that all flow variables which should be equal to one another are actuall equal. The values 
  ;; in each list printed should be identical across the respective list. 
  print ticks
  print (list  sum [msf] of firms                             sum [mef] of firms         sum fmsf   sum fmef)
  print (list (sum [md] of firms)            (sum fmd))
  print (list (sum [mi] of firms)            (sum fmi))
  
  ; flow of resource variables
  print (list  sum [orf] of firms                                  sum [irf] of firms          sum forf   sum firf)
  print (list (sum [ird] of firms + sum [ord] of firms)                      (sum fird + sum ford))
  print (list (sum [iri] of firms + sum [ori] of firms)                      (sum firi + sum fori))
end
@#$#@#$#@
GRAPHICS-WINDOW
940
10
1358
449
25
25
8.0
1
10
1
1
1
0
1
1
1
-25
25
-25
25
1
1
1
ticks
30.0

INPUTBOX
5
145
115
205
num-factors
5
1
0
Number

BUTTON
80
10
150
43
Go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
155
10
225
43
Go Once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
5
10
75
43
Setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
5
265
95
325
creation-radius
0.3
1
0
Number

INPUTBOX
365
265
455
325
firm-dissipation-rate
2
1
0
Number

INPUTBOX
275
265
365
325
firm-dissipation-probability
0.7
1
0
Number

PLOT
165
615
325
735
Output Resource Type of Firms
NIL
NIL
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 0 (num-factors + num-products + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nhistogram [output] of firms"

INPUTBOX
95
265
185
325
initial-firm-resources
0
1
0
Number

PLOT
5
735
645
855
Resource Distribution (Among Firms)
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "let i 0\nwhile [i < num-factors + num-products] [\n  let pen-name (word i)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (i * 10 + 5)\n  let os [orq] of firms with [output = i]\n  let is [irq] of firms with [input = i]\n  plot sum  (sentence os is)\n  set i i + 1\n]"
PENS

TEXTBOX
10
245
80
263
Firm Contorls
11
0.0
1

INPUTBOX
230
45
340
105
seed
1967497810
1
0
Number

SWITCH
230
10
340
43
use-seed?
use-seed?
1
1
-1000

SWITCH
5
50
225
83
SHOW-TRANS-NETWORK?
SHOW-TRANS-NETWORK?
1
1
-1000

INPUTBOX
275
325
365
385
firm-mean-vision-radius
1
1
0
Number

INPUTBOX
365
325
455
385
firm-mean-transfer-radius
1
1
0
Number

INPUTBOX
235
465
350
525
firm-mutation-probability
0.5
1
0
Number

INPUTBOX
120
465
235
525
firm-parent-contrib
0.5
1
0
Number

INPUTBOX
695
80
810
140
min-lifespan
0
1
0
Number

MONITOR
945
460
1020
505
Firms
firm-count
4
1
11

INPUTBOX
810
80
925
140
max-firms
10000
1
0
Number

PLOT
5
615
165
735
Input Resource Type Distribution
NIL
NIL
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 0 (num-factors + num-products + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nhistogram [input] of firms"

SWITCH
345
10
455
43
birth?
birth?
0
1
-1000

SWITCH
345
45
455
78
death?
death?
0
1
-1000

TEXTBOX
10
445
160
463
Firm Reproduction
11
0.0
1

INPUTBOX
185
265
275
325
initial-firm-money
0
1
0
Number

BUTTON
1185
460
1340
493
Layout By Trades
layout-trades
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
945
510
1020
555
Money
sum [mq] of firms
17
1
11

INPUTBOX
1375
120
1545
180
external-money-quantity
1
1
0
Number

INPUTBOX
1550
120
1720
180
external-resource-quantity
1
1
0
Number

INPUTBOX
1375
55
1480
115
incubate-fraction
0.1
1
0
Number

INPUTBOX
115
145
225
205
num-products
5
1
0
Number

MONITOR
1185
510
1260
555
Res. Types
count trans-nodes with [ (count out-link-neighbors) + (count in-link-neighbors) > 0]
17
1
11

MONITOR
1025
510
1100
555
Resources
sum [orq] of firms + \nsum [irq] of firms
2
1
11

MONITOR
1375
185
1442
230
Steps Left
ifelse-value (ticks >= incubation-start-time and ticks <= incubation-stop-time) [incubation-stop-time - ticks] [0]
17
1
11

INPUTBOX
1600
55
1720
115
incubation-stop-time
2000
1
0
Number

MONITOR
1105
510
1180
555
Total
sum [irq] of firms + \nsum [orq] of firms + \nsum [mq] of firms
2
1
11

INPUTBOX
590
80
695
140
max-contrib
1
1
0
Number

PLOT
1445
615
1605
735
Energy Distribution (Firms)
NIL
NIL
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if firm-count > 0 [\nplot-pen-reset  ;; erase what we plotted before\nlet data-values [energy] of firms with [energy >= 0]\nlet max-range max data-values\nset-plot-x-range 0 ( max-range  + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nset-histogram-num-bars 20\nhistogram data-values\n]"

INPUTBOX
1485
55
1595
115
incubation-start-time
1000
1
0
Number

SWITCH
1375
10
1482
43
incubate?
incubate?
0
1
-1000

CHOOSER
1485
10
1595
55
incubation-type
incubation-type
"money" "resources" "none"
1

PLOT
5
855
645
975
Technology Distribution (Firms)
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "foreach rule-table [\n  let r ?\n  let pen-name (word r)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-mode 2\n  set-plot-pen-color (item 0 r * 10 + item 1 r + 5)\n  plot count firms with [(input = item 0 r) and (output = item 1 r)]\n]"
PENS

PLOT
5
975
645
1095
Active Technology Distribution (Firms)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "foreach rule-table [\n  let r ?\n  let pen-name (word r)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-mode 2\n  set-plot-pen-color (item 0 r * 10 + item 1 r + 5)\n  plot count firms with [mef + msf > 0 and (input = item 0 r) and (output = item 1 r)]\n]"
PENS

CHOOSER
1600
10
1720
55
incubation-selection
incubation-selection
"producers" "consumers" "producers or consumers" "high energy" "low energy" "high age" "low age" "random" "none"
6

MONITOR
1105
460
1180
505
Incubates
incubator-population
17
1
11

PLOT
1285
615
1445
735
Age Distribution
NIL
NIL
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if count firms with [age > 0] > 0 [\nplot-pen-reset  ;; erase what we plotted before\nlet data-values [age] of firms with [age > 0]\nlet max-range max data-values\nset-plot-x-range 1 ( max-range  + 2)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nset-histogram-num-bars 20\nhistogram data-values\n]"

SWITCH
470
10
585
43
record-video?
record-video?
1
1
-1000

INPUTBOX
675
10
755
70
max-run-time
3000
1
0
Number

INPUTBOX
585
10
675
70
grab-rate
10
1
0
Number

INPUTBOX
755
10
925
70
movie-file-name
lowAge
1
0
String

INPUTBOX
95
325
185
385
firm-price-change-rate
1
1
0
Number

INPUTBOX
185
325
275
385
firm-mean-price-stability
10
1
0
Number

MONITOR
1025
460
1100
505
Active Firms
active-firm-count / firm-count
4
1
11

PLOT
1285
975
1605
1095
% Active Agent Population
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"F" 1.0 2 -16777216 true "" "plot ifelse-value (firm-count > 0) [active-firm-count / firm-count] [0]"

INPUTBOX
95
385
185
445
min-firm-price
1
1
0
Number

CHOOSER
5
525
350
570
firm-reproduction-criterion
firm-reproduction-criterion
"buy" "sell" "buy or sell" "buy and sell" "energy"
4

PLOT
325
1095
645
1215
Factor Prices
NIL
NIL
0.0
1.0
1.0
1.0
true
true
"" "let i 0\nwhile [i < num-factors] [\n  let pen-name (word i)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (i * 10 + 5)\n  set-plot-pen-mode 2\n  let os [op] of firms with [output = i]\n  let v 0\n  ifelse empty? os [\n    plot-pen-up\n  ] [\n    plot-pen-down\n    set v mean os\n  ]\n  plot v\n  set i i + 1\n]"
PENS

PLOT
325
1215
645
1335
Product Prices
NIL
NIL
0.0
1.0
1.0
1.0
true
true
"" "let i num-factors\nwhile [i < num-factors + num-products] [\n  let pen-name (word i)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (i * 10 + 5)\n  set-plot-pen-mode 2\n  let os [op] of firms with [output = i]\n  let v 0\n  ifelse empty? os [\n    plot-pen-up\n  ] [\n    plot-pen-down\n    set v mean os\n  ]\n  plot v\n  set i i + 1\n]"
PENS

PLOT
325
1335
645
1455
Mean Factor Price
NIL
NIL
0.0
1.0
1.0
1.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "let fs firms with [output < num-factors]\nplot ifelse-value (count fs > 0) [mean [op] of fs] [0]"

PLOT
325
1455
645
1575
Mean Product Price
NIL
NIL
0.0
1.0
1.0
1.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "let fs firms with [output >= num-factors]\nplot ifelse-value (count fs > 0) [mean [op] of fs] [0]"

PLOT
645
1335
965
1455
MFF (Money Traded)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot mff"

PLOT
965
1335
1285
1455
MFG (Money Dissipated)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot mfg"

PLOT
645
1455
965
1575
RFF (Resources Traded)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot rff"

PLOT
965
1455
1285
1575
RFG (Resources Dissipated)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot rfg"

PLOT
965
1215
1605
1335
Population Change
NIL
NIL
0.0
10.0
0.0
1.0
true
true
"" ""
PENS
"Births" 1.0 2 -16777216 true "" "plot firms-born"
"Deaths" 1.0 2 -2674135 true "" "plot firms-killed"

PLOT
5
1335
325
1455
Trade Volume
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot trade-volume"

PLOT
5
1095
325
1215
Total Production
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot sum firt"

PLOT
645
975
965
1095
Total Demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot sum [demand] of firms"

PLOT
965
735
1285
855
T Network Density
NIL
NIL
0.0
1.0
0.0
1.0
true
false
"" ""
PENS
"total" 1.0 0 -16777216 true "" "let denom count trans-nodes * count trans-nodes\nplot ifelse-value (denom = 0) [0] [(count trans-ties + count trans-nodes with [color = grey]) / denom]"
"active" 1.0 0 -2674135 true "" "let denom count trans-nodes * count trans-nodes\nplot ifelse-value (denom = 0) [0] [(count trans-ties with [color = lput 70 extract-rgb red] + count trans-nodes with [color = grey]) / denom]"

PLOT
965
975
1285
1095
Active Agent Populations
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"F" 1.0 2 -16777216 true "" "plot active-firm-count"

MONITOR
1265
510
1340
555
Arbitrage %
count firms with [input = output] / count firms
2
1
11

PLOT
965
615
1125
735
Existence Weight Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nlet xs [exist-weight] of trans-ties\nif not empty? xs [\nset-plot-x-range 0 max xs + 1 ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nset-histogram-num-bars 20\nhistogram xs\n]"

PLOT
1125
615
1285
735
Active Weight Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nlet xs [active-weight] of trans-ties\nif not empty? xs [\nset-plot-x-range 0 (max xs + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nset-histogram-num-bars 20\nhistogram xs\n]"

PLOT
965
1095
1605
1215
Total Population
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"total" 1.0 2 -16777216 true "" "plot firm-count"
"active" 1.0 2 -2674135 true "" "plot active-firm-count"

CHOOSER
245
220
455
265
firm-rule-format
firm-rule-format
"factor -> factor" "factor -> product" "factor -> factor + product" "product -> factor" "product -> product" "product -> factor + product" "factor + product -> factor" "factor + product -> product" "factor + product -> factor + product"
8

CHOOSER
485
220
645
265
firm-economic-viability
firm-economic-viability
"buy" "sell" "buy or sell" "buy and sell" "energy" "everyone"
1

PLOT
5
1455
325
1575
Total Value
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot sum [op * orq] of firms"

TEXTBOX
10
125
160
143
Resource Parameters
11
0.0
1

INPUTBOX
185
385
275
445
firm-sd-price-stability
0
1
0
Number

INPUTBOX
275
385
365
445
firm-sd-vision-radius
0
1
0
Number

INPUTBOX
365
385
455
445
firm-sd-transfer-radius
0
1
0
Number

PLOT
1285
1335
1605
1455
MGF (Money Injected)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot mgf"

PLOT
1285
1455
1605
1575
RGF (Resources Injected)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot rgf"

PLOT
645
1095
965
1215
Mean Demand
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value (count firms > 0) [mean [demand] of firms] [0]"

MONITOR
645
505
805
550
active-money
sum [mq] of firms
17
1
11

MONITOR
485
505
645
550
active-resources
sum [orq] of firms + \nsum [irq] of firms
17
1
11

PLOT
965
855
1285
975
Number of Edges
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"total" 1.0 0 -16777216 true "" "plot count trans-ties + count trans-ties with [color = grey]"
"active" 1.0 0 -2674135 true "" "plot count trans-ties with [color = lput 70 extract-rgb red] + count trans-nodes with [color = grey]"

PLOT
325
615
485
735
IRT (Active Firms)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 0 (num-factors + num-products + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nhistogram [input] of firms with [mef + msf > 0]"

PLOT
485
615
645
735
ORT (Active Firms)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 0 (num-factors + num-products + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nhistogram [output] of firms with [mef + msf > 0]"

PLOT
485
385
645
505
Active Resources
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot sum [orq] of firms + sum [irq] of firms"

INPUTBOX
5
465
120
525
firm-repro-radius
1
1
0
Number

PLOT
645
735
965
855
Resource Supply
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "let i 0\nwhile [i < num-factors + num-products] [\n  let pen-name (word i)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (i * 10 + 5)\n  let os [orq] of firms with [output = i]\n  plot sum os\n  set i i + 1\n]"
PENS

PLOT
645
855
965
975
Resource Demand
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "let i 0\nwhile [i < num-factors + num-products] [\n  let pen-name (word i)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (i * 10 + 5)\n  let d [demand] of firms with [input = i]\n  plot sum d\n  set i i + 1\n]"
PENS

INPUTBOX
5
385
95
445
min-firm-demand
1
1
0
Number

PLOT
5
1215
325
1335
Raw Number of Trades
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count trade-ties"

BUTTON
1080
560
1212
593
3-node cycle setup
setup\nset viable-resources [0]\nset viable-species [[0 1]]\nask firms [set input 0 set output 1]\nask n-of (num-firms / 3) firms [set input 1 set output 2]\nask n-of (num-firms / 3) firms [set input 2 set output 0]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1215
560
1340
593
4-node cycle setup
setup-4-cycle
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
945
560
1077
593
2-node cycle setup
setup-2-cycle
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
645
615
805
735
Resources Sold (Firms)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if count firms with [orf > 0] > 0 [\nplot-pen-reset  ;; erase what we plotted before\nlet data-values [orf] of firms with [orf > 0]\nlet max-range max data-values\nset-plot-x-range 0 ( max-range  + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nset-histogram-num-bars 10\nhistogram data-values\n]"

PLOT
805
615
965
735
Resources Bought (Firms)
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if count firms with [irf > 0] > 0 [\nplot-pen-reset  ;; erase what we plotted before\nlet data-values [irf] of firms with [irf > 0]\nlet max-range max data-values\nset-plot-x-range 0 ( max-range  + 1)  ;; + 1 to make room for the width of the last bar\nset-plot-y-range 0 1\nset-histogram-num-bars 10\nhistogram data-values\n]"

INPUTBOX
645
265
805
325
injection-probability
1
1
0
Number

PLOT
1285
735
1605
855
Injection - Dissipation
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot (mgf + rgf) - (mfg + rfg)"
"pen-1" 1.0 0 -11221820 true "" "plot 0"

PLOT
1285
855
1605
975
Dissipation vs. Injection
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"D" 1.0 2 -16777216 true "" "plot mfg + rfg"
"I" 1.0 2 -2674135 true "" "plot mgf + rgf"

PLOT
645
1215
965
1335
Total Supply
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot sum forq"

PLOT
645
385
805
505
Active Money
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot sum [mq] of firms"

TEXTBOX
490
200
580
218
Injection Consols
11
0.0
1

INPUTBOX
350
465
455
525
reproduction-energy-threshold
11
1
0
Number

PLOT
5
1575
325
1695
Buyers
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot length filter [? > 0] firf"

PLOT
325
1575
645
1695
Sellers
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot length filter [? > 0] forf"

MONITOR
1345
510
1437
555
Mortality Rate
mortality-rate
4
1
11

PLOT
645
1575
965
1695
Crude Mortality Rate
NIL
NIL
0.0
10.0
0.0
0.1
true
false
"" ""
PENS
"default" 1.0 2 -16777216 true "" "plot mortality-rate"

SWITCH
345
115
455
148
equal-radius?
equal-radius?
0
1
-1000

PLOT
965
1575
1285
1695
Mean Energy
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value (firm-count > 0) [mean [energy] of firms] [0]"

PLOT
1285
1575
1605
1695
Injection Set Size
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count firms with [iri + ori + mi > 0]"

INPUTBOX
485
265
645
325
external-energy-flow
4
1
0
Number

@#$#@#$#@
# Notes

WARNING: THIS DOCUMENT IS NOT UP TO DATE. THIS SIMULATION IS NOT UP TO DATE. PLEASE BE ADVISED THAT ECO2SIM HAS MOVED TO THE MASON FRAMEWORK AND IS NOW A JAVA/SCALA PROJECT.



In an open system, population dynamics should be much more "cyclic" and experience periods of growth and decay. In a closed system, there is an equilibrium state. 


The reproduction probability is the "natural growth rate" of the system
The mutation rate is the probability of innovation
The external flows are "exogenous investment" amounts

Add an "everyone reproduce" option

##17.1
Altered injection... completely.

Removed households. Removed the bank. Closed systems are handled by matching E_in to E_out.

The MFF variable is now a standin for the GDP. It's the money exchanged between firms in a particular tick. Likewise, RFF is the "real" GDP - the resources exchanged between firms in a particular tick.

Likewise, RFF should reflect the "Trade volume"


Options for calculating E_in...

let E_in be exactly E_out
let E_in follow a distribution (e.g. normal) with mean = E_out
let E_in be a function of the population. 

Given a value of E_in, the next question is who gets that energy. The most natural way is to simply say that every qualifying agent gets X energy, and so E(E_in) = E(X) * N. 

If E_in is a function of E_out, then the energy has to be conserved as it is split among the population... 


##16.4
Refactored some code to try and gain a performance boost. 

Changed reproduction so that the probabilty an agent reproduces depends on its relationship to other agents. E.g. current energy / max energy in the population.

Changed injection to work simialr to reproduction. 

##16.3
Smoothed out reproduction so that if X -> Y is drawn, X and Y are limited to existing resources. This mutation denotes a new connection between two possible disjoint resource sets. 

##16.2
Added a radius for reproduction such that if an agent has no empty sites within the radius it does not reproduce.

Restricted mutation to only a single rule being mutated, e.g. 4 possibilities: a -> X, b-> X, X -> a, or X -> B

Added economic viability based on species.

##16.1.1
Movement places each agent as close to its trade target as possible, without overlap.
Added options to give money to producers and consumers

##16.1
Agents are now spatiall restricted in that there can only be one agent in each patch. Reproduction places new agents on the closest empty patch to the parent.  

##V16
Demand now accounts for the quantity of resources currently possessed by an agent. 
Agents are initialized such that they do not have arbitrage rules. 

##V15.1
Added the ability for non viable firms reproduce in an order to "innovate" out of their failure. Necessity is the mother of all invention. A non-viable firm is a firm that does not buy and does not sell. 

##V15
Added a switch to make the system open by adding money/resources from an exogenous source (i.e. it doesn't get taken from the bank). Under an open system, dissipation fills the bank, but the bank is never emptied. Consequentally, under an open system there is no reason that production cannot suffer from dimishing or increasing returns (i.e. 1 input makes 2 outputs, or 2 inputs make 1 output). 

##V14.2
Vision radius, transfer radius, and price stability threshold are no longer re-seeded using the parent value as a mean. They are always created as a random number with the parameterized mean and standard deviation. 

##V14.1
Rule format and who gets money/resource injection has been parameterized to remove hard-coded restrictions put in place for V14.0. 

##V14.0
Households reproduce if they are successful buyers
Households dissipate
Households receive injections if they are successful buyers
Household input is restricted to products
Household output is restricted to factors

Firms reproduce if they are successful sellers
Firms dissipate
Firms injections if they are successful sellers
Firm input is not restricted
Firm output is not restricted

# WHAT IS IT?
This is a model of resource flow over a dynamic interaction/trade network. There are three types of nodes, firms, sources, and the central bank. Source nodes are given resources at a parameterized rate from the bank. Firms acquire resources form either a source or another firm and transform those resources. Transformed resources are held until another firm comes along to take them. The bank buys excess source resources from firms at a fixed, parameterized, price. firms have a parameterized probability of resource decay. When a resource decays it is moved from the firm to the bank at 0 cost.

This model can also be thought of as a closed model of resource flow and transformation on a randomized system. You put a bunch of agents in a box, shake it up, dump it out, then watch as firms hunt for resources and observe how those resources end up being distributed as the agents fight over them. 

# HOW IT WORKS

This model is based on the assumption that every economic entity can be abstracted as a "resource transformer." This implicitly assumes that firms, households, and banks are essentially the same. 

In total, there are _n_ firms and _s_ source nodes in the simulation. These agents are distributed uniformly over the landscape. Firms within a circle of radius _vision range_ are able to see one another. Firms that can see one another are connected by an implicit interaction network. Firms are only allowed to interact with their immediate neighbors in the interaction network (e.g. those agents they can see).

The interaction network changes as firms move across the landscape in search of resources. Resources can be found either within a source node (scattered across the landscape in fixed locations) or other firms. Resources are acquired at a cost to the firm, as determined by the user. These costs do not change once the simulation starts. Once a firm has resources, it then transforms them and the new resources available for purchase.

Any resources of the same type as those produced by source nodes are subject to buyback from the bank. Every turn, the bank searches the population for resources that match those produced by source nodes, and upon finding them buys up to two units back at a fixed cost. This method transfers money stored in the central bank back into the population. It enables the system to keep flowing, but only so long as the system is economically viable (for comparison, nobody would ever die if there was no dissipation.) Without putting money back into the system the population crashes once all the money is in the bank.)

Firms die when the run out of energy - the combination of money and resources. To compensate for death, agents reproduce when their energy exceeds a parameterized threshold. New agents copy the attributes of their parents, but there is a probability that their transformation rule will undergo mutation. This reproduction method is also how we introduce additional diversity into the population. 

## Agents
Agents can be either a **firm** or a **source**. (There is only one **bank** and it is special.)
### Firms (Resource Transformers)

 
![Adaptive Resource Transformer](figures/art_diagram.png)

Firms are the basic economic agents in the simulation. They attempt to acquire specific resources through interaction in an environment and then transform those resources in accordance with a **resource transformation rule**. 

Each firm contains a store of input resources and a store of output resources. The quantity in each of these stores is given by the _input-quantity_ and _output-quantity_. Firms start with an initial quantity of input resources equal to the _initial-firm-quantity_ parameter. Firms also contain _money_, and start with an amount equal to the _initial-money_ parameter. 

Firms obtain input resources via the **trade process**. A firm (the "buyer") examines every neighbor (on the interaction network) and identifies the subset of neighbors that contain positive quantities of output resources that match the buyer's input resource. The neighbor who offers the most resources for purchase, at a price the firm can afford, is selected as the "trade target". The trade target can be either a source or another firm. After a trade target has been selected by the buyer, resources flow between the two agents in a quantity determined by the _firm-cost_ (or _source-cost_) and the amount of _money_ the firm has.

Resources are moved from the input store to the output store via the **production process**. The production rule is guided by a firm's resource transformation rule. The _transform-quantity_ specifies the maximum number of resources that can be transformed during each tick. All transformations occur in a one-toone ratio. Once resources have been moved to the output store, they remain there until interaction with another firm removes them and/or they are lost through the **dissipation process**. 

The sum of a firm's input resources, output resources, and money make up the _energy_. At each tick, _dissipation_rate_ units of energy are lost with a probability of _dissipation_probability_. The energy lost is selected at random from a firm's input resources, output resources, or money. The lost energy is placed in the bank for redistribution through the population.

### Firm Behaviors
Firms execute three specific behaviors: **trade**, **production**, and **dissipation**
#### Trade
Trade is the process by which firms acquire resources. A firm can either trade with a source or another firm.

The trade process is defined as:
  1. The buyer selects a trade target
  2. Resources are transferred from the trade target to the buyer

Trade target selection
>  1. The buyer makes a list of all neighboring agents.
>  2. For each neighbor, the buyer calculates how many resources it can buy (restricted by quantity and money)
>  3. The buyer picks the neighbor which offers it the most resources

Resource transfer
>  1. The trade target removes the resources from its store of output resources
>  2. The buyer adds the resources to its store of input resources.

The trade process can be compacted into the following **trade rule**: Select a trade target based on your transformation rule and then acquire as many resources from it as you can afford.

#### Production
Production is the process of converting input resources to output resources. 

The production process is defined as:
1. Subtract _transform-quantity_ resources from the input store
2. Add _transform-quantity_ resources to the output store.

The production process can be more compactly stated with the following **production rule**: Transform _transform-quantity_ input resources into _transform-quantity_ output resources.

#### Dissipation
Dissipation is the process of resource loss. From a systems point of view it is the counterpoint to resource generation by a source. The semantics of resource loss are application specific; e.g. metabolism or spoilage. 

The dissipation process is defined as:
1. Generate a random number _x_ between 0 and 1. 
2. If _x < _dissipation-probability_ and the firm has resources to lose
    a. Remove _dissipation-rate_ units of energy from the firm, selected at random from input-resources, output-resources, or energy.
    b. Give whatever was lost to the bank in the quantity lost.

The dissipation process can be more compactly stated with the following **dissipation rule**: Lose _dissipation-rate_ energy with a probably _dissipation-probability_, and give the lost energy components to the bank.

#### Death
When an agent runs out of energy it dies and is removed from the system. 

#### Reproduction
When the output resources of an agent exceed the **spawn-threshold** then an agent creates a clone with a probability of _spawn-probability_. This clone has zero output resources and zero money. It's transformation rules are identical to the parent agent with a probability (1 - **mut-probability**)^2. A child agent receives an amount of input resource equal to **parent-contrib** * **output-resources** (of the parent).

### Sources

 
![Source](figures/source_diagram.png)

A source represents an entry point for resources into the system. Each source is capable of storing one type of resource. The quantity of resource available depletes as firms take the resources but regenerates at a rate specified by the simulation parameter _source-regen-rate_.

## The Environment
 
![Level 0 Environment](figures/art_network.png)

Agents are connected by a (dynamic) random geographic network with a radius of _vision-radius_. This graph imposes a notion of bounded rationality that restricts the information available to agent decision making to the local neighborhood; where the local neighborhood consists of only those agents within one degree (one hop).

# HOW TO USE IT

## Parameters
**num-resources** specifies the number of resources that exist in the system at creation
**max-resources** specifies the maximum number of resources that can ever exist in the system

**firm-cost** specifies the price firms pay to other firms to a resource
**source-cost** specifies the price firms pay to sources for a resource
**bank-cost** specifies the price the bank pays to firms for a resource

**use-seed?** if set to â€œonâ€ then a user defined seed is used instead of the system defined seed
**seed** specifies the user defined seed for the random number generator

**num-sources** specifies how many sources exist in the system
**initial-source-quantity** specifies the quantity of resource each sink has a tick 0
**source-regen-rate** specifies the quantity of resource a source gains at the start of each tick (resources/tick)

**num-firms** specifies the number of firms in the system at tick 0
**initial-firm-quantity** specifies the quantity of input resources a firm has a tick 0
**transfer-rate** specifies the maximum quantity of resources that can be acquired from other agents at each tick (resources/tick)
**transfer-all?** if â€œonâ€ then an agent will attempt to acquire every output resource that a trade target has
**transform-rate** specifies the maximum quantity of resources that an agent can transform in a tick (resources/tick)
**transform-all?** if â€œonâ€ then an agent will attempt to transform every input resource it has
**output-loss-probability** specifies the probability that a firm will lose output resources to dissipation
**output-loss-rate** specifies the maximum quantity of output resources that can be lost to dissipation
**lose-all-output?** if â€œonâ€ then a firm can dissipate all of its output resources
**input-loss-probability** specifies the probability that a firm will lose input resources to dissipation
**input-loss-rate** specifies the maximum quantity of input resources that can be lost to dissipation
**lose-all-input?** if â€œonâ€ then a firm can dissipate all of its input resources
**vision-radius** specifies how many patches out (in a circle) the agent can detect other agents
**transfer-radius** specifies how many patches out (in a circle) the agent can trade with other agents
**movement-range** specifies how many patches an agent can move in a single tick
**initial-money** specifies how much money each agent starts with

**spawn-threshold** specifies the minimum level of output resources required by a firm to reproduce
**spawn-probability** specifies the probability that each firm will reproduce, if qualified
**parent-contrib** specifies the percentage of resource transfered from parent to child
**mut-probability** specifies the probability that the input and output of a firm will change during reproduction
**min-lifespan** specifies the minimum lifespan for the initial population; this is used to ensure a minimal warm-up period
**max-firms** specifies the maximum number of firms that can exist in the world

**arbitrage?** if set to "on" then agents can have output = input, if set to "off" then an agent's input is different from its output. 
**distributions?** if set to â€œonâ€ then the _source-regen-rate_, _output-loss-rate_, and _input-loss-rate_ are treated as Poisson random variables with the specified value acting as the mean
**restrict-movement?** if set to "on" then agents make an additional random move if their destination patch is occupied. This only reduce overlap, it does not prevent it.

## Output

# THINGS TO NOTICE


## Stylized facts

An apparently power-law distributed firm age and wealth distribution.

# EXTENDING THE MODEL

Adaptive pricing and production efficiency. Supplementary and complementary resources. 

# EXPERIMENTAL IDEAS

Use the manual random seed and change each parameter to see how the system responds without the extra unput of random variability. 

# NETLOGO FEATURES


# RELATED MODELS

Related to sugarscape, resource decay is a stochastic metabolism. Production is the concept of turning sugar into spice (this is not done in sugarscape). Trade makes use of money, which is treated as a special resource in that it cannot be transformed in and of itself.


# CREDITS AND REFERENCES
  1. Epstein, J. M., & Axtell, R. L. (1996). Growing Artificial Societies: Social Science from the Bottom Up (First.). The MIT Press.
  2. Hollander, C. D., Garibay, I., & O'Neal, T. (2012). Transformation Networks: A study of how technological complexity impacts economic performance. 8th Artificial Economics. Castellon, Spain: Springer-Verlag.
  3. Hannon, B. (1973). The structure of ecosystems. Journal of Theoretical Biology, 41(3), 535-546.
  4. Hirata, H., & Ulanowicz, R. e. (1984). Information theoretical analysis of ecological networks. International Journal of Systems Science, 15(3), 261-270.
  5. Ulanowicz, R. E. (1983). Identifying the structure of cycling in ecosystems. Mathematical Biosciences, 65(2), 219-237.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="open incubation" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3690"/>
    <metric>firm-count</metric>
    <metric>active-firm-count</metric>
    <metric>firms-born</metric>
    <metric>firms-killed</metric>
    <metric>firm-ids</metric>
    <metric>fage</metric>
    <metric>finput</metric>
    <metric>foutput</metric>
    <metric>fop</metric>
    <metric>fmq</metric>
    <metric>firq</metric>
    <metric>forq</metric>
    <metric>firf</metric>
    <metric>fmsf</metric>
    <metric>forf</metric>
    <metric>fmef</metric>
    <metric>fort</metric>
    <metric>firi</metric>
    <metric>fmi</metric>
    <metric>factor-prices</metric>
    <metric>product-prices</metric>
    <metric>fprofits</metric>
    <metric>fincubated</metric>
    <metric>mff</metric>
    <metric>mfg</metric>
    <metric>mgf</metric>
    <metric>rff</metric>
    <metric>rfg</metric>
    <metric>rgf</metric>
    <metric>trade-volume</metric>
    <enumeratedValueSet variable="injection-type">
      <value value="&quot;open&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate-fraction">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-selection">
      <value value="&quot;low age&quot;"/>
      <value value="&quot;high age&quot;"/>
      <value value="&quot;random&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-type">
      <value value="&quot;money&quot;"/>
      <value value="&quot;resources&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seed">
      <value value="910207765"/>
      <value value="50784569"/>
      <value value="1080292177"/>
      <value value="-692395877"/>
      <value value="1079278928"/>
      <value value="1079605230"/>
      <value value="2008332414"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="use-seed?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="open no-incubation" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3690"/>
    <metric>firm-count</metric>
    <metric>active-firm-count</metric>
    <metric>firms-born</metric>
    <metric>firms-killed</metric>
    <metric>firm-ids</metric>
    <metric>fage</metric>
    <metric>finput</metric>
    <metric>foutput</metric>
    <metric>fop</metric>
    <metric>fmq</metric>
    <metric>firq</metric>
    <metric>forq</metric>
    <metric>firf</metric>
    <metric>fmsf</metric>
    <metric>forf</metric>
    <metric>fmef</metric>
    <metric>fort</metric>
    <metric>firi</metric>
    <metric>fmi</metric>
    <metric>factor-prices</metric>
    <metric>product-prices</metric>
    <metric>fprofits</metric>
    <metric>fincubated</metric>
    <metric>mff</metric>
    <metric>mfg</metric>
    <metric>mgf</metric>
    <metric>rff</metric>
    <metric>rfg</metric>
    <metric>rgf</metric>
    <metric>trade-volume</metric>
    <enumeratedValueSet variable="injection-type">
      <value value="&quot;open&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate-fraction">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-selection">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-type">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seed">
      <value value="910207765"/>
      <value value="50784569"/>
      <value value="1080292177"/>
      <value value="-692395877"/>
      <value value="1079278928"/>
      <value value="1079605230"/>
      <value value="2008332414"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="use-seed?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="closed no-incubation" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3690"/>
    <metric>firm-count</metric>
    <metric>active-firm-count</metric>
    <metric>firms-born</metric>
    <metric>firms-killed</metric>
    <metric>firm-ids</metric>
    <metric>fage</metric>
    <metric>finput</metric>
    <metric>foutput</metric>
    <metric>fop</metric>
    <metric>fmq</metric>
    <metric>firq</metric>
    <metric>forq</metric>
    <metric>firf</metric>
    <metric>fmsf</metric>
    <metric>forf</metric>
    <metric>fmef</metric>
    <metric>fort</metric>
    <metric>firi</metric>
    <metric>fmi</metric>
    <metric>factor-prices</metric>
    <metric>product-prices</metric>
    <metric>fprofits</metric>
    <metric>fincubated</metric>
    <metric>mff</metric>
    <metric>mfg</metric>
    <metric>mgf</metric>
    <metric>rff</metric>
    <metric>rfg</metric>
    <metric>rgf</metric>
    <metric>trade-volume</metric>
    <enumeratedValueSet variable="injection-type">
      <value value="&quot;closed&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate-fraction">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-selection">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-type">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seed">
      <value value="910207765"/>
      <value value="50784569"/>
      <value value="1080292177"/>
      <value value="-692395877"/>
      <value value="1079278928"/>
      <value value="1079605230"/>
      <value value="2008332414"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="use-seed?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="closed incubation" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="3690"/>
    <metric>firm-count</metric>
    <metric>active-firm-count</metric>
    <metric>firms-born</metric>
    <metric>firms-killed</metric>
    <metric>firm-ids</metric>
    <metric>fage</metric>
    <metric>finput</metric>
    <metric>foutput</metric>
    <metric>fop</metric>
    <metric>fmq</metric>
    <metric>firq</metric>
    <metric>forq</metric>
    <metric>firf</metric>
    <metric>fmsf</metric>
    <metric>forf</metric>
    <metric>fmef</metric>
    <metric>fort</metric>
    <metric>firi</metric>
    <metric>fmi</metric>
    <metric>factor-prices</metric>
    <metric>product-prices</metric>
    <metric>fprofits</metric>
    <metric>fincubated</metric>
    <metric>mff</metric>
    <metric>mfg</metric>
    <metric>mgf</metric>
    <metric>rff</metric>
    <metric>rfg</metric>
    <metric>rgf</metric>
    <metric>trade-volume</metric>
    <enumeratedValueSet variable="injection-type">
      <value value="&quot;closed&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubate-fraction">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-selection">
      <value value="&quot;low age&quot;"/>
      <value value="&quot;high age&quot;"/>
      <value value="&quot;random&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="incubation-type">
      <value value="&quot;money&quot;"/>
      <value value="&quot;resources&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seed">
      <value value="910207765"/>
      <value value="50784569"/>
      <value value="1080292177"/>
      <value value="-692395877"/>
      <value value="1079278928"/>
      <value value="1079605230"/>
      <value value="2008332414"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="use-seed?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="sweep 1" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="1000"/>
    <metric>firm-count</metric>
    <metric>active-firm-count</metric>
    <metric>firms-born</metric>
    <metric>firms-killed</metric>
    <metric>firm-ids</metric>
    <metric>fage</metric>
    <metric>finput</metric>
    <metric>foutput</metric>
    <metric>fop</metric>
    <metric>fmq</metric>
    <metric>firq</metric>
    <metric>forq</metric>
    <metric>firf</metric>
    <metric>fmsf</metric>
    <metric>forf</metric>
    <metric>fmef</metric>
    <metric>fort</metric>
    <metric>firi</metric>
    <metric>fmi</metric>
    <metric>factor-prices</metric>
    <metric>product-prices</metric>
    <metric>fprofits</metric>
    <metric>fdemand</metric>
    <metric>fincubated</metric>
    <metric>mff</metric>
    <metric>mfg</metric>
    <metric>mgf</metric>
    <metric>rff</metric>
    <metric>rfg</metric>
    <metric>rgf</metric>
    <metric>trade-volume</metric>
    <metric>mortality-rate</metric>
    <enumeratedValueSet variable="use-seed?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seed">
      <value value="904397871"/>
      <value value="1809879741"/>
      <value value="1514699718"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="injection-probability">
      <value value="0.285"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="external-money-flow">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="external-resource-flow">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-factors">
      <value value="1"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-products">
      <value value="1"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-firms">
      <value value="50"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-firm-money">
      <value value="1"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-firm-resources">
      <value value="1"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-dissipation-probability">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-dissipation-rate">
      <value value="0.1"/>
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-economic-viability">
      <value value="&quot;buy or sell&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-mean-price-stability">
      <value value="1"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-mean-vision-radius">
      <value value="1"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-reproduction-criterion">
      <value value="&quot;energy&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-mutation-probability">
      <value value="0.1"/>
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-parent-contrib">
      <value value="0.1"/>
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reproduction-energy-threshold">
      <value value="1"/>
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-rule-format">
      <value value="&quot;factor + product -&gt; factor + product&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-price-change-rate">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="injection-type">
      <value value="&quot;open&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-firm-demand">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-firm-price">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-dissipation?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-repro-radius">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-mean-transfer-radius">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-sd-price-stability">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-sd-transfer-radius">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="firm-sd-vision-radius">
      <value value="0"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
1
@#$#@#$#@
