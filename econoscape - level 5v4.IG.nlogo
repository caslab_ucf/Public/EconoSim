;; Input values need to correspond to distribution means, not exact values. 

;__includes ["banks.nls" firms.nls" "sources.nls" "trans.nls"]

globals [
  
  ; cumulative measures
  cumul-trades
  cumul-production
  
  cumul-dissipation            ; how many resources have the firms dissipated since t=0
  
  cumul-source-input           ; how many resources have the sources produced since t=0
  
  cumul-gini
  cumul-input-gini
  cumul-output-gini
  
  cumul-total-resources
  

  ; per step measures
  gdp
  prev-gdp

  trades
  production

  dissipation
  
  source-input
  
  energy-gini-index
  
  total-resources
  
  living-incubates


  ; network measures
  interaction-network-density
  
  
  ; utility measures
  living-firm-count
  living-incubated-count
  
  incubation-time-remaining
  incubation-firms
  
  ; population measures
  wealth-dist
  rule-dist
]

;;;;; Agent definitions

breed [sources source]

sources-own [
  ;; fields
  output
  output-resources
  
  regen-rate
  
  energy
  
  ;; methods
  ; regenerate
]



breed [banks bank]

banks-own [
  money
  resources   ;we want to eventually tie this into sources so that their resources come from the central bank
  
  ;; methods
  ; incubate-money
  ; incubate-resources
]

to shock-money
  let total-funds shock-quantity * external-money-quantity
  if [money] of bank 0 >= total-funds [
    ask n-of shock-quantity firms [
      set money money + external-money-quantity
    ]
    ask bank 0 [
      set money money - total-funds
    ]
  ]
end

to shock-resources
  let total-funds shock-quantity * external-resource-quantity
  if [money] of bank 0 >= total-funds [
    ask n-of shock-quantity firms [
      set input-resources input-resources + external-resource-quantity
    ]
    ask bank 0 [
      set resources resources - total-funds
    ]
  ]
end



breed [firms firm]

firms-own [
  ;; fields
  input
  output
  input-resources
  output-resources
  money
  
  energy
  age
  
  trade-target
  active?
  
  ;; network measures
  betweenness
  closeness
  degree
  eigenvector
  
  ;; methods
  ; move        has the side effect of selecting the trade target and repositioning the agent
  ; trade
  ; produce
  ; dissipate
]

;;;;; movement methods
to move
  set trade-target find-target
  ifelse trade-target != nobody [
    repeat vision-radius [
      if not member? trade-target (turtle-set (other firms in-radius transfer-radius) (sources in-radius transfer-radius)) [
        reposition ([xcor] of trade-target) ([ycor] of trade-target)
      ]
    ]
  ] [
    let target patch-at-heading-and-distance (random-float 360) vision-radius
    repeat vision-radius [reposition ([pxcor] of target) ([pycor] of target) ]
  ]
end

to-report find-target
  ; only look at nodes within my vision radius
  let my-input input
    
  let ns (((turtle-set other firms sources) in-radius vision-radius) 
    with [output = my-input and output-resources > 0])
    with-max [(ifelse-value (is-source? self) 
                [ ;the trade target is a source
                  ifelse-value (source-cost > 0) [min (list ([money] of myself / source-cost) output-resources)] [output-resources]]
                [ ; the trade target is a firm
                  ifelse-value (firm-cost > 0) [min (list ([money] of myself / firm-cost) output-resources)] [output-resources] ])]  ; you can't gain more than the target has
  
  report one-of ns
end

to reposition [tx ty]
  facexy tx ty
  forward 1
end



;;;;; Trade
to trade   
  update-energy
  ; if you're close enough to your trade
  if (trade-target != nobody) ;and 
;    (member? trade-target (turtle-set other firms in-radius transfer-radius sources in-radius transfer-radius))   ; this should be guaranteed unless movement range < vision radius
    [ ; we have somebody to trade with, they are close enough to trade with, and they have resources to trade
      let trade-cost ifelse-value (is-firm? trade-target) [firm-cost] [source-cost]
              
;      if money < trade-cost and [money] of bank 0 > trade-cost [
;        set money money + trade-cost
;        ask bank 0 [
;          set money money - trade-cost
;        ]
;      ]
      if (money >= trade-cost) 
      [
        ; create a link to the trade partner
        create-trade-tie-with trade-target [
          set color (10 * [input] of myself) + 5
          set thickness 0.1
        ]
      
      
        let trade-count 0
        while [([output-resources] of trade-target > 0) and
               (money >= trade-cost)] 
        [
          ; Buy a resource from the trade target
          ask trade-target [
            set output-resources (output-resources - 1)
          ]   
          set input-resources (input-resources + 1)
        
          if is-firm? trade-target [ask trade-target [ 
              set money money + trade-cost
              set active? true 
          ]]
          if is-source? trade-target [ask bank 0 [ set money money + trade-cost ]]
          
          set money money - trade-cost
          set active? true

          set gdp gdp + trade-cost

          set trade-count trade-count + 1
        ]
      
        ; update global counters
        set cumul-trades (cumul-trades + 1)
        set trades (trades + 1)
      ]
    ]
    
    update-energy
end



;;;;; Production
to produce
  update-energy
  if input-resources > 0 [
    let transform-value ifelse-value transform-all? 
                                    [ input-resources ] 
                                    [ min (list input-resources transform-rate) ]
                                    
    set input-resources (input-resources - transform-value)
    set output-resources (output-resources + transform-value)
    
    ; update global counters
    set cumul-production (cumul-production + transform-value)
    set production (production + transform-value)
  ]
  update-energy
end



;;;;; Dissipation
to dissipate
  update-energy
  
  if (energy > 0) and (random-float 1 < dissipation-probability) [
    ;dissipate by removing an equal amount from all energy contributors
    let total-dissipated 0
    while [ (total-dissipated <= dissipation-rate) and (energy > 0) ] [
      ;dissipate energy from a random component
      let index random 3
      if index = 0 and money > 0 [ 
        set money money - 1
        ask bank 0 [
          set money money + 1
        ]
        set total-dissipated total-dissipated + 1
      ]
      if index = 1 and output-resources > 0 [ 
        set output-resources output-resources - 1
        ask bank 0 [
          set resources resources + 1
        ]
        set total-dissipated total-dissipated + 1
      ]
      if index = 2 and input-resources > 0 [ 
        set input-resources input-resources - 1
        ask bank 0 [
          set resources resources + 1
        ]
        set total-dissipated total-dissipated + 1
      ]

      update-energy
    ]
    
    set cumul-dissipation (cumul-dissipation + total-dissipated)
    set dissipation (dissipation + total-dissipated)
  ]
end

to update-energy
  set energy input-resources + output-resources + money
end




undirected-link-breed [trade-ties trade-tie]      ; the trade ties are the realized trade network at step t
undirected-link-breed [hidden-ties hidden-tie]    ; the hidden ties are the potential trade network at step t

breed [trans-nodes trans-node]
trans-nodes-own [
  id
]

directed-link-breed [trans-ties trans-tie]

; create-transformation-network
; update-transformation-network
to create-transformation-network
  ask trans-nodes [die]
  ask trans-ties [die]
  
  foreach n-values max-resources [?] [
    create-trans-nodes 1 [
      set id ?
      set color lput 86 extract-rgb green
      set label ?
      set label-color red
      set shape "circle"
      set size 1.0
    ]
  ]
  
  update-transformation-network
  
  layout-circle trans-nodes 13
end

to update-transformation-network
  ask trans-ties [die]
  ask trans-nodes [set color lput 86 extract-rgb green]
    
  let rules []
  ask firms [
    set rules lput (list input output) rules
  ]
  
  set rule-dist rules

  ;; reduce the list rules down to a unique set
;  set rules reduce [ifelse-value member? ?2 ?1 [?1] [fput ?2 ?1]] (fput [] rules)
  set rules remove-duplicates rules
  
  ;; create an edge for each rule
  foreach rules [
    let r ?
    let in-rule item 0 r
    let out-rule item 1 r
        
    ask trans-nodes with [id = in-rule] [
      create-trans-ties-to (other trans-nodes with [id = out-rule]) [
        set thickness 0.25
;        set label length filter [? = r] rule-dist
        let matching-firms firms with [input = in-rule and output = out-rule]
        let exist-weight count matching-firms
        let active-weight count matching-firms with [active?]
        set label (word exist-weight " (" active-weight ")")
        set label-color black
        set color ifelse-value (active-weight > 0) [lput 70 extract-rgb red] [lput 40 extract-rgb green]
      ]
      if in-rule = out-rule [set color gray]
    ]
  ]
end




;;;;; simulation setup
to setup
  clear-all
  set seed ifelse-value use-seed? [seed] [new-seed]
  random-seed seed
  
  ask patches [set pcolor white]
  
  create-banks 1 [
    set money bank-money
    set resources bank-resources
    setxy 0 0
    set shape "square"
    set size 2
    set color white
  ]
  
  ;; Generate a random graph of num-sources + num-firms nodes based on firms
  create-firms num-firms [
    ;; initialize each agent with default values
    set input random num-resources
    set output random num-resources
    if (not arbitrage? and output = input) [set output (output + 1) mod num-resources]
    set input-resources initial-firm-quantity
    set output-resources 0
    set money initial-money
    
    update-energy
    set age 0
    
    set color 10 * input + 5
    
    setxy random-xcor random-ycor
  ]
  
  ;; Set num-sources randomly selected nodes to be of breed Source
  create-sources num-sources [
    set output random max-resources
    set output-resources initial-source-quantity
    set regen-rate source-regen-rate
    
    set color 10 * output + 5
    set shape "circle" 
      
    setxy random-xcor random-ycor
  ]
  
  ask links [ hide-link ]
  
  set incubation-firms nobody
  
  reset-tick-variables
  update-globals
  update-lorenz-and-gini
  
  create-transformation-network
  
  reset-ticks
end


to layout-trades
  repeat 100 [ layout-spring (turtle-set sources firms) trade-ties 0.01 5 1 ]
 ;layout-circle trans-nodes 10
end

to layout-hidden
  repeat 100 [ layout-spring (turtle-set sources firms) hidden-ties 0.01 5 1 ]
 ;layout-circle trans-nodes 10
end





;;;;; run-time behavior
to go
  if ticks > 0 and (count firms = 0 or (all? firms [output-resources = 0 and input-resources = 0 and money = 0] and all? sources [source-regen-rate = 0]))
    [stop]
  
 
  ask trade-ties [die]
  ask hidden-ties [die]
  
  update-transformation-network
  
  reset-tick-variables
  
  ifelse show-trans-network? [
    ask trans-ties [show-link]
    ask trans-nodes [show-turtle]
  ] [
  ask trans-ties [hide-link]
  ask trans-nodes [hide-turtle]
  ]
   
  ask sources [
    let regen-value regen-rate
    if [resources] of bank 0 > regen-value and output-resources <= 0 [
      set output-resources (output-resources + regen-value)
      ask bank 0 [
        set resources (resources - regen-value)
      ]
      ;update counters
      set cumul-source-input (cumul-source-input + regen-value)
      set source-input (source-input + regen-value)
    ]
  ]
  
  ask firms [    
    move
    trade
    produce
  ]
  
  ask firms [
    ; because dissipation occurs AFTER all firms have acted, it means that you cannot have the case
    ; where trade is disrupted because an output resource that was just created up and vanishes. 
    dissipate
    
    set color lput 200 extract-rgb (10 * output + (min list (output-resources / 100 + 2) 9.9))
    
    ;    create-hidden-ties-with (turtle-set firms-on neighbors sources-on neighbors) [
    create-hidden-ties-with (turtle-set (other firms in-radius transfer-radius) (sources in-radius transfer-radius)) [
      set color green
      if not show-hidden-ties? [ hide-link ]
    ]
    
    set age age + 1
  ]
  
  if death? and ticks > min-lifespan [
    ask firms with [energy <= 0] [ 
      die 
    ]
  ]
  
  ; reproduce
  if birth? [
    ask (firms with [energy > spawn-threshold]) [
      update-energy
      if living-firm-count < max-firms [
        if random-float 1 < spawn-probability [          
          let child-input ifelse-value (random-float 1 < mut-probability) [random max-resources] [input]
          let child-output ifelse-value (random-float 1 < mut-probability) [random max-resources] [output]

;          let child-input ifelse-value (random-float 1 < mut-probability) [random max-resources] [output]
;          let child-output random max-resources


          let child-energy floor (energy * parent-contrib)
          let in-contrib 0
          let out-contrib 0
          let money-contrib 0
          
          ifelse child-input = input 
          [ ifelse child-output = output 
            [ ; child has the input and output of the parent
              set in-contrib    floor (input-resources * parent-contrib)
              set out-contrib   floor (output-resources * parent-contrib)
              set money-contrib floor (money * parent-contrib)]
            [ ; child has the input of the parent but not the output
              let engy          input-resources + money
              let contrib       ifelse-value (engy != 0) [child-energy / engy] [0]
              
              if contrib > max-contrib [ set contrib max-contrib ]
              
              set in-contrib    floor (input-resources * contrib)
              set money-contrib floor (money * contrib)
              ]]
          [ ifelse child-output = output 
            [ ; child does not have the input of the parent but has the output
              let engy          output-resources + money
              let contrib       ifelse-value (engy != 0) [child-energy / engy] [0]
              
              if contrib > max-contrib [ set contrib max-contrib ]
              
              set out-contrib   floor (output-resources * contrib)
              set money-contrib floor (money * contrib)
            ]
            [ ; child does not have the input or the output of the parent
              let engy          money
              let contrib       ifelse-value (engy != 0) [child-energy / engy] [0]
              
              if contrib > max-contrib [ set contrib max-contrib ]
              
              set money-contrib floor (money * contrib)
            ]
          ]
          
          hatch-firms 1 [
            set input  child-input
            set output child-output
            if (not arbitrage? and output = input) [set output (output + 1) mod max-resources]
            
            set input-resources  in-contrib
            set output-resources out-contrib
            set money            money-contrib
            
            update-energy
            set age 0
            
            set color 10 * input + 5
            set shape "default"   ;if we do not set this, then children of incubators are also squares. 
            
            set heading random-float 360
            forward 1
            
            set living-firm-count living-firm-count + 1
          ]
          
          set input-resources  input-resources - in-contrib
          set output-resources output-resources - out-contrib
          set money            money - money-contrib
          update-energy
        ]
      ]
    ]
  ]
  
  ; the bank buys up resources
  ask bank 0 [
    let source-resources remove-duplicates [output] of sources
    
    ask firms [
      ; if the input of the firm is desired by the bank
      if length (filter [? = input] source-resources) > 0 and input-resources > 0 [
        ; and the bank has money to buy iy
        if [money] of bank 0 >= bank-cost [
          set input-resources input-resources - 1
          
          ; buy it
          set money money + bank-cost
          ask bank 0 [
            set resources resources + 1
            set money money - bank-cost
          ]
        ]
      ]
      
      ;if the output is desired by the bank
      if length (filter [? = output] source-resources) > 0 and output-resources > 0 [
        ; and the bank has money to buy it
        if [money] of bank 0 >= bank-cost [
          set output-resources output-resources - 1
          
          ; buy it
          set money money + bank-cost
          ask bank 0 [
            set resources resources + 1
            set money money - bank-cost
          ]
        ]
      ]
    ]
  ]
  
  ;do incubation
  ; incubation questions: do incubation resources come from the bank? or external? with the new buyback, the bank is always poor...
  ; incubation needs to preserve the agents being incubated instead of just randomly handing out stuff, right?
  ;   at the least, incubation needs to incubate the SAME firms each step, not different firms each step
  if incubate? and ticks >= incubation-start-time and ticks <= incubation-stop-time [
    if ticks = incubation-start-time [
      if incubation-selection = "high energy" [ set incubation-firms turtle-set sublist (sort-by [[energy] of ?1 > [energy] of ?2] firms) 0 num-firms-to-incubate ] ; highest energy first
      if incubation-selection = "low energy"  [ set incubation-firms turtle-set sublist (sort-by [[energy] of ?1 < [energy] of ?2] firms) 0 num-firms-to-incubate ] ; lowest energy first
      if incubation-selection = "high age"    [ set incubation-firms turtle-set sublist (sort-by [[age] of ?1 > [age] of ?2] firms) 0 num-firms-to-incubate ] ; highest energy first
      if incubation-selection = "low age"     [ set incubation-firms turtle-set sublist (sort-by [[age] of ?1 < [age] of ?2] firms) 0 num-firms-to-incubate ] ; lowest energy first
      if incubation-selection = "random"      [ set incubation-firms n-of num-firms-to-incubate firms ]
      ask incubation-firms [ set shape "square" ]
      show "incubation starting"
    ]
    
  ; exogeneous incubators
  if incubation-type = "money" [
    ask incubation-firms [
      set money money + external-money-quantity
      update-energy
    ]
  ]
  if incubation-type = "resources" [
    ask incubation-firms [
      set input-resources input-resources + external-resource-quantity
      update-energy
    ]
  ]
    
  ; endogeneous incubators
;    if incubation-type = "money" [
;      let total-funds num-firms-to-incubate * external-money-quantity
;      if [money] of bank 0 >= total-funds [
;        ask incubation-firms [
;          set money money + external-money-quantity
;          update-energy
;        ]
;        ask bank 0 [
;          set money money - total-funds
;        ]
;      ]]
;    if incubation-type = "resources" [
;      let total-funds num-firms-to-incubate * external-resource-quantity
;      if [resources] of bank 0 >= total-funds [
;        ask incubation-firms [
;          set input-resources input-resources + external-resource-quantity
;          update-energy
;        ]
;        ask bank 0 [
;          set resources resources - total-funds
;        ]
;      ]
;    ]
  ]
  
  set living-incubates ifelse-value (incubation-firms = nobody) [0] [count incubation-firms]
  
  update-globals
  update-lorenz-and-gini
  
  tick
end

;;;;; run-time helper functions
to reset-tick-variables
  set prev-gdp gdp
  set gdp 0
  set trades 0
  set production 0
  set dissipation 0
  set source-input 0
  
  ask firms [ set active? false ]
end

to update-globals
  set living-firm-count count firms
  
  set total-resources (sum [input-resources] of firms + sum [output-resources] of firms)
  set cumul-total-resources (cumul-total-resources + total-resources)
  
  let edge-count count hidden-ties
  let trade-node-count count firms + count sources
  let denom (trade-node-count * (trade-node-count - 1) / 2)
  set interaction-network-density ifelse-value (denom = 0) [0] [ edge-count / denom ]
  
  set wealth-dist [money] of firms
end

to update-lorenz-and-gini
  let sorted-energy sort [energy] of firms
  let total-energy  sum sorted-energy
  let energy-sum-so-far  0
  let index 0
  let energy-gini-index-reserve 0
  
  ; calculate the gini indexes
  repeat living-firm-count [        
    set energy-sum-so-far (energy-sum-so-far + item index sorted-energy)
    set energy-gini-index-reserve ifelse-value (total-energy != 0) [(energy-gini-index-reserve + (index / living-firm-count) - (energy-sum-so-far / total-energy))] [0]
 
    set index (index + 1)
  ]
  set energy-gini-index ifelse-value (living-firm-count > 0) [(energy-gini-index-reserve / living-firm-count) / 0.5] [0]
end
@#$#@#$#@
GRAPHICS-WINDOW
515
10
949
465
26
26
8.0
1
10
1
1
1
0
1
1
1
-26
26
-26
26
1
1
1
ticks
30.0

INPUTBOX
5
55
115
115
num-resources
1
1
0
Number

BUTTON
80
10
150
43
Go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
155
10
225
43
Go Once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
5
10
75
43
Setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
5
305
115
365
num-firms
50
1
0
Number

INPUTBOX
5
210
150
270
num-sources
1
1
0
Number

PLOT
955
475
1155
625
Total Resources
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot total-resources"

INPUTBOX
305
210
450
270
source-regen-rate
25
1
0
Number

INPUTBOX
150
210
305
270
initial-source-quantity
0
1
0
Number

INPUTBOX
115
405
225
465
dissipation-rate
3
1
0
Number

INPUTBOX
5
405
115
465
dissipation-probability
0.02
1
0
Number

PLOT
1160
10
1360
160
Output Resource Type Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 0 (max-resources + 1)  ;; + 1 to make room for the width of the last bar\nhistogram [output] of firms"

INPUTBOX
115
305
225
365
initial-firm-quantity
0
1
0
Number

INPUTBOX
335
305
450
365
transform-rate
1
1
0
Number

SWITCH
335
365
450
398
transform-all?
transform-all?
1
1
-1000

PLOT
1160
475
1360
625
Total Resource Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "let i 0\nwhile [i < max-resources] [\n  let pen-name (word i)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (i * 10 + 5)\n  let os [output-resources] of firms with [output = i]\n  let is [input-resources] of firms with [input = i]\n  plot sum  (sentence os is)\n  set i i + 1\n]"
PENS

TEXTBOX
10
285
80
303
Firm Contorls
11
0.0
1

TEXTBOX
10
190
100
208
Source Controls
11
0.0
1

INPUTBOX
335
55
450
115
seed
938542018
1
0
Number

SWITCH
335
115
450
148
use-seed?
use-seed?
1
1
-1000

SWITCH
160
750
295
783
show-hidden-ties?
show-hidden-ties?
1
1
-1000

SWITCH
5
750
155
783
show-trans-network?
show-trans-network?
0
1
-1000

MONITOR
465
475
540
520
Density
interaction-network-density
4
1
11

INPUTBOX
225
405
335
465
vision-radius
6
1
0
Number

INPUTBOX
335
405
445
465
transfer-radius
3
1
0
Number

INPUTBOX
320
590
425
650
mut-probability
0.2
1
0
Number

INPUTBOX
5
590
110
650
spawn-threshold
20
1
0
Number

INPUTBOX
215
590
320
650
parent-contrib
0.5
1
0
Number

INPUTBOX
5
660
120
720
min-lifespan
0
1
0
Number

MONITOR
465
525
540
570
Living Firms
living-firm-count
4
1
11

INPUTBOX
120
660
235
720
max-firms
5000
1
0
Number

PLOT
955
10
1155
160
Input Resource Type Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "plot-pen-reset  ;; erase what we plotted before\nset-plot-x-range 0 (max-resources + 1)  ;; + 1 to make room for the width of the last bar\nhistogram [input] of firms"

SWITCH
230
10
340
43
birth?
birth?
0
1
-1000

SWITCH
340
10
450
43
death?
death?
0
1
-1000

PLOT
750
630
950
780
Population
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot living-firm-count"

INPUTBOX
110
590
215
650
spawn-probability
0.25
1
0
Number

TEXTBOX
10
570
160
588
Reproduction
11
0.0
1

PLOT
545
475
745
625
Trades
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot trades"

PLOT
750
475
950
625
Production
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot production"

INPUTBOX
225
305
335
365
initial-money
40
1
0
Number

BUTTON
1570
250
1670
283
Shock Landscape
layout-trades
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
5
115
115
175
firm-cost
1
1
0
Number

MONITOR
465
575
540
620
Money
sum [money] of firms + sum [money] of banks
17
1
11

SWITCH
300
750
425
783
arbitrage?
arbitrage?
1
1
-1000

INPUTBOX
1375
120
1545
180
external-money-quantity
3
1
0
Number

INPUTBOX
1550
120
1720
180
external-resource-quantity
3
1
0
Number

BUTTON
1465
250
1565
283
Shock Money
shock-money
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
1375
55
1480
115
num-firms-to-incubate
10
1
0
Number

BUTTON
1465
285
1565
318
Add Resources
shock-resources
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
115
55
225
115
max-resources
20
1
0
Number

MONITOR
465
625
540
670
Resources
count trans-nodes with [ (count out-link-neighbors) + (count in-link-neighbors) > 0]
17
1
11

MONITOR
465
675
542
720
#Resources
sum [output-resources] of firms + sum [input-resources] of firms + sum [output-resources] of sources + sum [resources] of banks
2
1
11

MONITOR
1375
185
1442
230
Steps Left
ifelse-value (ticks >= incubation-start-time and ticks <= incubation-stop-time) [incubation-stop-time - ticks] [0]
17
1
11

INPUTBOX
1600
55
1720
115
incubation-stop-time
500
1
0
Number

BUTTON
1395
595
1520
628
Add Money to Bank
ask bank 0 [\n    set money money + external-money-quantity\n]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1395
630
1520
663
Add Resources to Bank
ask bank 0 [\n    set resources resources + external-resource-quantity\n]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
1365
720
1450
780
bank-resources
200
1
0
Number

INPUTBOX
1455
720
1540
780
bank-money
0
1
0
Number

MONITOR
1365
670
1450
715
Bank Res
[resources] of bank 0
17
1
11

MONITOR
1455
670
1540
715
Bank Cash
[money] of bank 0
17
1
11

MONITOR
392
475
462
520
T. Density
ifelse-value arbitrage? \n[(count trans-ties + count trans-nodes with [color = red]) / (count trans-nodes * count trans-nodes)]\n[(count trans-ties) / (count trans-nodes * (count trans-nodes - 1))]
4
1
11

MONITOR
465
730
540
775
Total
sum [input-resources] of firms + \nsum [output-resources] of firms + \nsum [money] of firms + \nsum [output-resources] of sources + \nsum [money] of banks + \nsum [resources] of banks
2
1
11

PLOT
1160
630
1360
780
Trans Network Density
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value arbitrage? [\n(count trans-ties) / (count trans-nodes * (count trans-nodes))\n] \n[(count trans-ties) / (count trans-nodes * (count trans-nodes - 1))]"

INPUTBOX
320
655
425
715
max-contrib
0.9
1
0
Number

PLOT
545
630
745
780
Energy Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if living-firm-count > 0 [\nplot-pen-reset  ;; erase what we plotted before\nlet data-values (sentence [input-resources] of firms [output-resources] of firms [money] of firms)\nlet max-range max data-values\nset-plot-x-range 0 ( max-range  + 1)  ;; + 1 to make room for the width of the last bar\nset-histogram-num-bars 30\nhistogram data-values\n]"

INPUTBOX
1485
55
1595
115
incubation-start-time
450
1
0
Number

BUTTON
1570
285
1670
318
Layout Neighbors
layout-hidden
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
955
165
1155
315
GDP
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot gdp"

PLOT
955
320
1365
470
GDP Change
NIL
NIL
0.0
10.0
-1.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot ifelse-value (gdp > 0) [prev-gdp / gdp - 1] [0]"

INPUTBOX
115
115
225
175
source-cost
2
1
0
Number

SWITCH
1375
10
1482
43
incubate?
incubate?
0
1
-1000

CHOOSER
1485
10
1595
55
incubation-type
incubation-type
"money" "resources"
1

INPUTBOX
225
115
325
175
bank-cost
10
1
0
Number

PLOT
750
785
1360
1090
Rule Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "let rules remove-duplicates rule-dist\nforeach rules [\n  let r ?\n  let pen-name (word r)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (item 0 r * 10 + item 1 r + 5)\n  plot length filter [? = r] rule-dist\n]"
PENS

PLOT
135
785
745
1090
Rule Activity
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "let rules remove-duplicates rule-dist\nforeach rules [\n  let r ?\n  let pen-name (word r)\n  create-temporary-plot-pen pen-name\n  set-current-plot-pen pen-name\n  set-plot-pen-color (item 0 r * 10 + item 1 r + 5)\n  plot count firms with [active? and (input = item 0 r) and (output = item 1 r)]\n]"
PENS

CHOOSER
1600
10
1720
55
incubation-selection
incubation-selection
"high energy" "low energy" "high age" "low age" "random"
3

PLOT
955
630
1155
780
Energy Gini
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot energy-gini-index"

INPUTBOX
1375
250
1460
320
shock-quantity
10
1
0
Number

MONITOR
392
525
462
570
NIL
living-incubates
17
1
11

PLOT
1160
165
1360
315
Age Distribution
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "if living-firm-count > 0 [\nplot-pen-reset  ;; erase what we plotted before\nlet data-values [age] of firms\nlet max-range max data-values\nset-plot-x-range 0 ( max-range  + 1)  ;; + 1 to make room for the width of the last bar\nset-histogram-num-bars 30\nhistogram data-values\n]"

@#$#@#$#@
Note: you can have fractional rates... the question is doing we want to allow trade/production to require whole resources? or should agents be able to trade/produce fractional resources?

# WHAT IS IT?

This is a model of resource flow over a static interaction/dynamic trade network. There are two types of nodes, firms and sources. Sources generate resources at a parameterized rate. Firms acquire resources form either a source or another firm and transform those resources. Transformed resources are held until another firm comes along to take them. To compensate for resource generation by the sources, firms have a parameterized probability of resource decay. When a resource decays it leaves the system.

This model can also be thought of as a model of resource flow and transformation on a randomized system. You put a bunch of agents in a box, shake it up, dump it out, then feed resources to a subset of nodes and see how those resources end up being distributed as the agents fight over them. Similar to autocatalytic sets. 
# HOW IT WORKS

This model is based on the assumption that every economic entity can be abstracted as a "resource transformer." This implicitly assumes that firms, households, and banks are essentially the same. 

In total, there are _n_ agents in the simulation. These agents are connected to each other by a static Erdos-Renyi random graph parameterized by the _link-probability_ variable. This graph forms the interaction network of the system. Agents are only allowed to interact with their immediate neighbors in the interaction network.
## Agents
Agents can be either a **firm** or a **source**.
### Firms (Resource Transformers)

 
![Adaptive Resource Transformer](figures/art_diagram.png)

Firms are the basic economic agents in the simulation. They attempt to acquire specific resources through interaction in an environment and then transform those resources in accordance with a **resource transformation rule**. 

Each firm contains a store of input resources and a store of output resources. The quantity in each of these stores is given by the _input-quantity_ and _output-quantity_. Firms start with an initial quantity of input resources equal to the _initial-firm-quantity_ parameter. 

Firms obtain input resources via the **trade process**. A firm (the "buyer") examines every neighbor and identifies the subset of neighbors that contain positive quantities of output resources that match the buyer's input resource. A single neighbor (the "trade target") is selected uniformly at random from this subset. The trade target can be either a source or another firm. After a trade target has been selected by the buyer, resources flow between the two agents in a quantity equal to the _transfer-quantity_.

Resources are moved from the input store to the output store via the **production process**. The production rule is guided by a firm’s resource transformation rule. The _transform-quantity_ specifies the maximum number of resources that can be transformed during each tick. 

Once resources have been moved to the output store, they remain there until interaction with another firm removes them and/or they are lost through the **dissipation process**. At each tick, resources in both the input and output stores have a probability, _input-loss-probability_ and _output-loss-probability_, of being lost. The quantity of resources lost is given by the _input-loss_ and _output-loss_.

### Firm Behaviors
Firms execute three specific behaviors: **trade**, **production**, and **dissipation**
#### Trade
Trade is the process by which firms acquire resources. A firm can either trade with a source or another firm.

The trade process is defined as:
  1. The buyer selects a trade target
  2. Resources are transferred from the trade target to the buyer

Trade target selection
>  1. The buyer makes a list of all neighboring agents.
>  2. The buyer removes from that list all agents that do not produce as output what the buyer needs as input.
>  3. The buyer picks an agent from the list the maximizes the resource gain

The resource gain of an agent is defined as d / M * (op * or - ip * ir)

Resource transfer
>  1. The trade target removes _transfer-quantity_ resources from its store of output resources
>  2. The buyer adds _transfer-quantity_ resources to its store of input resources.

The trade process can be compacted into the following **trade rule**: Select a trade target based on your transformation rule and then acquire at most _transfer-quantity_ resources.

#### Production
Production is the process of converting input resources to output resources. 

The production process is defined as:
1. Subtract _transform-quantity_ resources from the input store
2. Add _transform-quantity_ resources to the output store.

The production process can be more compactly stated with the following **production rule**: Transform _transform-quantity_ input resources into _transform-quantity_ output resources.
#### Dissipation
Dissipation is the process of resource loss. From a systems point of view it is the counterpoint to resource generation by a source. The semantics of resource loss are application specific; e.g. metabolism or spoilage. 

The dissipation process is defined as:
1. Generate a random number _x_ between 0 and 1. 
2. If _x < input-loss-probability_ and the firm has resources to lose
    a. Remove _input-loss_ quantity of resources from the firm’s store of input resources
3. Generate a random number _y_ between 0 and 1. 
    4. If _y < output-loss-probability_ and the firm has resources to lose
a. Remove _output-loss_ quantity of resources from the firm’s store of output resources

The dissipation process can be more compactly stated with the following **dissipation rule**: Lose _input-loss_ input resources with a probably _input-loss-probability_ and lose _output-loss_ resources with probability _output-loss-probability_.

#### Death
When an agent runs out of resources (or money and resources) it dies and is removed from the system. If an agent dies while it still has money, that money is distributed one unit at a time to a randomly chosen agent. This redistribution helps ensure the conservation of money.

#### Reproduction
When the output resources of an agent exceed the **spawn-threshold** then an agent creates a clone. This clone has zero output resources and zero money. It's transformation rules are identical to the parent agent with a probability (1 - **mut-probability**)^2. A child agent receives an amount of input resource equal to **parent-contrib** * **output-resources** (of the parent).

### Sources

 
![Source](figures/source_diagram.png)

A source represents an entry point for resources into the system. Each source is capable of storing one type of resource. The quantity of resource available depletes as firms take the resources but regenerates at a rate specified by the simulation parameter _source-regen-rate_.

## The Environment
 
![Level 0 Environment](figures/art_network.png)

Agents are connected by an Erdos-Renyi random graph with a connection probability specified by the user parameter _link-probability_. This graph imposes a notion of bounded rationality that restricts the information available to agent decision making to the local neighborhood; where the local neighborhood consists of only those agents within one degree (one hop).

# HOW TO USE IT

## Parameters
**num-resources** specifies the number of resources that exist in the system at creation
**max-resources** specifies the maximum number of resources that can ever exist in the system
**universal-cost** specifies the price (in money) of every resource

**use-seed?** if set to “on” then a user defined seed is used instead of the system defined seed
**see** specifies the user defined seed for the random number generator

**num-sources** specifies how many sources exist in the system
**initial-source-quantity** specifies the quantity of resource each sink has a tick 0
**source-regen-rate** specifies the quantity of resource a source gains at the start of each tick (resources/tick)

**num-firms** specifies the number of firms in the system at tick 0
**initial-firm-quantity** specifies the quantity of input resources a firm has a tick 0
**transfer-rate** specifies the maximum quantity of resources that can be acquired from other agents at each tick (resources/tick)
**transfer-all?** if “on” then an agent will attempt to acquire every output resource that a trade target has
**transform-rate** specifies the maximum quantity of resources that an agent can transform in a tick (resources/tick)
**transform-all?** if “on” then an agent will attempt to transform every input resource it has
**output-loss-probability** specifies the probability that a firm will lose output resources to dissipation
**output-loss-rate** specifies the maximum quantity of output resources that can be lost to dissipation
**lose-all-output?** if “on” then a firm can dissipate all of its output resources
**input-loss-probability** specifies the probability that a firm will lose input resources to dissipation
**input-loss-rate** specifies the maximum quantity of input resources that can be lost to dissipation
**lose-all-input?** if “on” then a firm can dissipate all of its input resources
**vision-radius** specifies how many patches out (in a circle) the agent can detect other agents
**transfer-radius** specifies how many patches out (in a circle) the agent can trade with other agents
**movement-range** specifies how many patches an agent can move in a single tick
**initial-money** specifies how much money each agent starts with

**spawn-threshold** specifies the minimum level of output resources required by a firm to reproduce
**spawn-probability** specifies the probability that each firm will reproduce, if qualified
**parent-contrib** specifies the percentage of resource transfered from parent to child
**mut-probability** specifies the probability that the input and output of a firm will change during reproduction
**min-lifespan** specifies the minimum lifespan for the initial population; this is used to ensure a minimal warm-up period
**max-firms** specifies the maximum number of firms that can exist in the world

**arbitrage?** if set to "on" then agents can have output = input, if set to "off" then an agent's input is different from its output. 
**distributions?** if set to “on” then the _source-regen-rate_, _output-loss-rate_, and _input-loss-rate_ are treated as Poisson random variables with the specified value acting as the mean
**restrict-movement?** if set to "on" then agents make an additional random move if their destination patch is occupied. This only reduce overlap, it does not prevent it.

## Output

# THINGS TO NOTICE

Because agents move based on a utility, but their movement range is limited, they become unable to decide where to go when they are able to see the entire space at once (unbounded rationality). This occurs because at each step an agent moves a finite number of steps towards the best choice, but then in the next step decides to chase a different agent. As a result, the agent never goes anyplace because it keeps changing its mind. 

The effectiveness of positive shocks depends on the structure of the firms. Adding money is helpful if production is limited (e.g. transform-rate = 1) and trade has become slow, where as adding resources is helpful when production is not so limited.

## Stylized facts

Even in its simplicity, this model is able to reproduce at least one well known phenomenon from macroeconomics – an unequal distribution of resources. Furthermore, the level of inequality appears to be tied to the connectivity of the graph. This association is also observed in work on game-theoretical trade games over random networks. 

# EXTENDING THE MODEL

Make the interaction network dynamic. Add birth and death. Have buyers give a "reward" when taking resources. 

Have agents die when they run out of input resources, output resources, and rewards. 

# EXPERIMENTAL IDEAS

Use the manual random seed and change each parameter to see how the system responds without the extra unput of random variability. 

# NETLOGO FEATURES
This model makes use of the **nw** extension. However, there is a problem with the random number generators somewhere in its code, and so setting a user seed will not enable reproducibility.

# RELATED MODELS
Related to our original MASON model, this is a drastic simplification that removes money, movement, birth, and death. We use output resources instead of money. Instead of movement, agents are part of a fixed interaction network with a fixed set of neighbors. To compensate for the removal of death we add in dissipation (resource decay).

We also generalize many of the arbitrarily fixed parameters from the original model, such as how many resources can be bought or transformed by an agent. 


Related to sugarscape, resource decay is a stochastic metabolism. Production is the concept of turning sugar into spice (this is not done in sugarscape). Trade is a forced unilateral exchange of resources instead of a reciprocal transaction.

## Model Comparison

<table border="1">
  <tr><td>&nbsp;</td><td>Sugarscape</td><td>Econoscape</td><td>Justification</td></tr>
  <tr><td>Environment</td>
      <td>2D Lattice</td>
      <td>Geographic Network</td>
      <td>A network based environment provides more degrees of freedom in terms of movement and structural formation.</td>
  </tr>
  <tr><td>Movement</td>
      <td></td>
      <td></td>
      <td></td>
  </tr>
  <tr><td>Trade</td>
      <td></td>
      <td></td>
      <td></td>
  </tr>
  <tr><td>Production</td>
      <td></td>
      <td></td>
      <td></td>
  </tr>
</table>


# CREDITS AND REFERENCES
  1. Epstein, J. M., & Axtell, R. L. (1996). Growing Artificial Societies: Social Science from the Bottom Up (First.). The MIT Press.
  2. Hollander, C. D., Garibay, I., & O’Neal, T. (2012). Transformation Networks: A study of how technological complexity impacts economic performance. 8th Artificial Economics. Castellon, Spain: Springer-Verlag.
  3. Hannon, B. (1973). The structure of ecosystems. Journal of Theoretical Biology, 41(3), 535–546.
  4. Hirata, H., & Ulanowicz, R. e. (1984). Information theoretical analysis of ecological networks. International Journal of Systems Science, 15(3), 261–270.
  5. Ulanowicz, R. E. (1983). Identifying the structure of cycling in ecosystems. Mathematical Biosciences, 65(2), 219–237.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.0.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="10"/>
    <metric>cumul-trades</metric>
    <metric>cumul-production</metric>
    <metric>cumul-dissipation</metric>
    <metric>cumul-input-dissipation</metric>
    <metric>cumul-output-dissipation</metric>
    <metric>cumul-source-input</metric>
    <metric>cumul-gini</metric>
    <metric>cumul-input-gini</metric>
    <metric>cumul-output-gini</metric>
    <metric>cumul-total-resources</metric>
    <metric>trades</metric>
    <metric>production</metric>
    <metric>dissipation</metric>
    <metric>input-dissipation</metric>
    <metric>output-dissipation</metric>
    <metric>source-input</metric>
    <metric>output-gini-index</metric>
    <metric>input-gini-index</metric>
    <metric>total-resources</metric>
    <metric>interaction-network-density</metric>
    <metric>living-firm-count</metric>
    <metric>wealth-dist</metric>
    <enumeratedValueSet variable="external-money-quantity">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="death?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="die-with-money?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mut-probability">
      <value value="0.05"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="external-resource-quantity">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="output-loss-rate">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="transform-rate">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="distributions?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="source-regen-rate">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="lose-all-output?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seed">
      <value value="1981"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-lifespan">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-firm-quantity">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-sources">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="input-loss-probability">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="spawn-probability">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-source-quantity">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="transfer-radius">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-money">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="vision-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-resources">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-firms">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="see-all-transfer?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="parent-contrib">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="universal-cost">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="output-loss-probability">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="see-all-vision?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-firms">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="movement-range">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="num-firms-to-shock">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="move-all?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="birth?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-hidden-ties?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="spawn-threshold">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="transform-all?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-resources">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="transfer-all?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="restrict-movement?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="show-trans-network?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="input-loss-rate">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="transfer-rate">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="lose-all-input?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="use-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="arbitrage?">
      <value value="true"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
1
@#$#@#$#@
