package eco2sim;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.util.Bag;
import ec.util.MersenneTwisterFast;

/**
 * The environment is responsible for conducting external agent operations: reproduction, 
 * incubation, injection, and dissipation.
 *  
 * @author miakbas
 * @author chollander
 * 
 */
public class Environment implements Steppable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3057636745116636355L;

	public enum FitnessType { BUY, SELL, BUY_OR_SELL, BUY_AND_SELL, ENERGY, EVERYONE }
	public enum IncubationType { MONEY, RESOURCES }
	
    private FitnessType reproductionFitnessType;
	
    
    private double injectionProbability;
    private FitnessType injectionFitnessType;
    
    private FitnessType incubationFitnessType;
    private IncubationType incubationType;
    private double incubationResourceQuantity;
    private double incubationMoneyQuantity;
    
    private double maxBuys;  // max input resources bought
    private double maxSells; // max output resources sold
    private double maxEnergy;
    
    
	public double getInjectionProbability() {
		return injectionProbability;
	}
	public void setInjectionProbability(double injectionProbability) {
		this.injectionProbability = injectionProbability;
	}

	public double getMaxEnergy() { return maxEnergy; }
    
    public Environment(FitnessType reproductionFitnessType,
    		           double injectionProbability, FitnessType injectionFitnessType) {
    	
    	this.reproductionFitnessType = reproductionFitnessType;
    	
    	this.setInjectionProbability(injectionProbability);
    	this.injectionFitnessType = injectionFitnessType;
    	
    	// These need to be parameterized. 
    	this.incubationFitnessType = FitnessType.ENERGY;
    	this.incubationType = IncubationType.MONEY;
    	this.incubationMoneyQuantity = 10.0;
    	this.incubationResourceQuantity = 10.0;
    }
    
    
    /*
     * The environment is where reproduction, incubation, investment (harvesting), and dissipation occur. These processes take place here, instead
     * of at the agent level because they are things that the world does in response to performance. If a firm is successful, somebody
     * tries to copy its business model; somebody invests in it; and somebody offers to incubate it. Dissipation occurs here because currently, 
     * agents without energy cannot dissipate, and so we have to give them some energy first. If they're successful, there will be energy left
     * after dissipation. Remember, the purpose of dissipation is to kill agents so that only the strong survive and thrive.  
     */
    public void step(SimState state) {
        final Eco2Sim s = (Eco2Sim) state;
        
        // prep for fitness calculations
        calculateMaximumValues(s);
        
        reproduce(s);
        
        // incubate(s);
        
        invest(s);
        dissipate(s);
    }
        
    private void calculateMaximumValues(Eco2Sim state) {
    	Bag agents = new Bag(state.world.allObjects);

		maxBuys   = 0;
		maxSells  = 0;
		maxEnergy = 0;
		for (Object o : agents) {
			final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
			if (art.inputQuantityBought > maxBuys)
				maxBuys = art.inputQuantityBought;
			if (art.outputQuantitySold > maxSells) {
				maxSells = art.outputQuantitySold; 
			}
			final double energy = art.inputQuantity + art.outputQuantity + art.moneyQuantity;
			if (energy > maxEnergy)
				maxEnergy = energy;
		}
    }
    
    private boolean isAgentFit(MersenneTwisterFast rng, AdaptiveResourceTransformer agent, FitnessType fitnessType) {
    	/*
    	 * Determine whether or not an agent is fit based on a proportional fitness. 
    	 */
    	final double p;
		switch (fitnessType) {
		case BUY:
			p = (agent.inputQuantityBought + 1.0) / (maxBuys + 1.0);
			break;
		case SELL:
			p = (agent.outputQuantitySold + 1.0) / (maxSells + 1.0);
			break;
		case BUY_OR_SELL:
			p = (agent.inputQuantityBought + 1.0) / (maxBuys + maxSells + 1.0)
			  + (agent.outputQuantitySold + 1.0) / (maxBuys + maxSells + 1.0);
			break;
		case BUY_AND_SELL:
			p = (agent.inputQuantityBought + 1.0) / (maxBuys + 1.0)
			  * (agent.outputQuantitySold + 1.0) / (maxSells + 1.0);
			break;
		case ENERGY:
			final double energy = agent.inputQuantity + agent.outputQuantity + agent.moneyQuantity;
			p = (energy + 1.0) / (maxEnergy + 1.0);
			break;
		case EVERYONE:
			p = 1;
			break;
		default:
			p = 1;
			break;
		}

		final double x = rng.nextDouble();
		return x < p;
    }    
		
	
	public void reproduce(Eco2Sim state) {
		// calculate the mean energy of the population
		final double meanEnergy = state.getMeanEnergy();
		
		for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            if(art.getEnergy() >= meanEnergy && isAgentFit(state.random, art, reproductionFitnessType)) {
            	final AdaptiveResourceTransformer child = art.reproduce(state);
				if (child != null) {
					state.world.setObjectLocation(child, child.location.x, child.location.y);
					child.stoppableReference = state.schedule.scheduleRepeating(child, 1, 1.0);
					
					state.firmsBorn++;
				}
        	}
		}
	}
	
	
	private void dissipate(final Eco2Sim state) {
    	for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            art.dissipate(state);
    	}
    }
	
    private void invest(final Eco2Sim state) {
    	for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
	    		
	    	if(isAgentFit(state.random, art, injectionFitnessType)) {
	            art.harvest(state);
	        }
    	}
    }
    
    public void incubate(final Eco2Sim state) {
    	for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
	    	
	    	if(isAgentFit(state.random, art, incubationFitnessType)) {
	            switch(incubationType) {
	            case MONEY:
	            	incubateWithMoney(art);
	            	break;
	            case RESOURCES:
	            	incubateWithResources(art);
	            	break;
	            }
	            
	        }
    	}
    }
    
    public void incubateWithResources(AdaptiveResourceTransformer art) {
    	art.inputQuantity = incubationResourceQuantity;
    	art.inputQuantityInjected = incubationResourceQuantity;
    }
    
    public void incubateWithMoney(AdaptiveResourceTransformer art) {
    	art.moneyQuantity += incubationMoneyQuantity;
    	art.moneyInjected += incubationMoneyQuantity;
    }
}