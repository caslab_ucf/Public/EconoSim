package eco2sim;

import java.awt.Color;
import java.awt.Graphics2D;

import javax.swing.JFrame;

import sim.display.Console;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.DrawInfo2D;
import sim.portrayal.Inspector;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.portrayal.simple.CircledPortrayal2D;
import sim.portrayal.simple.LabelledPortrayal2D;
import sim.portrayal.simple.MovablePortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;


public class Eco2SimWithUI extends GUIState {
	public Display2D display;
	public JFrame displayFrame;
	SparseGridPortrayal2D worldPortrayal = new SparseGridPortrayal2D();
	
	public static void main(String[] args) {
		Eco2SimWithUI vid = new Eco2SimWithUI();
		Console c = new Console(vid);
		c.setVisible(true);
	}
	
	public Eco2SimWithUI() {
		super(new Eco2Sim(System.currentTimeMillis()));
	}
	
	public Eco2SimWithUI(SimState state) {
		super(state);
	}
	
	public static String getName() {
		return("Economic Ecosystem Simulation");
	}
	
	public Object getSimulationInspectedObject() { return state; }
	public Inspector getInspector() {
		Inspector i = super.getInspector();
		i.setVolatile(true);
		return i;
	}
	
	public void start() {
		super.start();
		setupPortrayals();
	}
	
	public void load(SimState state) {
		super.load(state);
		setupPortrayals();
	}
	
	public void setupPortrayals() {
		final Eco2Sim s = (Eco2Sim) state;
		
		final float numResources = s.getNumberOfResources();
		
		worldPortrayal.setField(s.world);
		worldPortrayal.setPortrayalForAll(
			new MovablePortrayal2D(
				new CircledPortrayal2D(
					new LabelledPortrayal2D(
						new OvalPortrayal2D() {
							private static final long serialVersionUID = -8898829991362769146L;

							public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
								AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) object;

								paint = new Color( (art.input+1) / numResources, (art.output+1) / numResources, 0.0f);
								super.draw(object, graphics, info);
							}
						}, 5.0, null, Color.black, true), 
					0, 5.0, Color.green, true)));
		
		display.reset();
		display.setBackdrop(Color.white);
		
		display.repaint();
	}
	
	public void init(Controller c) {
		super.init(c);
		display = new Display2D(600, 600, this);
		display.setClipping(false);
		
		displayFrame = display.createFrame();
		displayFrame.setTitle("World Display");
		
		c.registerFrame(displayFrame);
		displayFrame.setVisible(true);
		display.attach(worldPortrayal, "World");
	}
	
	public void quit() {
		super.quit();
		if(displayFrame != null)
			displayFrame.dispose();
		displayFrame = null;
		display = null;
	}
}