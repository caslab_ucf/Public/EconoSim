package eco2sim;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.util.Bag;

public class PostProcessDaemon implements Steppable {

	private static final long serialVersionUID = 8573435979621726850L;

	public void step(SimState state) {
		final Eco2Sim s = (Eco2Sim) state;
		
		updateSimulationMeasures(s);
		updateAgentMeasures(s);
		
		commitGenocide(s);
		
		if(s.world.size() == 0) {
			s.finish();
		}
	}
	
	public void updateSimulationMeasures(Eco2Sim state) {		
		updateDataMapEntry(state, "cPopulation", state.getPopulation());
		updateDataMapEntry(state, "cFirmsBorn", state.getFirmsBorn());
		updateDataMapEntry(state, "cTotalMoney", state.getTotalMoney());
		updateDataMapEntry(state, "cMoneyExchanged", state.getMFF());
		updateDataMapEntry(state, "cMoneyDissipated", state.getMFG());
		updateDataMapEntry(state, "cMoneyInjected", state.getMGF());
		updateDataMapEntry(state, "cTotalResources", state.getTotalResources());
		updateDataMapEntry(state, "cResourcesExchanged", state.getRFF());
		updateDataMapEntry(state, "cResourcesDissipated", state.getRFG());
		updateDataMapEntry(state, "cResourcesInjected", state.getRGF());
		updateDataMapEntry(state, "cTotalEnergy",     state.getTotalEnergy());
		updateDataMapEntry(state, "cTotalEnergyExchanged",  state.getMFF() + state.getRFF());
		updateDataMapEntry(state, "cTotalEnergyDissipated", state.getMFG() + state.getRFG());
		updateDataMapEntry(state, "cTotalEnergyInjected",   state.getMGF() + state.getRGF());
		updateDataMapEntry(state, "cTransNetworkEdges",     state.getTransformationNetworkEdgeCount());
		updateDataMapEntry(state, "cTransNetworkDensity",   state.getTransformationNetworkDensity());
		
		// need to add TotalEnergyExchange as MFF + RFF
		// TotalMoney
		// TotalResources
	}
	
	
	public void updateDataMapEntry(Eco2Sim state, String key, double value) {
		if(state.dataMap.containsKey(key)) {
			final double v = (Double) state.dataMap.get(key) + value;
			state.dataMap.put(key, v);
		} else {
			state.dataMap.put(key, value);
		}
	}
	
	public void updateAgentMeasures(Eco2Sim state) {
        for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            art.updateTickVariables();
		}
	}
	
	public void commitGenocide(Eco2Sim state) {
		for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            if(art.getEnergy() == 0) {
        		art.stoppableReference.stop();
        		state.world.remove(art);
        		
        		state.firmsKilled++;
        	}
		}
		
		// we don't know how many firms are killed until we actually kill them. 
		updateDataMapEntry(state, "cFirmsKilled", state.getFirmsKilled());
	}
}