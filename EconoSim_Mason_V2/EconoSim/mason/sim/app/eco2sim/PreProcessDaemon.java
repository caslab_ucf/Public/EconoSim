package eco2sim;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.util.Bag;

public class PreProcessDaemon implements Steppable {

	private static final long serialVersionUID = 476146726332961845L;

	public void step(SimState state) {
		final Eco2Sim s = (Eco2Sim) state;
		
		resetSimulationMeasures(s);
		resetAgentMeasures(s);
	}
	
	public void resetSimulationMeasures(Eco2Sim state) {
		state.firmsBorn = 0;
		state.firmsKilled = 0;
	}
	
	public void resetAgentMeasures(Eco2Sim state) {
        for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            art.resetFlowVariables();
		}
	}
}