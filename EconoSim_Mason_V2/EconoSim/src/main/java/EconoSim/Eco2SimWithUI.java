package EconoSim;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;

import sim.display.ChartUtilities;
import sim.display.Console;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.portrayal.DrawInfo2D;
import sim.portrayal.Inspector;
import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.portrayal.simple.CircledPortrayal2D;
import sim.portrayal.simple.LabelledPortrayal2D;
import sim.portrayal.simple.MovablePortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;
import sim.util.media.chart.ScatterPlotGenerator;
import sim.util.media.chart.TimeSeriesChartGenerator;


public class Eco2SimWithUI extends GUIState {
	public Display2D display;
	public JFrame displayFrame;
	SparseGridPortrayal2D worldPortrayal = new SparseGridPortrayal2D();
	public sim.util.media.chart.ScatterPlotSeriesAttributes chart1Attributes;
	double[] chart1XData = new double[3000];
	double[] chart1YData = new double[3000];
	
	public sim.util.media.chart.ScatterPlotSeriesAttributes chart2Attributes;
	double[] chart2XData = new double[3000];
	double[] chart2YData = new double[3000];
	
	private int step = 0;
	
	public static void main(String[] args) {
		Eco2SimWithUI vid = new Eco2SimWithUI();
		Console c = new Console(vid);
		c.setVisible(true);
	}
	
	public Eco2SimWithUI() {
		super(new Eco2Sim(System.currentTimeMillis()));
	}
	
	public Eco2SimWithUI(SimState state) {
		super(state);
	}
	
	public static String getName() {
		return("Economic Ecosystem Simulation");
	}
	
	public Object getSimulationInspectedObject() { return state; }
	public Inspector getInspector() {
		Inspector i = super.getInspector();
		i.setVolatile(true);
		return i;
	}
	
	public void start() {
		super.start();
		setupPortrayals();
	}
	
	public void load(SimState state) {
		super.load(state);
		setupPortrayals();
	}
	
	public void setupPortrayals() {
		final Eco2Sim s = (Eco2Sim) state;
		
		/*ChartUtilities.scheduleSeries(this, chart1Attributes, new sim.display.ChartUtilities.ProvidesDoubleDoubles() {
			
			@Override
			public double[][] provide() {
				// TODO Auto-generated method stub
				double[][] result = new double[2][3000];
				if (s != null) {
					result[0] = Arrays.copyOfRange(chart1XData, 1, chart1XData.length);
					result[1] = Arrays.copyOfRange(chart1YData, 1, chart1YData.length);
				result[0][result.length-1] = s.getPopulation();
				result[1][result.length-1] = s.getRFF();				
				}
				if(step > 2999) {
					chart1XData = result[0];
					chart1YData = result[1];
				} else	{
					chart1XData[step] = s.getPopulation();
					chart1YData[step] = s.getRFF();
				}
				step++;
				return result;
			}
		});
		
		ChartUtilities.scheduleSeries(this, chart2Attributes, new sim.display.ChartUtilities.ProvidesDoubleDoubles() {
			
			@Override
			public double[][] provide() {
				// TODO Auto-generated method stub
				double[][] result = new double[2][3000];
				if (s != null) {
					result[0] = Arrays.copyOfRange(chart2XData, 1, chart2XData.length);
					result[1] = Arrays.copyOfRange(chart2YData, 1, chart2YData.length);
				result[0][result.length-1] = s.getProductCount();
				result[1][result.length-1] = s.getRFF();				
				}
				if(step > 2999) {
					chart2XData = result[0];
					chart2YData = result[1];
				} else	{
					chart2XData[step] = s.getProductCount();
					chart2YData[step] = s.getRFF();
				}
				step++;
				return result;
			}
		});*/
		
		final float numResources = s.getNumberOfResources();
		
		worldPortrayal.setField(s.world);
		worldPortrayal.setPortrayalForAll(
			new MovablePortrayal2D(
				new CircledPortrayal2D(
					new LabelledPortrayal2D(
						new OvalPortrayal2D() {
							private static final long serialVersionUID = -8898829991362769146L;

							public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
								AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) object;

								paint = new Color( (art.input+1) / numResources, (art.output+1) / numResources, 0.0f);
								super.draw(object, graphics, info);
							}
						}, 5.0, null, Color.black, true), 
					0, 5.0, Color.green, true)));
		
		display.reset();
		display.setBackdrop(Color.white);
		
		display.repaint();
	}
	
	public void init(Controller c) {
		super.init(c);
		
		/*ScatterPlotGenerator chart1 = ChartUtilities.buildScatterPlotGenerator(this, "Variation of GDP with Population", "GDP", "Population" );
		chart1Attributes = ChartUtilities.addSeries(chart1,"Series1");
		
		ScatterPlotGenerator chart2 = ChartUtilities.buildScatterPlotGenerator(this, "Variation of GDP with Products Used", "GDP", "No. of Products Used" );
		chart2Attributes = ChartUtilities.addSeries(chart2,"Series1");
		*/
		display = new Display2D(600, 600, this);
		display.setClipping(false);
		
		displayFrame = display.createFrame();
		displayFrame.setTitle("World Display");
		
		c.registerFrame(displayFrame);
		displayFrame.setVisible(true);
		display.attach(worldPortrayal, "World");
	}
	
	public void quit() {
		super.quit();
		if(displayFrame != null)
			displayFrame.dispose();
		displayFrame = null;
		display = null;
	}
}