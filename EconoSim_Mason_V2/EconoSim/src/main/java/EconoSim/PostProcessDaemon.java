package EconoSim;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;

import com.sun.corba.se.spi.orbutil.fsm.State;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.util.Bag;
import sim.util.Int2D;

public class PostProcessDaemon implements Steppable {

	private static final long serialVersionUID = 8573435979621726850L;
	BufferedWriter writer;
	
	public PostProcessDaemon () {
		try{
		writer = new BufferedWriter(new FileWriter("output" + Math.random()+ ".csv"));
		} catch (Exception e) {}
	}
	
	public void step(SimState state) {
		final Eco2Sim s = (Eco2Sim) state;
		s.setCumGDP(s.getCumGDP() + s.getGDP());
		updateSimulationMeasures(s);
		updateAgentMeasures(s);
		
		commitGenocide(s);
		
		writeState(s);
		
		if(s.world.size() == 0) {
			s.finish();
		}
	}
	
	public void updateSimulationMeasures(Eco2Sim state) {		
		updateDataMapEntry(state, "cPopulation", state.getPopulation());
		updateDataMapEntry(state, "cFirmsBorn", state.getFirmsBorn());
		updateDataMapEntry(state, "cTotalMoney", state.getTotalMoney());
		updateDataMapEntry(state, "cMoneyExchanged", state.getMFF());
		updateDataMapEntry(state, "cMoneyDissipated", state.getMFG());
		updateDataMapEntry(state, "cMoneyInjected", state.getMGF());
		updateDataMapEntry(state, "cTotalResources", state.getTotalResources());
		updateDataMapEntry(state, "cResourcesExchanged", state.getRFF());
		updateDataMapEntry(state, "cResourcesDissipated", state.getRFG());
		updateDataMapEntry(state, "cResourcesInjected", state.getRGF());
		updateDataMapEntry(state, "cTotalEnergy",     state.getTotalEnergy());
		updateDataMapEntry(state, "cTotalEnergyExchanged",  state.getMFF() + state.getRFF());
		updateDataMapEntry(state, "cTotalEnergyDissipated", state.getMFG() + state.getRFG());
		updateDataMapEntry(state, "cTotalEnergyInjected",   state.getMGF() + state.getRGF());
		updateDataMapEntry(state, "cTransNetworkEdges",     state.getTransformationNetworkEdgeCount());
		updateDataMapEntry(state, "cTransNetworkDensity",   state.getTransformationNetworkDensity());
		
		// need to add TotalEnergyExchange as MFF + RFF
		// TotalMoney
		// TotalResources
	}
	
	
	public void updateDataMapEntry(Eco2Sim state, String key, double value) {
		if(state.dataMap.containsKey(key)) {
			final double v = (Double) state.dataMap.get(key) + value;
			state.dataMap.put(key, v);
		} else {
			state.dataMap.put(key, value);
		}
	}
	
	public void updateAgentMeasures(Eco2Sim state) {
        for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            art.updateTickVariables();
		}
	}
	
	public void commitGenocide(Eco2Sim state) {
		for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            if(art.getEnergy() == 0) {
        		art.stoppableReference.stop();
        		state.world.remove(art);
        		
        		state.firmsKilled++;
        	}
		}
		
		// we don't know how many firms are killed until we actually kill them. 
		updateDataMapEntry(state, "cFirmsKilled", state.getFirmsKilled());
	}
	
	public void writeState(Eco2Sim state) {
		try {
			
			//writer.write(Arrays.toString(state.getAgeDistribution()).substring(1, Arrays.toString(state.getAgeDistribution()).length()-1) + "\r\n");
			//writer.write(Double.toString(state.getRFF()) + "," + Double.toString(state.getProductCount()) + "\r\n");
			
			//if (300 == state.schedule.getSteps())
			//for(Double profit: state.getValueOfTotalOutputProduced()ed()){
			//	writer.write(profit + "," + state.schedule.getSteps() + "\r\n");		
			//}
			//whia output:
//			writer.write(state.schedule.getSteps() + "," +  Double.toString(state.schedule.getTime()) + ",");
//			writer.write(Double.toString(state.getValueOfTotalOutputProduced()) + "," + Double.toString(state.getNumberOfResources()) + ",");
//			writer.write(Double.toString(state.getTransformationNetworkDensity()) + "," + Double.toString(state.getTransformationNetworkEdgeCount()) + ",");
//			writer.write(Integer.toString(state.getPopulation())+ ",");
//			writer.write(Double.toString(state.getGDP()) + "," + Double.toString(state.getCumGDP()));
//			if(state.schedule.getSteps() == 800 || state.schedule.getSteps() == 1100 || state.schedule.getSteps() == 1800){
				for(Int2D tech: state.getActiveTechDistribution().keySet()){
					writer.write(state.schedule.getSteps() + "," + Integer.toString(tech.x) + "_" + Integer.toString(tech.y) + "," + Integer.toString(state.getActiveTechDistribution().get(tech)));	
					writer.write("\r\n");
				}
//			}
			//writer.write("\r\n");
			writer.flush();
		} catch (Exception e) {}
		
	}
}
