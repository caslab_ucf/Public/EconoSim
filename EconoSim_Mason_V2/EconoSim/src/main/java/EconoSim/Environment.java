package EconoSim;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.util.Bag;
import ec.util.MersenneTwisterFast;

/**
 * The environment is responsible for conducting external agent operations: reproduction, 
 * incubation, injection, and dissipation.
 *  
 * @author miakbas
 * @author chollander
 * @author cgunaratne
 * 
 */
public class Environment implements Steppable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3057636745116636355L;

	public enum FitnessType { BUY, SELL, BUY_OR_SELL, BUY_AND_SELL, ENERGY, EVERYONE }
	public enum IncubationSelectionType {BEST, WORST, YOUNGEST, OLDEST, BEST_YOUNGEST, WORST_YOUNGEST, RANDOM}
	public enum IncubationAssistanceType { MONEY, RESOURCES, RANDOM, PROPORTIONAL, NETWORKING, NETWORKING_AND_RESOURCES, NETWORKING_AND_MONEY, PROPORTIONAL_AND_NETWORKING, NONE }
	
	
	
    private FitnessType reproductionFitnessType;
	
    
    private double injectionProbability;
    private FitnessType injectionFitnessType;
    
    private IncubationSelectionType incubationSelectionType;
    private IncubationAssistanceType incubationType;
    private double pIncubateWithResource; //Only used for proportional
    private double networkLimit;
    private double incubationResourceQuantity;
    private double incubationMoneyQuantity;
    private double incubationStartStep;
    private double incubationEndStep;
    
    private double maxBuys;  // max input resources bought
    private double maxSells; // max output resources sold
    private double maxEnergy;
    
    
	public double getInjectionProbability() {
		return injectionProbability;
	}
	public void setInjectionProbability(double injectionProbability) {
		this.injectionProbability = injectionProbability;
	}

	public double getMaxEnergy() { return maxEnergy; }
    
    public Environment(FitnessType reproductionFitnessType,
    		           double injectionProbability, FitnessType injectionFitnessType,
    		           IncubationSelectionType incubationSelectionType, IncubationAssistanceType incubationAssistanceType, 
    		           double pIncubateWithResource, double networkLimit,
    		           double incubationMoneyQuantity, double incubationResourceQuantity, 
    		           double incubationStartStep, double incubationEndStep) {
    	
    	this.reproductionFitnessType = reproductionFitnessType;
    	
    	this.setInjectionProbability(injectionProbability);
    	this.injectionFitnessType = injectionFitnessType;
    	
    	// These need to be parameterized. 
    	this.incubationSelectionType = incubationSelectionType;
    	this.incubationType = incubationAssistanceType;
    	this.pIncubateWithResource = pIncubateWithResource;
    	this.networkLimit = networkLimit;
    	this.incubationMoneyQuantity = incubationMoneyQuantity;
    	this.incubationResourceQuantity = incubationResourceQuantity;
    	this.incubationStartStep = incubationStartStep;
    	this.incubationEndStep = incubationEndStep;
    }
    
    
    /*
     * The environment is where reproduction, incubation, investment (harvesting), and dissipation occur. These processes take place here, instead
     * of at the agent level because they are things that the world does in response to performance. If a firm is successful, somebody
     * tries to copy its business model; somebody invests in it; and somebody offers to incubate it. Dissipation occurs here because currently, 
     * agents without energy cannot dissipate, and so we have to give them some energy first. If they're successful, there will be energy left
     * after dissipation. Remember, the purpose of dissipation is to kill agents so that only the strong survive and thrive.  
     */
    public void step(SimState state) {
        final Eco2Sim s = (Eco2Sim) state;
        
        // prep for fitness calculations
        calculateMaximumValues(s);
        
        reproduce(s);
         
        invest(s);
        
        if (state.schedule.getSteps() > this.incubationStartStep && state.schedule.getSteps() < this.incubationEndStep) {
        	incubate(s);
        }
        
        dissipate(s);
    }
        
    private void calculateMaximumValues(Eco2Sim state) {
    	Bag agents = new Bag(state.world.allObjects);

		maxBuys   = 0;
		maxSells  = 0;
		maxEnergy = 0;
		for (Object o : agents) {
			final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
			if (art.inputQuantityBought > maxBuys)
				maxBuys = art.inputQuantityBought;
			if (art.outputQuantitySold > maxSells) {
				maxSells = art.outputQuantitySold; 
			}
			final double energy = art.inputQuantity + art.outputQuantity + art.moneyQuantity;
			if (energy > maxEnergy)
				maxEnergy = energy;
		}
    }
    
    private boolean isAgentFit(MersenneTwisterFast rng, AdaptiveResourceTransformer agent, FitnessType fitnessType) {
    	/*
    	 * Determine whether or not an agent is fit based on a proportional fitness. 
    	 */
    	final double p;
		switch (fitnessType) {
		case BUY:
			p = (agent.inputQuantityBought + 1.0) / (maxBuys + 1.0);
			break;
		case SELL:
			p = (agent.outputQuantitySold + 1.0) / (maxSells + 1.0);
			break;
		case BUY_OR_SELL:
			p = (agent.inputQuantityBought + 1.0) / (maxBuys + maxSells + 1.0)
			  + (agent.outputQuantitySold + 1.0) / (maxBuys + maxSells + 1.0);
			break;
		case BUY_AND_SELL:
			p = (agent.inputQuantityBought + 1.0) / (maxBuys + 1.0)
			  * (agent.outputQuantitySold + 1.0) / (maxSells + 1.0);
			break;
		case ENERGY:
			final double energy = agent.inputQuantity + agent.outputQuantity + agent.moneyQuantity;
			p = (energy + 1.0) / (maxEnergy + 1.0);
			break;
		case EVERYONE:
			p = 1;
			break;
		default:
			p = 1;
			break;
		}// age???

		final double x = rng.nextDouble();
		return x < p;
    }    
	
    private boolean isAgentFitForIncubation(Eco2Sim state, AdaptiveResourceTransformer agent, IncubationSelectionType selectionType) {
    	/*
    	 * Determine whether or not an agent is fit based on a proportional fitness. 
    	 */
    	final double p;
    	final double energy = agent.inputQuantity + agent.outputQuantity + agent.moneyQuantity;
    	boolean selected = false;
    	double x = state.random.nextDouble();
    	
		switch (selectionType) {
		case BEST:
			p = (energy + 1.0) / (maxEnergy + 1.0);
			break;
		case WORST:
			p = 1 - (energy + 1.0) / (maxEnergy + 1.0);
			break;
		case YOUNGEST:
			p = 1 - (agent.getAge() + 1.0) / (state.schedule.getSteps() + 1.0);
			break;
		case OLDEST:
			p = (agent.getAge() + 1.0) / (state.schedule.getSteps() + 1.0);
			break;
		case BEST_YOUNGEST:
			p = ( (energy + 1.0) / (maxEnergy + 1.0) ) *
				(1 - (agent.getAge() + 1.0) / (state.schedule.getSteps() + 1.0));
			break;
		case WORST_YOUNGEST:
			p = (1 - (energy + 1.0) / (maxEnergy + 1.0) ) *
				(1 - (agent.getAge() + 1.0) / (state.schedule.getSteps() + 1.0));
			break;
		case RANDOM:
			p = 1;
			break;
		default:
			p = 1;
			break;
		}
		selected =  x < p/4;
		return selected;
    }
	
	public void reproduce(Eco2Sim state) {
		// calculate the mean energy of the population
		final double meanEnergy = state.getMeanEnergy();
		
		for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            if(art.getEnergy() >= meanEnergy && isAgentFit(state.random, art, reproductionFitnessType)) {//chk
            	final AdaptiveResourceTransformer child = art.reproduce(state);
				if (child != null) {
					state.world.setObjectLocation(child, child.location.x, child.location.y);
					child.stoppableReference = state.schedule.scheduleRepeating(child, 1, 1.0);
					
					state.firmsBorn++;
				}
        	}
		}
	}
	
	
	private void dissipate(final Eco2Sim state) {
    	for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            art.dissipate(state);
    	}
    }
	
    private void invest(final Eco2Sim state) {
    	for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
	    		
	    	if(isAgentFit(state.random, art, injectionFitnessType)) {
	            art.harvest(state);
	        }
    	}
    }
    
    public void incubate(final Eco2Sim state) {
    	int incubatorSize = 300;
    	int incubated = 0;
    	double percentIncubated = 0;
    	for(Object o : new Bag(state.world.allObjects)) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            double x;
	    	if(isAgentFitForIncubation(state, art, incubationSelectionType)) {//chk
	    		incubated++;
	            switch(incubationType) {
	            case MONEY:
	            	incubateWithMoney(art);
	            	break;
	            case RESOURCES:
	            	incubateWithResources(art);
	            	break;
	            case RANDOM:
	            	x = state.random.nextDouble();
	            	if ( x > 0.5) {
	            		incubateWithMoney(art);
	            	} else {
		            	incubateWithResources(art);
	            	}
	            	break;
	            case PROPORTIONAL:
	            	x = state.random.nextDouble();
	            	if (x < pIncubateWithResource) {
	            		incubateWithResources(art);
	            	} else {
	            		incubateWithMoney(art);
	            	}
	            	break;
	            case NETWORKING: 
	            	if(art.vr < state.vrMax * networkLimit)
	            			incubateWithNetworking(art);
	            	break;
	            case NETWORKING_AND_RESOURCES:
	            	incubateWithResources(art);
	            	if(art.vr < state.vrMax * networkLimit)
	            		incubateWithNetworking(art);
	            	break;
	            case NETWORKING_AND_MONEY:
	            	incubateWithMoney(art);
	            	if(art.vr < state.vrMax * networkLimit)
	            		incubateWithNetworking(art); 
	            	break;
	            case PROPORTIONAL_AND_NETWORKING:
	            	x = state.random.nextDouble();
	            	if (x < pIncubateWithResource) {
	            		incubateWithResources(art);
	            	} else {
	            		incubateWithMoney(art);
	            	}
	            	if(art.vr < state.vrMax * networkLimit)
	            		incubateWithNetworking(art); 
	            	break;
	            case NONE:
	            	break;
	            }
	            
	        }
//	    	percentIncubated = (double)incubatorSize / (double)state.getPopulation();
//	    	if(percentIncubated> 0.05)
//	    		break;
	    	if(incubated >= incubatorSize)
	    		break;
    	}
    	
    	System.out.println(state.schedule.getSteps() + ", "+ (double)(percentIncubated) + ", " + state.getIncubationSelectionType());
    }
    
    public void incubateWithResources(AdaptiveResourceTransformer art) {
    	art.inputQuantity += incubationResourceQuantity;
    	art.inputQuantityInjected += incubationResourceQuantity;
    }
    
    public void incubateWithMoney(AdaptiveResourceTransformer art) {
    	art.moneyQuantity += incubationMoneyQuantity;
    	art.moneyInjected += incubationMoneyQuantity;
    }
    public void incubateWithNetworking(AdaptiveResourceTransformer art) {
    	art.vr++;
    }
}