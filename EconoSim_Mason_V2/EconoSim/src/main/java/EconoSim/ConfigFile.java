package EconoSim;

import java.io.*;
import java.lang.reflect.Field;

/**
 * The ConfigFile class reads from the specified configuration file and loads parameters as specified
 * Note: any line in the config file that contains a ":" will be considered as a parameter and the 
 * reader will attempt to load it into the simulation
 *  
 * @author miakbas
 * @author cgunaratne
 * 
 */
public class ConfigFile {

	private String fileName;
	private BufferedReader reader;
	private Eco2Sim state;
	
	public ConfigFile() {
		try {
		reader = new BufferedReader(new FileReader(fileName));
		} catch (FileNotFoundException e) {
			return;
		}
	}
	
	public ConfigFile(Eco2Sim state, String fileName) {
		this.fileName = fileName;
		this.state = state;
		try {
		reader = new BufferedReader(new FileReader(this.fileName));
		} catch (FileNotFoundException e) {
			return;
		}
	} 

	public String getFileLocation() {
		return fileName;
	}

	public void setFileLocation(String fileLocation) {
		this.fileName = fileLocation;
		try {
			reader = new BufferedReader(new FileReader(fileLocation));
			} catch (FileNotFoundException e) {
				return;
		}
	}
	
	
	public void read() {
		if(fileName != null) {
			Class c = state.getClass();
			Field[] fields = c.getFields();
			try {
				while (true) { 
						String line = reader.readLine();
						if (line == null )
							break;
						if( line.contains(":")) {
							String key = line.substring(0, line.indexOf(":")).replaceAll("\\s", "");
							String value = line.substring(line.indexOf(":")+1).replaceAll("\\s", "");
							for(Field field: fields){
								if(field.getName().equalsIgnoreCase(key)){
									String type = field.getType().getName();
									if (type.equals("java.lang.String")) {
										field.set(state, value);
									} else {
										if (type.equals("int")) {
											field.set(state, Integer.valueOf(value));
										} else {
											field.set(state, Double.valueOf(value));
										}
									} 
									break;
								}
							}
						}
				} 
				
			} catch (IOException e) {
				return;
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (Exception e) {
				
			}
			
		}
	}
}
