package EconoSim;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.print.attribute.standard.NumberOfDocuments;

import EconoSim.Environment.IncubationSelectionType;
import EconoSim.Environment.IncubationAssistanceType;
import sim.engine.Schedule;
import sim.engine.SimState;
import sim.field.grid.Grid2D;
import sim.field.grid.SparseGrid2D;
import sim.util.Bag;
import sim.util.Int2D;
import sim.util.IntBag;
import sim.util.distribution.Normal;

public class Eco2Sim extends SimState {	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6571834101244638311L;
	// World Parameters
	public int worldHeight    = 100;
	public int worldWidth     = 100;
	public double creationDiameter = 0.1;
	
	public SparseGrid2D world = new SparseGrid2D(worldWidth, worldHeight);
	
    // Ecosystem Parameters
    public int          numFactors  = 1;
    public int          numProducts = 1;

    public int          numAgents   = 0;	
    
    // Agent Parameters
    public int			vrMean      = 2;
    public int			vrStd       = 5;
    public int 			vrMax		= Math.min(worldWidth, worldHeight)/2;

	public double initialResourceQuantity = 0;
    public double initialMoneyQuantity    = 0;
    
	public double dissipationProbability = 0.25;
	public double dissipationAmount      = 100.0;
    
	public int priceChangeThresholdMean    = 10;
	public int priceChangeThresholdStd     = 0;
	public double priceChangeAmount        = 1;
    public double minimumPrice             = 1;
    public double minimumDemand            = 1;
    
    // Agent Reproduction Parameters
	public double mutationProbability = 0.5;
	public double childContribution   = 0.5;
    
    // Environment Parameters
    private Environment.FitnessType reproductionFitnessType = Environment.FitnessType.ENERGY;
    private Environment.FitnessType injectionFitnessType    = Environment.FitnessType.SELL;
    public  double	injectionProbability    = 0.9;
    private double	externalEnergyFlow      = 110.0;
    
    private Environment.IncubationSelectionType incubationSelectionType = Environment.IncubationSelectionType.YOUNGEST;
	private Environment.IncubationAssistanceType incubationAssistanceType = Environment.IncubationAssistanceType.PROPORTIONAL;
	private double networkLimit = 0.05; //used only during networking assistance for incubation
	private double pIncubateWithResource = 0.8; //used only for proportional assistance for incubation
    private double incubationMoneyQuantity = 200.0;
    private double incubationResourceQuantity = 200.0;
	private double incubationStartStep = 2900;
	

	private double incubationEndStep = 3000;
    private double cumGDP = 0;
    
	// Population Measures
    public int firmsBorn = 0;
    public int firmsKilled = 0;
    
    public Map<String, Object> dataMap;
    public Map<String, List<Double>> vectorMap;
    
    public String dbName = "eco2sim_gui.db";
    public String tableName = "Simulation";
    
    
    // Configuration Properties
    public String getDBName() { return dbName; }
    public void   setDBName(String val) { if(val.length() > 0) { dbName = val; } }
    
    public String getTableName() { return tableName; }
    public void   setTableName(String val) { if(val.length() > 0) { tableName = val; } }
    
    // [input] World Properties
    public int  getWorldHeight() { return worldHeight; }
    public void setWorldHeight(final int val) { if(val >= 0) worldHeight = val; }
    
    public int  getWorldWidth() { return worldWidth; }
    public void setWorldWidth(final int val) { if(val >= 0) worldWidth = val; }
    
    public double  getCreationDiameter() { return creationDiameter; }
    public void setCreationDiameter(final double val) { if(val >= 0.0) creationDiameter = val; }
    
    // Ecosystem Properties
//    public int  getNumFactors() { return numFactors; }
//    public void setNumFactors(final int val) { if(val >= 0) numFactors = val; }
//    
//    public int  getNumProducts() { return numProducts; }
//    public void setNumProducts(final int val) { if(val >= 0) numProducts = val; }
    
//    public int  getNumAgents() { return numAgents; }
//    public void setNumAgents(final int val) { if(val >= 0) numAgents = val; }
    
//    public void setInitialPopulation(final int val) { if(val >= 0) numAgents = val; }
	
    // Agent Properties
//    public int  getVrMean() { return vrMean; }
//    public void setVrMean(final int val) { if(val >= 0) vrMean = val; }
//    
//    public int  getVrStd() { return vrStd; }
//    public void setVrStd(final int val) { if(val >= 0) vrStd = val; }
    
    public double  getInitialResourceQuantity() { return initialResourceQuantity; }
    public void setInitialResourceQuantity(final double val) { if(val >= 0) initialResourceQuantity = val; }
    
    public double  getInitialMoneyQuantity() { return initialMoneyQuantity; }
    public void setInitialMoneyQuantity(final double val) { if(val >= 0) initialMoneyQuantity = val; }
    
    public double getDissipationProbability() {return dissipationProbability; }
    public void   setDissipationProbability(final double val) { if(val >= 0.0 && val <= 1.0) dissipationProbability = val; }
    
    public double getDissipationAmount() {return dissipationAmount; }
//    public void   setDissipationAmount(final double val) { if(val >= 0.0 && val <= 1.0) dissipationAmount = val; }
    public void   setDissipationAmount(final double val) { if(val >= 0) dissipationAmount = val; }
    
//    public int  getPriceChangeThresholdMean() { return priceChangeThresholdMean; }
//    public void setPriceChangeThresholdMean(final int val) { if(val >= 0) priceChangeThresholdMean = val; }
//    
//    public int  getPriceChangeThresholdStd() { return priceChangeThresholdStd; }
//    public void setPriceChangeThresholdStd(final int val) { if(val >= 0) priceChangeThresholdStd = val; }
    
    public double  getPriceChangeAmount() { return priceChangeAmount; }
    public void setPriceChangeAmount(final double val) { if(val >= 0) priceChangeAmount = val; }
    
    public double  getMinimumPrice() { return minimumPrice; }
    public void setMinimumPrice(final double val) { if(val >= 0) minimumPrice = val; }
    
    public double  getMinimumDemand() { return minimumDemand; }
    public void setMinimumDemand(final double val) { if(val >= 0) minimumDemand = val; }
    
    // Agent Reproduction Properties
    public double  getMutationProbability() { return mutationProbability; }
    public void    setMutationProbability(final double val) { if(val >= 0 && val <= 1.0) mutationProbability = val; }
    
    public double  getChildContribution() { return childContribution; }
    public void    setChildContribution(final double val) { if(val >= 0 && val <= 1.0) childContribution = val; }
    
    
    
    // Environment Properties
//    public SystemType getSystemType() { return systemType; }
//    public boolean isSystemOpen() { return systemType == Environment.SystemType.OPEN; }
//    public void setSystemOpen(boolean type) { 
//    	if(type) 
//    		systemType = Environment.SystemType.OPEN;
//    	else 
//    		systemType = Environment.SystemType.CLOSED; 
//    }
    
    public double getInjectionProbability() {return injectionProbability; }
    public void   setInjectionProbability(final double val) { if(val >= 0.0 && val <= 1.0) injectionProbability = val; }

    public double  getExternalEnergyFlow() { return externalEnergyFlow; }
	public void setExternalEnergyFlow(double val) { if (val >= 0) { externalEnergyFlow = val; } }
    
    
    // [input] Aggregate Properties
    public int getNumberOfResources() { return numFactors + numProducts; }
	public void setNumberOfResources(int val) {
		if (val >= 2) {
			numFactors = (int) Math.floor(val / 2.0);
			numProducts = (int) Math.ceil(val / 2.0);
		}
	}
    
//    public double getPopulationResourceDensity() { return numAgents / Math.pow(getNumberOfResources(), 2.0); }
//	public void   setPopulationResourceDensity(double val) {
//		if (val >= 0) {
//			numAgents = (int) (val * Math.pow(getNumberOfResources(), 2.0));
//		}
//	}
    
//    public int  getInitialUnitEnergy() { return initialResourceQuantity + initialMoneyQuantity; }
//    public void setInitialUnitEnergy(int val) { 
//    	if(val >= 0) {
//    		initialResourceQuantity = (int) Math.ceil(val / 2.0);
//    		initialMoneyQuantity = (int) Math.ceil(val / 2.0);
//    	}
//    }
    
    public int  getInteractionRadius() { return vrMean; }
    public void setInteractionRadius(final int val) { 
    	if(val >= 0) { 
    		vrMean = val;
    		vrStd = 0;
    	}
    }
    
    public int  getPriceChangeThreshold() { return priceChangeThresholdMean; }
    public void setPriceChangeThreshold(final int val) { 
    	if(val >= 0) { 
    		priceChangeThresholdMean = val;
    		priceChangeThresholdStd = 0;
    	}
    }
    
//    public int  getExternalEnergyFlow() { return externalResourceFlow + externalMoneyFlow; }
//	public void setExternalEnergyFlow(int val) {
//		if (val >= 1) {
//			externalResourceFlow = (int) Math.floor(val / 2.0);
//			externalMoneyFlow = (int) Math.floor(val / 2.0);
//		}
//	}
	
    public double getCumGDP() {
		return cumGDP;
	}
    
	public void setCumGDP(double cumGDP) {
		this.cumGDP = cumGDP;
	}
    
    
    
    // [output] Population Properties
    public int getPopulation() {
    	return world.allObjects.size();
    }
    
    public int getActivePopulation() {
    	int val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		// an agent is active if it either bought or sold resources
    		if(agent.getInputResourcesBought() + agent.getOutputResourcesSold() > 0) {
    			val ++;
    		}
    	}
    	return val;
    }
    
   public Map<Int2D,Integer> getActiveTechDistribution() {
    	int[] result = new int[getNumberOfResources()*getNumberOfResources()];
    	Bag agents = new Bag(world.allObjects);
    	
    	//find active rules
    	Map<Int2D,Integer> rules = new HashMap<Int2D,Integer>();
    	for(int i = 0; i < agents.numObjs; i++) {
    		Int2D rule = ((AdaptiveResourceTransformer) agents.objs[i]).getTransformationRule();
    		if(rules.containsKey(rule)) {
    			rules.replace(rule, ((Integer)rules.get(rule))+1);
    		} else {
    			rules.put(rule, 1);
    		}
    	}
    	
    	/*for(int i = 0; i < getNumberOfResources(); i++){
    		for (int j = 0; j < getNumberOfResources(); j++){
    			for(Object o : agents) {
            		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
            		// an agent is active if it either bought or sold resources
            		if((agent.getInputResourcesBought() + agent.getOutputResourcesSold() > 0) && agent.getInput() == i && agent.getOutput() == j){
            			result[(i)* getNumberOfResources() + j] ++; 
            		}
            	}
    		}
    	}*/    	
    	return rules;
    }
    public int getFirmsBorn() {
    	return firmsBorn;
    }
    
    public int getFirmsKilled() {
    	return firmsKilled;
    }
    
    public int[] getAgeDistribution() {
    	Bag agents = new Bag(world.allObjects);
    	int[] results = new int[agents.numObjs];
    	for(int i = 0; i < agents.numObjs; i++) {
    		results[i] = ((AdaptiveResourceTransformer) agents.objs[i]).getAge();
    	}
    	return results;
    }
    
    public double[] getPriceDistribution() {
    	Bag agents = new Bag(world.allObjects);
    	double[] results = new double[agents.numObjs];
    	for(int i = 0; i < agents.numObjs; i++) {
    		results[i] = ((AdaptiveResourceTransformer) agents.objs[i]).getOutputPrice();
    	}
    	return results;
    }
    
    public double[] getGrossProfitDistribution() {
    	Bag agents = new Bag(world.allObjects);
    	double[] results = new double[agents.numObjs];
    	for(int i = 0; i < agents.numObjs; i++) {
    		results[i] = ((AdaptiveResourceTransformer) agents.objs[i]).getMoneyEarned() - ((AdaptiveResourceTransformer) agents.objs[i]).getMoneySpend();
    	}
    	return results;
    }
    
    public int getInjectionSetSize() {
    	int val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		// an agent is active if it either bought or sold resources
    		if(agent.getEnergyInjected() > 0) {
    			val ++;
    		}
    	}
    	return val;
    }
    
    public int getDissipationSetSize() {
    	int val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		// an agent is active if it either bought or sold resources
    		if(agent.getEnergyDissipated() > 0) {
    			val ++;
    		}
    	}
    	return val;
    }
    
    // [output] Energy properties
    public double getTotalInputQuantity() {
    	double x = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getInputResourceQuantity();
    	}
    	return x;
    }
    
    public double getTotalOutputQuantity() {
    	double x = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getOutputResourceQuantity();
    	}
    	return x;
    }
    
    public double getTotalOutputProduced() {
    	double x = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getOutputProduced();
    	}
    	return x;
    }
    
    public double getValueOfTotalOutputProduced() {
    	double x = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getOutputProduced() * agent.getOutputPrice();
    	}
    	return x;
    }
    
    public double getConsumerExpenditure() {
    	double x = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getInputResourcesBought() * agent.getMoneySpend();
    	}
    	return x;
    }
    
    public double getGDP () {
    	double x = 0;
    	//consumer expenditure + investment + Exports - imports
    	x = getConsumerExpenditure();
    	return x;
    }
    public double getGDPPerCapita() {
		return getGDP()/getPopulation();
	}
    public double getTotalMoney() {
    	double tmq = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		tmq += agent.getMoneyQuantity();
    	}
    	return tmq;
    }
    
    public double getTotalResources() {
    	double x = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getInputResourceQuantity() + agent.getOutputResourceQuantity();
    	}
    	return x;
    }
    
    public double getTotalEnergy() {
    	double x = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getEnergy();
    	}
    	return x;
    }
    
    public double getMeanEnergy() {
    	double meanEnergy = 0.0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
            final AdaptiveResourceTransformer art = (AdaptiveResourceTransformer) o;
            meanEnergy += art.getEnergy();
		}
		meanEnergy = Math.ceil( meanEnergy / world.size());
		
		return meanEnergy;
	}
    
    public double getEnergyPerCapita() {
    	double x = 0.0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		x += agent.getEnergy();
    	}
    	return x / getPopulation();
    }
    
    public double[] getEnergyDistribution() {
    	Bag agents = new Bag(world.allObjects);
    	double[] results = new double[agents.numObjs];
    	for(int i = 0; i < agents.numObjs; i++) {
    		results[i] = ((AdaptiveResourceTransformer) agents.objs[i]).getEnergy();
    	}
    	return results;
    }
    
    
    // [output] Rule Properties
    
    /*public Int2D[] getRuleDistribution() {
    	Bag agents = new Bag(world.allObjects);
    	
    	List<Int2D> rules = new ArrayList<Int2D>();
    	for(int i = 0; i < agents.numObjs; i++) {
    		rules.add(((AdaptiveResourceTransformer) agents.objs[i]).getTransformationRule());
    	}
    	
    	
    	return rules.toArray(new Int2D[rules.size()]);
    }*/
    public double[] getRuleDistribution() {
    	Bag agents = new Bag(world.allObjects);
    	
    	double[] rules = new double[agents.numObjs];
    	for(int i = 0; i < agents.numObjs; i++) {
    		rules[i] = ((AdaptiveResourceTransformer) agents.objs[i]).getTransformationRuleWord();
    	}
    	
    	
    	return rules;
    }
    
    public double[] getOutputSoldDistribution() {
    	Bag agents = new Bag(world.allObjects);
    	
    	double[] result = new double[agents.numObjs];
    	for(int i = 0; i < agents.numObjs; i++) {
    		result[i] = ((AdaptiveResourceTransformer) agents.objs[i]).getOutputResourcesSold();
    	}
    	
    	
    	return result;
    }
    
    public double getProductCount() {    	
    	Bag agents = new Bag(world.allObjects);
    	
    	Set<Integer> products = new HashSet<Integer>();
    	for(int i = 0; i < agents.numObjs; i++) {
    		products.add(((AdaptiveResourceTransformer) agents.objs[i]).getTransformationRule().y);
    	}
    	
    	return (double) products.size();
    }
    
    public double getTransformationNetworkEdgeCount() {    	
    	Bag agents = new Bag(world.allObjects);
    	
    	Set<Int2D> rules = new HashSet<Int2D>();
    	for(int i = 0; i < agents.numObjs; i++) {
    		rules.add(((AdaptiveResourceTransformer) agents.objs[i]).getTransformationRule());
    	}
    	
    	return (double) rules.size();
    }
    
    public double getTransformationNetworkDensity() {
    	// there are N * (N-1) possible rules since we do not allow (x, x)
    	// the number of rules in use is given by the size of the rule distribution
    	final int nor = this.getNumberOfResources();
    	
    	return (double) getTransformationNetworkEdgeCount() / (double) (nor * (nor - 1));
    }
    
    // [output] Flow Properties
    public double getMFF() {
    	// could be written as a fold over a list comprehension
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.moneySpent;
    	}
    	return val;
    }
    
    public double getMFG() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.moneyDissipated;
    	}
    	return val;
    }
    
    public double getMGF() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.moneyInjected;
    	}
    	return val;
    }
    
    public double getRFF() {
    	// could be written as a fold over a list comprehension
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.inputQuantityBought;
    	}
    	return val;
    }
    
    public double getRFG() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.inputQuantityDissipated + agent.outputQuantityDissipated;
    	}
    	return val;
    }
    
    public double getRGF() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.inputQuantityInjected + agent.outputQuantityInjected;
    	}
    	return val;
    }
    
    public double getTotalDissipation() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.inputQuantityDissipated + agent.outputQuantityDissipated + agent.moneyDissipated;
    	}
    	return val;
    }
    
    public double getTotalInjection() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.inputQuantityInjected + agent.outputQuantityInjected + agent.moneyInjected;
    	}
    	return val;
    }
    public double getTotalResourcesInjected() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.inputQuantityInjected + agent.outputQuantityInjected;
    	}
    	return val;
    }
    public double getTotalMoneyInjected() {
    	double val = 0;
    	Bag agents = new Bag(world.allObjects);
    	for(Object o : agents) {
    		final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
    		val += agent.moneyInjected;
    	}
    	return val;
    }
    public double getCTotalEnergy() {
    	if(dataMap != null)
    	return (Double) dataMap.get("cTotalEnergy");
    	return 0;
    }
    
    public double getMeanVR() {
		double val = 0;
		Bag agents = new Bag(world.allObjects);
		for(Object o : agents) {
			final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
			val += agent.vr;
		}
		return val/agents.size();
	}
    public double getIncubationStartStep() {
		return incubationStartStep;
	}
	public void setIncubationStartStep(double incubationStartStep) {
		this.incubationStartStep = incubationStartStep;
	}
	public double getIncubationEndStep() {
		return incubationEndStep;
	}
	public void setIncubationEndStep(double incubationEndStep) {
		this.incubationEndStep = incubationEndStep;
	}
    public Environment.IncubationSelectionType getIncubationSelectionType() {
		return incubationSelectionType;
	}
	public void setIncubationSelectionType( int incubationSelectionType) {
		//this.incubationSelectionType = (IncubationSelectionType)incubationSelectionType;
	}
	public Environment.IncubationAssistanceType getIncubationAssistanceType() {
			return incubationAssistanceType;
	}
	public void setIncubationAssistanceType(
			Environment.IncubationAssistanceType incubationAssistanceType) {
		this.incubationAssistanceType = incubationAssistanceType;
	}
    
	public Eco2Sim(long seed) {
		super(100);
	}
	
	public double getNetworkLimit() {
		return networkLimit;
	}
	public void setNetworkLimit(double networkLimit) {
		this.networkLimit = networkLimit;
	}
	
	public void start() {
		/*
		 * Simulation Runtime Sequence:
		 * 
		 * pre process
		 * 		reset the global tick variables
		 * 		reset the local tick variables for each agent
		 * step each agent
		 * 		move
		 * 		trade
		 * 		produce
		 * 		dissipate
		 * spawn new agents via reproduction
		 * step the environment
		 * 		injection
		 * 		incubation
		 * post process
		 * 		record agent data
		 * 		remove dead agents
		 * 		record population data
		 * 		update the transformation network
		 */
		try{
			super.start();
        } catch (Exception e){}
		ConfigFile config = new ConfigFile(this, "config.txt");
		//config.read();
		
        
        // create the world
        world = createWorld(worldWidth, worldHeight);
                
        PreProcessDaemon pred = new PreProcessDaemon();
        schedule.scheduleRepeating(Schedule.EPOCH, 0, pred, 1.0);
        
        // position the agents in the world
        // schedule the agents
        
        // create agents within a circle of radius 1/8
        //final List<AdaptiveResourceTransformer> agents = createAgents();
        createAgents(1);
        
        // create agents in random locations
//        final List<AdaptiveResourceTransformer> agents = createAgents();
//        for(AdaptiveResourceTransformer agent : agents) {
//            final int x = random.nextInt(worldWidth);
//            final int y = random.nextInt(worldHeight);
//            
//            world.setObjectLocation(agent, x, y);
//            agent.stoppableReference = schedule.scheduleRepeating(Schedule.EPOCH, 1, agent, 1.0);
//        }
        
        Environment env = new Environment(reproductionFitnessType, injectionProbability, injectionFitnessType, 
        		incubationSelectionType, incubationAssistanceType, 
        		pIncubateWithResource, networkLimit, 
        		incubationMoneyQuantity, incubationResourceQuantity, 
        		incubationStartStep, incubationEndStep);
        schedule.scheduleRepeating(Schedule.EPOCH, 2, env, 1.0);
        
        PostProcessDaemon postd = new PostProcessDaemon();
        schedule.scheduleRepeating(Schedule.EPOCH, 3, postd, 1.0);
       
        initializeDataMap();
	}
	
	private void initializeDataMap() {
		dataMap = new HashMap<String, Object>();
		dataMap.put("seed", seed());
		dataMap.put("numberOfResources", getNumberOfResources()); 
		dataMap.put("initialPopulation",getPopulation());
		dataMap.put("initialMoneyQuantity", this.initialMoneyQuantity);
		dataMap.put("initialResourceQuantity", this.initialResourceQuantity);
		dataMap.put("interactionRadius", getInteractionRadius());
		dataMap.put("dissipationProbability", getDissipationProbability());
		dataMap.put("dissipationAmount", getDissipationAmount());
		dataMap.put("priceChangeThreshold", getPriceChangeThreshold());
		dataMap.put("priceChangeAmount", getPriceChangeAmount());
		dataMap.put("minimumPrice", getMinimumPrice());
		dataMap.put("minimumDemand", getMinimumDemand());
		dataMap.put("mutationProbability", getMutationProbability());
		dataMap.put("childContribution", getChildContribution());
		dataMap.put("injectionProbability", getInjectionProbability());
		dataMap.put("externalEnergyFlow", getExternalEnergyFlow());
	}
    
	public void finish() {
		// Augment the data map
		dataMap.put("stopTime", schedule.getSteps());
		
		System.out.println("Simulation ended. Writing to database...");
		//for (Map.Entry<String, Double> entry : dataMap.entrySet()) {
		//	System.out.println(entry.getKey() + ": " + entry.getValue());
		//}
		
		List<String> columnNames = Arrays.asList(
				"repId",
				"seed",
				"numberOfResources",
				"initialPopulation",
				"initialMoneyQuantity",
				"initialResourceQuantity",
//				"initialUnitEnergy",
				"interactionRadius",
				"dissipationProbability",
				"dissipationAmount",
				"priceChangeThreshold",
				"priceChangeAmount",
				"minimumPrice",
				"minimumDemand",
				"mutationProbability",
				"childContribution",
				"injectionProbability",
				"externalEnergyFlow",
				"stopTime",
				"cPopulation",
				"cFirmsBorn",
				"cFirmsKilled",
				"cTotalMoney",
				"cMoneyExchanged",
				"cMoneyDissipated",
				"cMoneyInjected",
				"cTotalResources",
				"cResourcesExchanged",
				"cResourcesDissipated",
				"cResourcesInjected",
				"cTotalEnergy",
				"cTotalEnergyExchanged",
				"cTotalEnergyDissipated",
				"cTotalEnergyInjected",
				"cTransNetworkEdges",
				"cTransNetworkDensity"
		);
		
		String namesString = strJoin(columnNames, ',');
		String valuesString = createDataString(columnNames);
		//System.out.println(namesString);
		//System.out.println(valuesString);
		
		Connection c = null;
		Statement s =  null;
		try {
			// open the database
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
			c.setAutoCommit(false);
			
			s = c.createStatement();
			
			// create a table for this experiment
			// for each string in columnNames, append " INT," or " REAL, depending on the type"
			String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (" +  
					"repId                   TEXT," + 
					"seed                    INTEGER," +
					"numberOfResources       INTEGER," +
					"initialPopulation       INTEGER," +
					"initialMoneyQuantity    REAL," +
					"initialResourceQuantity REAL," +
//					"initialUnitEnergy       REAL," +
					"interactionRadius       INTEGER," +
					"dissipationProbability  REAL," +
					"dissipationAmount       REAL," +
					"priceChangeThreshold    INTEGER," +
					"priceChangeAmount       REAL," +
					"minimumPrice            REAL," +
					"minimumDemand           REAL," +
					"mutationProbability     REAL," +
					"childContribution       REAL," +
					"injectionProbability    REAL," +
					"externalEnergyFlow      REAL," +
					"stopTime                INTEGER," + 
					"cPopulation             REAL," +
					"cFirmsBorn              REAL," +
					"cFirmsKilled            REAL," +
					"cTotalMoney             REAL," + 
					"cMoneyExchanged         REAL," + 
					"cMoneyDissipated        REAL," + 
					"cMoneyInjected          REAL," + 
					"cTotalResources         REAL," + 
					"cResourcesExchanged     REAL," + 
					"cResourcesDissipated    REAL," + 
					"cResourcesInjected      REAL," + 
					"cTotalEnergy            REAL," +
					"cTotalEnergyExchanged   REAL," +
					"cTotalEnergyDissipated  REAL," + 
					"cTotalEnergyInjected    REAL," +
					"cTransNetworkEdges      INTEGER," +
					"cTransNetworkDensity    REAL"  +
					")";
			s.executeUpdate(sql);
			
			sql = "INSERT INTO " + tableName + " (" + namesString + ") VALUES (" + valuesString + ");";
			s.executeUpdate(sql);
			c.commit();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			for (Map.Entry<String, Object> entry : dataMap.entrySet()) {
				System.out.println(entry.getKey() + ": " + entry.getValue());
			}
		} finally {
			try {
				if (s != null)
					s.close();
			} catch (SQLException se2) {
				// nothing can be done
			}
			try {
				if (c != null)
					c.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Data written to database.");
		
		// double check data was written
		/*
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + dbName);
			c.setAutoCommit(false);
			
			s = c.createStatement();
			ResultSet rs = s.executeQuery("SELECT * FROM Simulation");
			while(rs.next()) {
				
				for(String cName : columnNames) {
					final Object val = rs.getObject(cName);
					System.out.println(cName + " = " + val);
				}
			} 
			rs.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
		} finally {
			try {
				if (s != null)
					s.close();
			} catch (SQLException se2) {
				// nothing can be done
			}
			try {
				if (c != null)
					c.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		*/
		
		super.finish();
	}
	
	private String strJoin(List<String> strings, char delim) {
		StringBuilder sb = new StringBuilder();
		for(String s : strings) {
			sb.append(s).append(delim).append(' ');
		}
		return sb.substring(0, sb.length() - 2); // -1 because of the extra space
	}
	
	private String createDataString(List<String> keys) {
		StringBuilder sb = new StringBuilder();
		for (String k : keys) {
			sb.append(dataMap.get(k)).append(", ");
		}
		return sb.substring(0, sb.length() - 2);
	}
	
	
	
    private SparseGrid2D createWorld(final int width, final int height) {
        return new SparseGrid2D(worldWidth, worldHeight);
    }
    
    private void createAgents(int order) {
    	IntBag nxs       = new IntBag();
		IntBag nys       = new IntBag();
		int creationRadius = (int) Math.ceil(worldWidth * creationDiameter * 1/2);
	    world.getRadialLocations(worldWidth / 2, worldHeight / 2, creationRadius, Grid2D.TOROIDAL, true, nxs, nys);
	    for(int i = 0; i < nxs.numObjs; i++) {
	    	final AdaptiveResourceTransformer agent = createAgent();
	    	final int x = nxs.objs[i];
	        final int y = nys.objs[i];
	        
	        world.setObjectLocation(agent, x, y);
	        agent.stoppableReference = schedule.scheduleRepeating(Schedule.EPOCH, order, agent, 1.0);
	        
	        numAgents = world.size();
	    }
    }
    
    private AdaptiveResourceTransformer createAgent() {
    	Normal vrGenerator  = new Normal(vrMean, vrStd, random);
    	Normal pctGenerator = new Normal(priceChangeThresholdMean, priceChangeThresholdStd, random);
    	List<Integer> tRule;System.out.println(getNumberOfResources());
    	//Chathika: Added to control creation of transformation rule.
    	tRule = generateTransformationRule(2);
    	
    	int newVR = (int) vrGenerator.nextDouble();
        return new AdaptiveResourceTransformer(
        		newVR > 0 ? newVR:1, 
        		tRule.get(0), 
        		tRule.get(1), 
        		minimumPrice,
        		minimumDemand, 
        		initialResourceQuantity,
        		initialMoneyQuantity,
        		dissipationProbability,
        		dissipationAmount,
        		(int) pctGenerator.nextDouble(), 
        		priceChangeAmount, 
        		mutationProbability,
        		childContribution);
    }
    //Chathika: Added to control creation of transformation rule.
    public List<Integer> generateTransformationRule(int upperLimitID) {
    	List<Integer> results = new ArrayList<Integer>(2);
    	
    	IntBag resourceList = createRandomSequence(upperLimitID);
    	int i = resourceList.get(0);
    	int o = resourceList.get(1);
    	
    	results.add(i);
    	results.add(o);
    	return results;
    }
    public List<Integer> generateTransformationRule() {
    	List<Integer> results = new ArrayList<Integer>(2);
    	
    	IntBag resourceList = createRandomSequence(numFactors + numProducts);
    	int i = resourceList.get(0);
    	int o = resourceList.get(1);
    	
    	results.add(i);
    	results.add(o);
    	return results;
    }
    
    
    /*
     * Miscellaneous Helper Methods
     */
    
    public IntBag createSequence(int size) {
        IntBag rb = new IntBag(size);
        for(int i = 0; i < size; i++) {
            rb.add(i);
        }
        return rb;
    }
    
    public IntBag createRandomSequence(int size) {
        IntBag rb = createSequence(size);
        rb.shuffle(random);
        return rb;
    }
    
    public void printBag(IntBag b) {
    	for(int i = 0; i < b.numObjs; i++) {
    		System.out.print(b.objs[i] + " ");
    	}
    	System.out.println();
    }
   
	public static void main(String[] args) {
//		for(int i = 100; i> 0; i = i - 20) {
//			Eco2Sim.numberOfResourceTypes = i;
//			doLoop(Eco2Sim.class, args);
//		}
//		Eco2Sim.numberOfResourceTypes = 1;
//		doLoop(Eco2Sim.class, args);
//		System.exit(0);
		
		int repeats = 1; // let�s do 30 runs
		;
		int[] noResources = new int[]{1,5};
		for(int caseNo = 0; caseNo < noResources.length; caseNo++)
		{
			SimState state = new Eco2Sim(100); // MyModel is our SimState subclass
			state.nameThread();
			((Eco2Sim)state).setNumberOfResources((int) Math.pow(2,noResources[caseNo]));
			System.out.println(((Eco2Sim)state).getNumberOfResources());
			//((Eco2Sim)state).setNumberOfResources(10);
			for(int repeat = 0; repeat < repeats;repeat++)
			{
				
				state.setJob((caseNo*repeats) + repeat);
				state.start();
				do
				if (!state.schedule.step(state)) 
					break;
				while(state.schedule.getSteps() < 3000);
				state.finish();
			}
		}
		for(int caseNo = 0; caseNo < noResources.length; caseNo++)
		{
			SimState state = new Eco2Sim(100); // MyModel is our SimState subclass
			state.nameThread();
			((Eco2Sim)state).setIncubationStartStep(900);
			((Eco2Sim)state).setIncubationEndStep(1000);
			((Eco2Sim)state).setNumberOfResources((int) Math.pow(2,noResources[caseNo]));
			System.out.println(((Eco2Sim)state).getNumberOfResources());
			//((Eco2Sim)state).setNumberOfResources(10);
			for(int repeat = 0; repeat < repeats;repeat++)
			{
				
				state.setJob((caseNo*repeats) + repeat);
				state.start();
				do
				if (!state.schedule.step(state)) 
					break;
				while(state.schedule.getSteps() < 3000);
				state.finish();
			}
		}
		System.exit(0);
	}
}