package EconoSim;

import java.util.ArrayList;
import java.util.List;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.field.grid.Grid2D;
import sim.util.Bag;
import sim.util.Int2D;
import sim.util.IntBag;

public class AdaptiveResourceTransformer implements Steppable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 3597866718094002546L;

	// Parameters (things that are fixed)
	public int vr;
    
	public int input;
	public int output;
    
	public double dissipationProbability;
	public double dissipationAmount;
    
	public int    priceChangeThreshold;
	public double priceChangeAmount;
    public double minimumPrice;
    
    public double reproductionEnergyThreshold;
	public double mutationProbability;
	public double childContribution;
    
    // Local World State
	public Int2D location;
    
	public  Bag    neighbors = new Bag();
	public  IntBag nxs       = new IntBag();
	public  IntBag nys       = new IntBag();
    
    // Local State (things that change)    
	public int age;
    
	public double inputQuantity;
	public double outputQuantity;
    public double moneyQuantity;
    
    public double outputPrice;
    
    public double currentDemand;
    public double previousDemand;
    public double minimumDemand;
    
    public double inputQuantityBought;
    public double moneySpent;
    public double outputQuantitySold;
    public double moneyEarned;
    
    public double inputQuantityConsumed; // inputConsumed
    public double outputQuantityProduced; // outputProduced
    
    

	public double inputQuantityDissipated; // inputDissipated
    public double inputQuantityInjected; // inputInjected
    public double outputQuantityDissipated; // outputDissipated
    public double outputQuantityInjected; // outputInjected
    public double moneyDissipated;  // moneyDissipated
    public double moneyInjected;  // moneyInjected
    
    // At the present time, we do not care about the flow of energy between 
    // parents and child, i.e. irc, orc, mc, irp, orp, mp.  
    
    private int consecutiveSellActions;
    
    // Other State
    public AdaptiveResourceTransformer tradePartner;
    
    // Misc State Objects
    public Stoppable stoppableReference;
    
    public AdaptiveResourceTransformer(
    		int vr, 
    		int input, 
    		int output, 
    		double minp, 
    		double mind, 
    		double irq, 
    		double mq, 
    		double dp, 
    		double da, 
    		int pct, 
    		double pca, 
    		double mp, 
    		double cc) {   	
    	//interaction variables
        this.vr                       = vr;
        
        this.input                    = input;
        this.output                   = output;
        
        this.dissipationProbability   = dp;
        this.dissipationAmount        = da;
        
    	this.age                      = 0;
        
        // Transaction Variables
        this.inputQuantity            = irq;
        this.outputQuantity           = 0;
        this.moneyQuantity            = mq;
        
        this.minimumPrice             = minp;
        this.outputPrice              = this.minimumPrice;
        
        this.minimumDemand            = mind;
        this.currentDemand            = this.minimumDemand;
        this.previousDemand           = this.minimumDemand; 
        
        this.inputQuantityBought      = 0;
        this.outputQuantitySold       = 0;
        this.moneySpent               = 0;
        this.moneyEarned              = 0;
        
        // Transformation Variables
        this.inputQuantityConsumed    = 0;
        this.outputQuantityProduced   = 0;
        
        // External Flow Variables
        this.inputQuantityDissipated  = 0;
        this.inputQuantityInjected    = 0;
        this.outputQuantityDissipated = 0;
        this.outputQuantityInjected   = 0;
        this.moneyDissipated          = 0;
        this.moneyInjected            = 0;
        
        // Reproduction Variables
        this.mutationProbability      = mp;
        this.childContribution        = cc;
        
        // Price Variables
        this.consecutiveSellActions   = 0;
        this.priceChangeThreshold     = pct;
        this.priceChangeAmount        = pca;
    }
    
    // Java-Bean Property Methods
    public Bag getNeighbors() { return neighbors; }
    
    public int getAge() { return age; }
    public int getVisionRadius() { return vr; }    
    
    public int getInput() { return input; }
    public void setInput(final int val) { if(val >= 0) input = val; }
    
    public int getOutput() { return output; }
    public void setOutput(final int val) { if(val >= 0) output = val; }
    
    public double getDissipationProbability() { return dissipationProbability; }
    public double getDissipationAmount() { return dissipationAmount; }
        
    public double  getInputResourceQuantity() { return inputQuantity; }
    public void setInputResourceQuantity(final double val) { if(val >= 0) inputQuantity = val; }
    
    public double  getOutputResourceQuantity() { return outputQuantity; }
    public void setOutputResourceQuantity(final double val) { if(val >= 0) outputQuantity = val; }
    
    public double  getMoneyQuantity() { return moneyQuantity; }
    public void setMoneyQuantity(final double val) { if(val >= 0) moneyQuantity = val; }
    
    public double  getOutputPrice() { return outputPrice; }
    public void setOutputPrice(final double val) { if(val >= minimumPrice) outputPrice = val; }

    public double getPreviousDemand() { return previousDemand; }
    public double getCurrentDemand() { return currentDemand; }
    public int getConsecutiveSellActions() { return consecutiveSellActions; }
    
    public double getOutputResourcesSold() { return outputQuantitySold; }
    public double getMoneyEarned() { return moneyEarned; }
    public double getInputResourcesBought() { return inputQuantityBought; }
    public double getMoneySpend() { return moneySpent; }
    
    public double getEnergy() { return inputQuantity + outputQuantity + moneyQuantity; }
    
    public double getInputConsumed() { return inputQuantityConsumed; }
    public double getOutputProduced() { return outputQuantityProduced; }
    
    public double getInputResourceDissipated() { return inputQuantityDissipated; }
    public double getOutputResourceDissipated() { return outputQuantityDissipated; }
    public double getMoneyDissipated() { return moneyDissipated; }
    public double getEnergyDissipated() { return inputQuantityDissipated + outputQuantityDissipated + moneyDissipated; }
    
    public double getInputResourceInjected() { return inputQuantityInjected; }
    public double getOutputResourceInjected() { return outputQuantityInjected; }
    public double getMoneyInjected() { return moneyInjected; }
    public double getEnergyInjected() { return inputQuantityInjected + outputQuantityInjected + moneyInjected; }
    
    public AdaptiveResourceTransformer getTradePartner() { return tradePartner; } 
    
    public Int2D getTransformationRule() {
    	return new Int2D(input, output);
    }
    
    public double getTransformationRuleWord () {
    	String word = Integer.toString(input) + "." + Integer.toString(output);
    	return Double.valueOf(word);
    }
    
    // Core Behavior
    public void step(SimState state) {
        final Eco2Sim s = (Eco2Sim) state;

        location = getCurrentLocation(s);
        
        // Gather information about the surrounding neighborhood
        nxs       = new IntBag();
    	nys       = new IntBag();
        s.world.getRadialLocations(location.x, location.y, this.vr, Grid2D.TOROIDAL, false, nxs, nys);
        neighbors = s.world.getRadialNeighbors(location.x, location.y, this.vr, Grid2D.TOROIDAL, false, null, null, null);
        List<Int2D> emptySites = findEmptySites(s);
        
        // Execute core agent behavior
        tradePartner = selectTradePartner(s);
        move(s, emptySites);
        trade(s);
        produce();
    }
    
    // Helper Methods
    public void updateTickVariables() {
        updateDemand();
        updatePrice();
        updateAge();
    }
    
    private void updateDemand() {    	
    	previousDemand = currentDemand;
        currentDemand = minimumDemand;
    }
    
    private void updatePrice() {
    	// The price of a firm updates when output has been sold consecutively 
    	if(outputQuantitySold > 0) {
    		consecutiveSellActions++;
    	} else {
    		consecutiveSellActions--;
    	}
    	
    	// Increase the price after a period of successful sales
    	if(consecutiveSellActions > priceChangeThreshold) {
    		outputPrice = outputPrice + priceChangeAmount;
    		consecutiveSellActions = 0;
    	}
    	
    	// Decrease the price after a period of unsuccessful sales
    	if(consecutiveSellActions < 0 - priceChangeThreshold) {
    		outputPrice = (int) Math.max(outputPrice - priceChangeAmount, minimumPrice);
    		consecutiveSellActions = 0;
    	}
    }
    
    private void updateAge() {
    	age++;
    }
    
    public void resetFlowVariables() {
        this.inputQuantityBought = 0;
        this.moneySpent          = 0;
        this.outputQuantitySold  = 0;
        this.moneyEarned         = 0;
        
        this.inputQuantityConsumed    = 0;
        this.outputQuantityProduced   = 0;
        
        this.inputQuantityInjected    = 0;
        this.outputQuantityInjected   = 0;
        this.moneyInjected            = 0;
        
        this.inputQuantityDissipated  = 0;
        this.outputQuantityDissipated = 0;
        this.moneyDissipated          = 0;        
    }
    
    
    
    private Int2D getCurrentLocation(final Eco2Sim state) {
        return state.world.getObjectLocation(this);
    }
    
    
    
    private List<Int2D> findEmptySites(final Eco2Sim state) {
        // Find all unoccupied cells in the local area and randomize them
        List<Int2D> emptySites = new ArrayList<Int2D>();   
        
        IntBag rb = state.createRandomSequence(nxs.size());
        
        for(int i = 0; i < rb.size(); i++) {
            final int j = rb.objs[i];
            if(state.world.getObjectsAtLocation(nxs.objs[j], nys.objs[j]) == null) {
                emptySites.add(new Int2D(nxs.objs[j], nys.objs[j]));
            }
        }
        
        return emptySites;
    }
    
    
    
	private AdaptiveResourceTransformer selectTradePartner(final Eco2Sim state) {
        /*
         *  Select another agent within a radius of VR that produces the required
         *  resource with the lowest price. 
         */
    	
		neighbors.shuffle(state.random);
    	
		// Ideally, we want to have a list of trade partners so that we can work down
		// the list from best ratio to lowest, buying until the demand of an agent is
		// totally satisfied. Right now we just buy from a single agent and may have
		// our demand unsatisfied. 
        AdaptiveResourceTransformer tradePartner = null;         
        double maxTradeRatio = 0.0;
        for(Object o : neighbors) {
        	final AdaptiveResourceTransformer agent = (AdaptiveResourceTransformer) o;
            final double tradeRatio                 = this.moneyQuantity / agent.outputPrice;
            
          
            if((agent.output % 8) == (this.input % 8) && agent.outputQuantity > 0 && tradeRatio > maxTradeRatio) {
            	maxTradeRatio = tradeRatio;
                tradePartner  = agent;
            }
         }
         
         return tradePartner;
    }
    
	
	/**
	 * Given a list of visible locations, this method finds the empty site that 
	 * is closest to the agent's trade partner.
	 *  
	 * @param emptySites A List of locations in the world
	 * @return
	 */
    private Int2D findClosestEmptySite(final List<Int2D> emptySites) {
    	Int2D  closestSite = this.location;
        double minDistance = this.location.distance(tradePartner.location);
        for(Int2D site: emptySites) {
            final double d = site.distance(tradePartner.location);
            if(d < minDistance) {
                minDistance = d;
                closestSite = site;
            }
        }
        return closestSite;
    }
    
    
    
    private void move(final Eco2Sim state, final List<Int2D> emptySites) {        
        if(tradePartner != null) {
        	if(!emptySites.isEmpty()) {
	            // move to the closest empty cell near the trade partner
	            Int2D closestSite = findClosestEmptySite(emptySites);
	            state.world.setObjectLocation(this, closestSite.x, closestSite.y);
        	}
        } else {
            if(!emptySites.isEmpty()) {
                // move to a random empty site
                state.world.setObjectLocation(this, emptySites.get(0).x, emptySites.get(0).y);
            }
        }
    }
    
    /*
     * Trade has to be redone from single shot trading to trading with multiple agents until demand is
     * satisfied. This also means that we have to calculate demand in a new way - based off how many
     * times agents wanted to trade, even if they were unable to because THIS agent had insufficient
     * resources. 
     */
    private void trade(final Eco2Sim state) {
    	if(this.tradePartner != null) {
    		final double tradeCost = this.tradePartner.outputPrice;
    		if(this.moneyQuantity >= tradeCost) {
    			double tradeQuantity = Math.max(1, this.previousDemand - this.outputQuantity - this.inputQuantity);
    			
    			double resourcesTraded = 0;
    			while(this.tradePartner.outputQuantity > 0 && this.moneyQuantity > tradeCost && resourcesTraded < tradeQuantity) {
    				// Exchange resources and money
    				this.tradePartner.outputQuantity -= 1;
    				this.tradePartner.moneyQuantity += tradeCost;
    				
    				this.inputQuantity += 1;
    				this.moneyQuantity -= tradeCost;
    				
    				// Update trackers
    				this.tradePartner.outputQuantitySold += 1;
    				this.tradePartner.moneyEarned += tradeCost;
    				
    				this.inputQuantityBought += 1;
    				this.moneySpent += tradeCost;
  
    				resourcesTraded++;
    			}
    			
    			// increase the demand of the trade partner by the quantity of resources desired, and
    			// not by the quantity actually traded. 
    			this.tradePartner.currentDemand += tradeQuantity;
    		}
    	}
    }
    
    private void produce() {
        /*
         *  Production converts all available input resources into output resources.
         */
        if(inputQuantity > 0) {
        	// Calculate transformation quantities
        	final double consumedQuantity = inputQuantity;
        	final double producedQuantity = inputQuantity;
        	
        	// Transform resources
        	inputQuantity = inputQuantity - consumedQuantity;
            outputQuantity = outputQuantity + producedQuantity;
            
            // Update trackers
            inputQuantityConsumed = consumedQuantity;
            outputQuantityProduced = producedQuantity;
        }
    }
    
    public void dissipate(final Eco2Sim state) {
        if(state.random.nextDouble() <= dissipationProbability) {
        	double energy = inputQuantity + outputQuantity + moneyQuantity;
            double totalDissipated = 0;
            // remove energy from a random source 1 unit at a time
            while(energy > 0 && totalDissipated < this.dissipationAmount) {
                final int choice = state.random.nextInt(3);
                switch(choice) {
                    case 0:
                        if(inputQuantity > 0) {
                            inputQuantity--;
                            totalDissipated++;
                            inputQuantityDissipated++;
                        }
                        break;
                    case 1:
                        if(outputQuantity > 0) {
                            outputQuantity--;
                            totalDissipated++;
                            outputQuantityDissipated++;
                        }
                        break;
                    case 2:
                        if(moneyQuantity > 0) {
                            moneyQuantity--;
                            totalDissipated++;
                            moneyDissipated++;
                        }
                        break;
                }
                energy = inputQuantity + outputQuantity + moneyQuantity;
            }
        }
    }
    
    /**
     * This is an experimental method that localizes the injection process. Agents "harvest" the
     * environment and receive resources and money for "free".
     */
    public void harvest(final Eco2Sim state) {
    	if(state.random.nextDouble() <= state.getInjectionProbability()) {
            final double injectionQuantity = state.getExternalEnergyFlow();
                  double totalInjected     = 0;

            while(totalInjected < injectionQuantity) {
                final int choice = state.random.nextInt(2);
                switch(choice) {
				case 0:
					inputQuantity++;
					totalInjected++;
					inputQuantityInjected++;
					break;
				case 1:
					moneyQuantity++;
					totalInjected++;
					moneyInjected++;
					break;
                }
            }
        }
    }
    
    /**
     * Creates a new agent using THIS agent as a template
     */
    public AdaptiveResourceTransformer reproduce(Eco2Sim state) {
    	AdaptiveResourceTransformer child = null;
    	// can only reproduce if there is an empty visible site
    	nxs       = new IntBag();
    	nys       = new IntBag();
    	location  = getCurrentLocation(state);
    	state.world.getRadialLocations(location.x, location.y, this.vr, Grid2D.TOROIDAL, false, nxs, nys);
        neighbors = state.world.getRadialNeighbors(location.x, location.y, this.vr, Grid2D.TOROIDAL, false, null, null, null);
        
    	List<Int2D> emptySites = findEmptySites(state);
    	
    	if(!emptySites.isEmpty() && state.getPopulation() < 3000) {
    		List<Integer> childRule = generateChildRule(state);
    		
    		double childResourceQuantity = Math.floor(childContribution * outputQuantity);
    		double childMoneyQuantity    = Math.floor(childContribution * moneyQuantity);
    		
    		child = new AdaptiveResourceTransformer(
    				this.vr,
    				childRule.get(0),
    				childRule.get(1),
    				this.minimumPrice,
    				this.minimumDemand,
    				childResourceQuantity, 
    				childMoneyQuantity,
    				this.dissipationProbability,
    				this.dissipationAmount,
    				this.priceChangeThreshold,
    				this.priceChangeAmount,
    				this.mutationProbability,
    				this.childContribution);
    		child.location = emptySites.get(state.random.nextInt(emptySites.size()));
    		
    		this.outputQuantity -= childResourceQuantity;
    		this.moneyQuantity  -= childMoneyQuantity;
    	}
    	return child;
    }
    
    private List<Integer> generateChildRule(Eco2Sim state) {
    	List<Integer> results = new ArrayList<Integer>(2);
    	int i = this.input;
    	int o = this.output;
    	
    	
    	IntBag resourceList = state.createSequence(state.getNumberOfResources());
    	
    	if(state.random.nextDouble() < mutationProbability) {
    		switch(state.random.nextInt(4)) {
    		case 0: // i >= X
    			resourceList.remove(i);
    			o = resourceList.get(state.random.nextInt(resourceList.size()));
    			break;
    		case 1: // X -> i
    			o = i;
    			resourceList.remove(o);
    			i = resourceList.get(state.random.nextInt(resourceList.size()));
    			break;
    		case 2: // X -> o
    			resourceList.remove(o);
    			i = resourceList.get(state.random.nextInt(resourceList.size()));
    			break;
    		case 3: // X -> X
    			i = resourceList.get(state.random.nextInt(resourceList.size()));
    			resourceList.remove(i);
    			o = resourceList.get(state.random.nextInt(resourceList.size()));
    			break;
    		}
    	}
    	results.add(i);
    	results.add(o);
    	return results;
    }
}