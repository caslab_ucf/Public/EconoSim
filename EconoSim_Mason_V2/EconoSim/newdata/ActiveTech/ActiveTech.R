temp = data.frame(matrix(0, ncol = 3, nrow = 0))
colnames(temp) = c("Steps","Rule","Value")
i = 2
while (i <=  1669){
  temp2 = data.frame(c(1:3000),ActiveTechNoInc[,i],ActiveTechNoInc[,i+1])
  colnames(temp2) = c("Steps","Rule","Value")
  #temp2 = (c(1:3000),ActiveTechNoInc[,i:i+1])
  temp = rbind(temp, temp2)
  i = i + 2
}
i = 10
ActiveTechNoInc[,2]


ggplot(AT32Inc, aes(Step,Popularity )) + geom_smooth(aes(group=Rule, color = factor(Rule)), se = FALSE) + ylim(0,30) + xlim(0,2900) + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black"))  +annotate("rect", xmin = 900, xmax = 1000, ymin = 0, ymax = 15000, alpha = .2 ) + ylab("ARTs using Transformation Rule")

ggplot(AT32NI, aes(Step,Popularity )) + geom_smooth(aes(group=Rule, color = factor(Rule))) + ylim(0,30) + xlim(0,2900) + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black"))  + ylab("ARTs using Transformation Rule")