ggplot(incChange, aes(Steps,ValTotalProduct)) + geom_boxplot(aes(group = round_any(Steps,50,floor))) + facet_grid(.~NoResources)
ggplot(WehiaData, aes(Steps,ValTotalProduct)) + geom_point() + facet_grid(Incubation~NoResources)


incChange = data.frame(wehiaNoInc$Steps, wehiaNoInc$Time, wehiaInc$ValueOfTotalOutputProduced - wehiaNoInc$ValueOfTotalOutputProduced, wehiaNoInc$NumberofResources, wehiaInc$TransformationNetworkDensity - wehiaNoInc$TransformationNetworkDensity, wehiaInc$TransformationNetworkEdgeCount - wehiaNoInc$TransformationNetworkEdgeCount, wehiaInc$Population - wehiaNoInc$Population, wehiaInc$GDP - wehiaNoInc$GDP, wehiaInc$CumGDP - wehiaNoInc$CumGDP)
colnames(incChange) = colnames(wehiaInc)

f <- function(x) {
  r <- quantile(x, probs = c(0.05, 0.25, 0.5, 0.75, 0.95))
  names(r) <- c("ymin", "lower", "middle", "upper", "ymax")
  r
}


ggplot(incChange, aes(Steps,GDP)) + geom_boxplot(aes(group = round_any(Steps,200,floor))) + facet_grid(.~NumberOfResources) + geom_smooth() +
  coord_cartesian(ylim=c(-2500,25000))


ggplot(wehiaInc, aes(TransformationNetworkEdgeCount,GDP))+ geom_point() + facet_grid(.~NumberOfResources)

ggplot(wehiaInc, aes(TransformationNetworkEdgeCount,GDP))+ geom_boxplot(outlier.shape = NA,aes(group = round_any(TransformationNetworkEdgeCount,20,floor))) + facet_grid(.~NumberOfResources) + coord_cartesian(ylim=c(0,25000))

ggplot(wehiaExp1_2, aes(Steps,GDP)) + geom_boxplot(outlier.shape = NA,aes(group = round_any(Steps,100,floor))) + facet_grid(.~NumberOfResources) + xlim(0, 2900) + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black")) + coord_cartesian(ylim=c(0,4000))

ggplot(wehiaExp1_32, aes(Steps,GDP)) + geom_boxplot(outlier.shape = NA,aes(group = round_any(Steps,100,floor))) + facet_grid(.~NumberOfResources) + xlim(0, 2900) + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black")) + coord_cartesian(ylim=c(0,4000))












ggplot(subset(incChange, NumberOfResources == 2), aes(Steps,GDP)) + geom_boxplot(aes(group = round_any(Steps,100,floor))) + ylab("Aggregate Change in GDP due to Entrepreneurial Support") + coord_cartesian(ylim=c(-3000, 16000)) + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black")) +annotate("rect", xmin = 900, xmax = 1000, ymin = -3000, ymax = 16000, alpha = .2 ) + xlim(0,2900)
ggplot(subset(incChange, NumberOfResources == 32), aes(Steps,GDP)) + geom_boxplot(aes(group = round_any(Steps,100,floor))) + ylab("Aggregate Change in GDP due to Entrepreneurial Support") + coord_cartesian(ylim=c(-3000, 16000)) + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black")) +annotate("rect", xmin = 900, xmax = 1000, ymin = -3000, ymax = 16000, alpha = .2 ) + xlim(0,2900)

ggplot(incChangeExp2, aes(Steps,GDP)) + geom_boxplot(outlier.shape = NA,aes(group = round_any(Steps,100,floor))) + facet_grid(.~NumberOfResources) + xlim(0, 2900) + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black")) + coord_cartesian(ylim=c(-1000,15000)) +annotate("rect", xmin = 900, xmax = 1000, ymin = 0, ymax = 15000, alpha = .2 ) + ylab("Aggregate Change in GDP due to Entrepreneurial Support")


ggplot(changeByInc, aes(Steps, CumGDP))  + geom_boxplot(width = 0.1) + stat_summary(fun.y=mean, colour="darkred", geom="point", shape=18, size=3,show_guide = FALSE)+ geom_text(data = changeByInc, aes(label = paste("mean =", mean(changeByInc$CumGDP)), y = mean(changeByInc$CumGDP) + 500000, x = 2000.07)) +xlim (1999.8,2000.2) + ylab("Difference in Cumulated GDP due to Incubation")  + theme(axis.title = element_text(size = 16), axis.text = element_text(size = 14, color = "black"), strip.text = element_text(size = 14, color = "black"))