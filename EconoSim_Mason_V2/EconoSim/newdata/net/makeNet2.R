net32_2Links = read.csv("net32_2.csv")
net32_2HIST <- graph.data.frame(net32_2Links, nodes, directed=T)
net32_1Links = read.csv("net32_1.csv")
net32_1 <- graph.data.frame(net32_1Links, nodes, directed=T)
net32_2Links = read.csv("net32_2.csv")
net32_2 <- graph.data.frame(net32_2Links, nodes, directed=T)
net32_3Links = read.csv("net32_3.csv")
net32_3 <- graph.data.frame(net32_3Links, nodes, directed=T)
l2 = layout.circle(net32_2HIST)

plot(net32_1, layout= l2, 
     edge.color = rgb( 
       1-(E(net32_1)$weight) / (max(E(net32_2HIST)$weight)), 
       1-(E(net32_1)$weight) / (max(E(net32_2HIST)$weight)),
       1-(E(net32_1)$weight) / (max(E(net32_2HIST)$weight)),
       c(0, 1 )[(E(net32_1)$weight > 4)+1]
     ) ,
     edge.curved=0.1,
     vertex.size=8,
     vertex.label.cex	=0.5,
     edge.arrow.size=0.6+0.5*(E(net32_1)$weight) / (max(E(net32_2HIST)$weight))
)
plot(net32_2, layout= l2, 
     edge.color = rgb( 
       1-(E(net32_2)$weight) / (max(E(net32_2HIST)$weight)), 
       1-(E(net32_2)$weight) / (max(E(net32_2HIST)$weight)),
       1-(E(net32_2)$weight) / (max(E(net32_2HIST)$weight)),
       c(0, 1 )[(E(net32_2)$weight > 4)+1]
     ) ,
     edge.curved=0.1,
     vertex.size=8,
     vertex.label.cex	=0.5,
     edge.arrow.size=0.6+0.5*(E(net32_2)$weight) / (max(E(net32_2HIST)$weight))
)
plot(net32_3, layout= l2, 
     edge.color = rgb( 
       1-(E(net32_3)$weight) / (max(E(net32_2HIST)$weight)), 
       1-(E(net32_3)$weight) / (max(E(net32_2HIST)$weight)),
       1-(E(net32_3)$weight) / (max(E(net32_2HIST)$weight)),
       c(0, 1 )[(E(net32_3)$weight > 4)+1]
     ) ,
     edge.curved=0.1,
     vertex.size=8,
     vertex.label.cex	=0.5,
     edge.arrow.size=0.6+0.5*(E(net32_3)$weight) / (max(E(net32_2HIST)$weight))
)